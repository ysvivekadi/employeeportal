<!DOCTYPE html>
<html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="layout" content="mtemplate">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">        
    <link href="http://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
    
    <style>   
        input[type=text]{
            width: 80%; 
            padding: 6px 20px;
            border: 1px solid #ccc;
            box-sizing: border-box;
        }
        .wrapper {
            text-align: center;
        }

        .button {
            position: absolute;
            top: 50%;
        }
    </style> 

    <meta http-equiv="Content-type" content="text/html; charset=utf-8"/>
    <title>DHTMLX Gantt Chart </title>

    
        <asset:javascript src="dhtmlxcommon.js"/>
        <asset:javascript src="dhtmlxgantt48b2.js"/>
        <asset:javascript src="dhtmlxgantt_marker.js"/>
        <asset:javascript src="api.js"/>
        <asset:javascript src="dhtmlxmenu.js"/>
        <asset:javascript src="dhtmlxmenu_deprecated.js"/>
        <asset:javascript src="dhtmlxmenu_ext.js"/>


    <link rel="stylesheet" href="${resource(dir:'css',file:'dhtmlxmenu_dhx_terrace.css')}" type="text/css" media="screen"title="no title" charset="utf-8">
    <link rel="stylesheet" href="${resource(dir:'css',file:'dhtmlxgantt_skyblue48b2.css')}" type="text/css" media="screen"
          title="no title" charset="utf-8">
    <style type="text/css" media="screen">
        html, body {
            
            height: 100%;
            
        }
        .nested_task .gantt_add{
        display: none !important;
        }
        .weekend {
            background: #f4f7f4 !important;
        }

        .gantt_selected .weekend {
            background: #FFF3A1 !important;
        }

        .controls_bar {
            border-top: 2px solid #bababa;
            border-bottom: 2px solid #bababa;
            clear: both;
            margin-top: 90px;
            height: 28px;
            background: #f1f1f1;
            color: #494949;
            font-family: Arial, sans-serif;
            font-size: 13px;
            padding-left: 10px;
            line-height: 25px
        }
        .status_line{
            background-color: #0ca30a;
        }
    </style>

<script>
    function modSampleHeight() {

        var headHeight = 100;
        var sch = document.getElementById("gantt_here");
        sch.style.height = (parseInt(document.body.offsetHeight) - headHeight) + "px";
        var contbox = document.getElementById("contbox");
        contbox.style.width = (parseInt(document.body.offsetWidth) - 300) + "px";

        gantt.setSizes();
    }
</script>



</head>

<body class="side_menu_active side_menu_expanded" onresize="modSampleHeight()" onload="modSampleHeight()">
          <script type="text/javascript">
            function myFunction() {
            alert("___" ${flash.message});
            }   
            $( document ).ready(function() {
              var $promo = $('#datepicker');
              $promo.datepicker({language: 'en'})
            });
          </script>
        <div id="page_wrapper">

            <!-- header -->

            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="../manager/abc">Home</a></li>    
                    <li>Gantt Chart</li>    
                </ul>
            </nav>
            <!-- id="datepicker" class='datepicker-here' data-language='en' -->
            <!-- main content -->
            <div id="main_wrapper">
                <div style="height:95px;background-color:#3D3D3D;border-bottom:5px solid #828282; overflow:hidden;">
    

    <div id="contbox"
         style="float:left;color:white;margin:22px 75px 0 75px; overflow:hidden;font: 17px Arial,Helvetica;color:white">
        
        <div style="font-size:12px;padding-left:225px;max-width:500px;">
        </div>
        <div style="font-size:30px;padding-left:300px;margin-top:5px;color:#949292;max-width:1000px;">
            EMPLOYEE GANTT VIEW
        </div>
    </div>

    
</div>
<div>
       
      <g:if test="${flash.message=='error'}">
  <div class="alert alert-warning" role="alert">
   Employee has already been alloted the subtask
  </div >
  </g:if>  
    </div>
<div id="gantt_here" style='width:100%; height:100%;'></div>
<script type="text/javascript" >


    gantt.templates.scale_cell_class = function (date) {
        if (date.getDay() == 0 || date.getDay() == 6) {
            return "weekend";
        }
    };
    gantt.templates.task_cell_class = function (item, date) {
        if (date.getDay() == 0 || date.getDay() == 6) {
            return "weekend";
        }
    };

    gantt.templates.task_text = function (start, end, task) {
        if ( task.type == gantt.config.types.milestone ) {
            return  task.text;
        }
        else if (task.type == gantt.config.types.project){
            return  Math.round(task.progress*100)+ "%  " + task.text;
        }
        else 
        return Math.round(task.progress*100)+ "%  "+ task.text;
        // for text inside progress bar
    };

    gantt.config.columns = [
        {name: "text", label: "Task name", width: "*", tree: true, resize: true },
        {name: "start_time", label: "Start time", template: function (obj) {
            return gantt.templates.date_grid(obj.start_date);
        }, align: "center", width: 60, resize: true },
        {name: "end_date", label: "Deadline", align: "center", width: 60, resize: true },
        {name: "duration", label: "Work-Days", align: "center", width: 60, resize: true},

        {name:"owner",     label:"Owner",  template:function(obj){
            return gantt.getLabel("owner", obj.owner);
        }, align: "center", width:70 },
        {name: "add", label: "", width: 44 }
    ];


    var date_to_str = gantt.date.date_to_str(gantt.config.task_date);
    var today = new Date();
    gantt.addMarker({ start_date: today, css: "today", text: "Today",  title:date_to_str( today)});
    

    gantt.config.grid_resize = true;
    gantt.config.grid_width = 480;
    gantt.config.date_grid = "%F %d";
    gantt.config.scale_height = 60;
    gantt.config.subscales = [
        { unit: "week", step: 1, date: "Week #%W"}
    ];

    gantt.config.start_date = new Date(2016,4,15);
    gantt.config.end_date = new Date(2016,12,15);

gantt.config.work_time = true; //removes non-working time from calculations 
gantt.config.skip_off_time = true;    //hides non-working time in the chart

    gantt.init("gantt_here");
    modSampleHeight();     

    gantt.attachEvent("onTaskLoading", function (task) {
        task.planned_start = gantt.date.parseDate(task.planned_start, "xml_date");
        task.planned_end = gantt.date.parseDate(task.planned_end, "xml_date");
        return true;
    });
        gantt.templates.grid_row_class = function( start, end, task ){
    if ( task.$level > 1 ){
        return "nested_task"
    }
    return "";
};
var emp_table = "${jsonData2}"
emp_table = emp_table.replace(/&quot;/g,'"');
// console.log(emp_table);
var opts =JSON.parse(emp_table);

//console.log(emp_table);
// var opts =  [{key:"0", label: ""},
//          {key:"1", label: "kartikeya Pr."},
//          {key:"2", label: "Jitu Pr."},
//          {key:"3", label: "Vivek Pr."},
//          {key:"4", label: "Avinash Pr."},
//          {key:"5", label: "Arvind"}
//          ];
//gantt.locale.labels["section_time"] = "Boundations";
gantt.locale.labels["section_owner"] = "Owner";
    gantt.config.lightbox.sections = [
        {name: "description", height: 38, map_to: "text", type: "textarea", focus: true},
        {name: "type", type: "typeselect", map_to: "type"},
        {name: "owner", height: 22, map_to: "owner", type: "select", options: opts},
        {name: "time", type: "time", map_to: "auto", time_format:["%d","%m","%Y"]}
    ];

    gantt.config.lightbox.project_sections = [
        {name: "description", height: 38, map_to: "text", type: "textarea", focus: true},
        {name: "type", type: "typeselect", map_to: "type"},
        
        {name: "time", type: "time", map_to: "auto", time_format:["%d","%m","%Y"]}
    ];

    gantt.init("gantt_here");
    var demo_tasks = "${jsonData1}";
    
    demo_tasks = demo_tasks.replace(/&quot;/g,'"');
    console.log(demo_tasks);
    gantt.parse(demo_tasks);


    (function addContentMenu() {
        var menu = new dhtmlXMenuObject();
        menu.setIconsPath("http://dhtmlx.com/docs/products/common/dhtmlxMenu/sample_images/");
        menu.renderAsContextMenu();
        menu.setSkin("dhx_terrace");

        gantt.attachEvent("onContextMenu", function (taskId, linkId, event) {
            var x = event.clientX + document.body.scrollLeft + document.documentElement.scrollLeft,
                    y = event.clientY + document.body.scrollTop + document.documentElement.scrollTop;

            var target = (event.target || event.srcElement);
            var column_id = target.getAttribute("column_id");
            menu.clearAll();

            addColumnsConfig();
            if (column_id) {
                addColumnToggle(column_id);
            }

            menu.showContextMenu(x, y);
            return false;
        });

        menu.attachEvent("onClick", function (id, zoneId, cas) {
            var parts = (id + "").split("#");
            var is_toggle = parts[0] == "toggle",
                    column_id = parts[1] || id;

            var column = gantt.getGridColumn(column_id);

            if (column) {
                var visible = !is_toggle ? menu.getCheckboxState(id) : false;
                column.hide = !visible;
                gantt.render();
            }
            return true;
        });

        function addColumnToggle(column_name) {
            var column = gantt.getGridColumn(column_name);
            var label = getColumnLabel(column);

            //add prefix to distinguish from the same item in 'show columns' menu
            var item_id = "toggle#" + column_name
            menu.addNewChild(null, -1, item_id, "Hide '" + label + "'", false);
            menu.addNewSeparator(item_id);
        }

        function addColumnsConfig() {
            menu.addNewChild(null, -1, "show_columns", "Show columns:", false);
            var columns = gantt.config.columns;

            for (var i = 0; i < columns.length; i++) {
                var checked = (!columns[i].hide),
                        itemLabel = getColumnLabel(columns[i]);
                menu.addCheckbox("child", "show_columns", i, columns[i].name, itemLabel, checked);
            }
        }




        function getColumnLabel(column) {
            if (column == null)
                return '';

            var locale = gantt.locale.labels;
            var text = column.label !== undefined ? column.label : locale["column_" + column.name];

            text = text || column.name;
            return text;
        }
    })();

</script>

            </div>
        <!-- page specific plugins -->
       <!--  <script src="http://yukon.tzdthemes.com/html/assets/lib/select2/select2.min.js"></script> -->
            <!-- validation (parsley.js) -->
            <!-- <script src="http://yukon.tzdthemes.com/html/assets/js/parsley.config.js"></script>
            <script src="http://yukon.tzdthemes.com/html/assets/lib/parsley/dist/parsley.min.js"></script>
             --><!-- wysiwg editor -->
            <!-- <script src="http://yukon.tzdthemes.com/html/assets/lib/ckeditor/ckeditor.js"></script>
            <script src="http://yukon.tzdthemes.com/html/assets/lib/ckeditor/adapters/jquery.js"></script> -->
            
    </body>
</html>