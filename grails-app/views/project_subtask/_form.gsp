<%@ page import="new1.Project_subtask" %>



<div class="fieldcontain ${hasErrors(bean: project_subtaskInstance, field: 'employee', 'error')} ">
	<label for="employee">
		<g:message code="project_subtask.employee.label" default="Employee" />
		
	</label>
	<g:select id="employee" name="employee.id" from="${new1.Employee.list()}" optionKey="id" value="${project_subtaskInstance?.employee?.id}" class="many-to-one" noSelection="['null': '']"/>

</div>

<div class="fieldcontain ${hasErrors(bean: project_subtaskInstance, field: 'end_time', 'error')} required">
	<label for="end_time">
		<g:message code="project_subtask.end_time.label" default="Endtime" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="end_time" precision="day"  value="${project_subtaskInstance?.end_time}"  />

</div>

<div class="fieldcontain ${hasErrors(bean: project_subtaskInstance, field: 'progress', 'error')} required">
	<label for="progress">
		<g:message code="project_subtask.progress.label" default="Progress" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="progress" type="number" value="${project_subtaskInstance.progress}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: project_subtaskInstance, field: 'project_task', 'error')} required">
	<label for="project_task">
		<g:message code="project_subtask.project_task.label" default="Projecttask" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="project_task" name="project_task.id" from="${new1.Project_task.list()}" optionKey="id" required="" value="${project_subtaskInstance?.project_task?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: project_subtaskInstance, field: 'start_time', 'error')} required">
	<label for="start_time">
		<g:message code="project_subtask.start_time.label" default="Starttime" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="start_time" precision="day"  value="${project_subtaskInstance?.start_time}"  />

</div>

<div class="fieldcontain ${hasErrors(bean: project_subtaskInstance, field: 'subtask_name', 'error')} required">
	<label for="subtask_name">
		<g:message code="project_subtask.subtask_name.label" default="Subtaskname" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="subtask_name" required="" value="${project_subtaskInstance?.subtask_name}"/>

</div>

