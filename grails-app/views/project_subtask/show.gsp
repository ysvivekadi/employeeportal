
<%@ page import="new1.Project_subtask" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'project_subtask.label', default: 'Project_subtask')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-project_subtask" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-project_subtask" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list project_subtask">
			
				<g:if test="${project_subtaskInstance?.employee}">
				<li class="fieldcontain">
					<span id="employee-label" class="property-label"><g:message code="project_subtask.employee.label" default="Employee" /></span>
					
						<span class="property-value" aria-labelledby="employee-label"><g:link controller="employee" action="show" id="${project_subtaskInstance?.employee?.id}">${project_subtaskInstance?.employee?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${project_subtaskInstance?.end_time}">
				<li class="fieldcontain">
					<span id="end_time-label" class="property-label"><g:message code="project_subtask.end_time.label" default="Endtime" /></span>
					
						<span class="property-value" aria-labelledby="end_time-label"><g:formatDate date="${project_subtaskInstance?.end_time}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${project_subtaskInstance?.progress}">
				<li class="fieldcontain">
					<span id="progress-label" class="property-label"><g:message code="project_subtask.progress.label" default="Progress" /></span>
					
						<span class="property-value" aria-labelledby="progress-label"><g:fieldValue bean="${project_subtaskInstance}" field="progress"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${project_subtaskInstance?.project_task}">
				<li class="fieldcontain">
					<span id="project_task-label" class="property-label"><g:message code="project_subtask.project_task.label" default="Projecttask" /></span>
					
						<span class="property-value" aria-labelledby="project_task-label"><g:link controller="project_task" action="show" id="${project_subtaskInstance?.project_task?.id}">${project_subtaskInstance?.project_task?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${project_subtaskInstance?.start_time}">
				<li class="fieldcontain">
					<span id="start_time-label" class="property-label"><g:message code="project_subtask.start_time.label" default="Starttime" /></span>
					
						<span class="property-value" aria-labelledby="start_time-label"><g:formatDate date="${project_subtaskInstance?.start_time}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${project_subtaskInstance?.subtask_name}">
				<li class="fieldcontain">
					<span id="subtask_name-label" class="property-label"><g:message code="project_subtask.subtask_name.label" default="Subtaskname" /></span>
					
						<span class="property-value" aria-labelledby="subtask_name-label"><g:fieldValue bean="${project_subtaskInstance}" field="subtask_name"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:project_subtaskInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${project_subtaskInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
