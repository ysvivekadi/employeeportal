
<%@ page import="new1.Project_subtask" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'project_subtask.label', default: 'Project_subtask')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-project_subtask" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-project_subtask" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<th><g:message code="project_subtask.employee.label" default="Employee" /></th>
					
						<g:sortableColumn property="end_time" title="${message(code: 'project_subtask.end_time.label', default: 'Endtime')}" />
					
						<g:sortableColumn property="progress" title="${message(code: 'project_subtask.progress.label', default: 'Progress')}" />
					
						<th><g:message code="project_subtask.project_task.label" default="Projecttask" /></th>
					
						<g:sortableColumn property="start_time" title="${message(code: 'project_subtask.start_time.label', default: 'Starttime')}" />
					
						<g:sortableColumn property="subtask_name" title="${message(code: 'project_subtask.subtask_name.label', default: 'Subtaskname')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${project_subtaskInstanceList}" status="i" var="project_subtaskInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${project_subtaskInstance.id}">${fieldValue(bean: project_subtaskInstance, field: "employee")}</g:link></td>
					
						<td><g:formatDate date="${project_subtaskInstance.end_time}" /></td>
					
						<td>${fieldValue(bean: project_subtaskInstance, field: "progress")}</td>
					
						<td>${fieldValue(bean: project_subtaskInstance, field: "project_task")}</td>
					
						<td><g:formatDate date="${project_subtaskInstance.start_time}" /></td>
					
						<td>${fieldValue(bean: project_subtaskInstance, field: "subtask_name")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${project_subtaskInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
