<!DOCTYPE html>
<html>
<head>
        <meta charset="UTF-8">
        <title>TE Employee portal</title>
        <meta name="layout" content="mtemplate">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
        
        <!-- <asset:javascript src="application.js"/>  -->
        <!-- <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"> -->
        <!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css"> -->
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
        <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.3.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
              
        <script type="text/javascript" class="init">
            $(document).ready(function() {

                var radha = "${data}";
                
                radha = radha.replace(/&quot;/g, '\"');
                radha = radha.replace(/&#39;/g, '\"');
                radha = radha.replace(/&#92;/g, ''); 
                       
                var krishna =  JSON.parse(radha);
                console.log(krishna);
                console.log(radha);
                $('#example').DataTable( {
                    data: krishna 
                } );
            } );
        </script>
          
    </head>
    <body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">

            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="abc">Home</a></li>
                    <li><a href="Timesheet">Timesheet</a></li>    
                </ul>
            </nav>

            <!-- main content -->
            <div id="main_wrapper">
                <div class="container-fluid">

                <div class="col-md-3">
                    <g:form action="selectemp">    
                        <label for="val_select" class="req">Employee</label>
                        <select name="emplo" onchange="submit()">
                            <g:if test="${flash.message=='0'}">
                            <option disabled selected value>Select Employee</option>
                            </g:if>
                            <g:each var="empno" in="${emp}">
                                <g:if test="${flash.message=='1' && empno.username==employee.username}">
                                    <option value="${empno.username}" selected >${empno.username}</option>
                                </g:if>
                                <g:else>
                                    <option value="${empno.username}" >${empno.username}</option>
                                </g:else>
                            </g:each>
                        </select>
                    </g:form>
                </div>
                <br>
                <br>
                <br>
                <br>
                    <p id="fooHolder"></p>
                    <table id="example" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>fromdate</th>
                                <th>todate</th>
                                <th>project</th>
                                <th>task</th>
                                <th>description</th>
                                <th>work hour</th>
                                <th>Employee</th>
                            </tr>
                        </thead>
                    </table>
             </div>
            </div>
    </body>
</html>