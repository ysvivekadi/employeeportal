<<!DOCTYPE html>
<html>
<head>
<meta name="layout" content="mtemplate">
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  

	<title>salary structure form</title>
</head>
<body>
    <div id="main_wrapper">
		<div class="container-fluid">
			<g:form controller="Salary">
				<label><h4>  UPDATE SALARY STRUCTURE</h4></label> <br> <br>
				
				<g:if test="${flash.message}">
				<div class="alert" style="color: black" >  ${flash.message}  </div>
				</g:if>

				
				<div class="col-xs-3"> <br><label> basic:</label>
				<input type="number" step="any" name="basic" value="${s.basic}" class="form-control input-sm" ></div>
				
				<div class="col-xs-3"><br><label> hra:</label>
				<input type="number" step="any" name="hra" value="${s.hra}" class="form-control input-sm" ></div>
				
				<div class="col-xs-3"> <br><label> conveyance:</label>
				<input type="number" step="any" name="conveyance" value="${s.conveyance}" class="form-control input-sm" ></div>
				
				<div class="col-xs-3"> <br><label> special:</label>
				<input type="number" step="any" name="special" value="${s.special}" class="form-control input-sm" ></div>
				
				<div class="col-xs-3"> <br><label> medical:</label>
				<input type="number" step="any" name="medical" value="${s.medical}"class="form-control input-sm" ></div>
				
				<div class="col-xs-3"> <br><label> employeePF:</label>
				<input type="number" step="any" name="employeePF" value="${s.employeePF}"class="form-control input-sm" ></div>
				
				<div class="col-xs-3"> <br><label> employerPF:</label>
				<input type="number" step="any" name="employerPF" value="${s.employerPF}"class="form-control input-sm" ></div>
				
				<div class="col-xs-3"> <br><label> year:</label>
				<input type="number" step="any" name="year" value="${s.year}" min="1990" max="2050" class="form-control input-sm" ></div>
				
				<div class="col-xs-3"> <br><label> basic_proj:</label>
				<input type="number" step="any" name="basic_proj" value="${s.basic_proj}" class="form-control input-sm" ></div>
				
				<div class="col-xs-3"> <br><label> hra_proj:</label>
				<input type="number" step="any" name="hra_proj" value="${s.hra_proj}" class="form-control input-sm" ></div>
				
				<div class="col-xs-3"> <br><label> conveyance_proj:</label>
				<input type="number" step="any" name="conveyance_proj" value="${s.conveyance_proj}" class="form-control input-sm" ></div>
				
				<div class="col-xs-3"> <br><label> special_proj:</label>
				<input type="number" step="any" name="special_proj" value="${s.special_proj}" class="form-control input-sm" ></div>
				
				<div class="col-xs-3"> <br><label> medical_proj:</label>
				<input type="number" step="any" name="medical_proj" value="${s.medical_proj}" class="form-control input-sm" ></div>
				
				<div class="col-xs-3"> <br><label> employeePF_proj:</label>
				<input type="number" step="any" name="employeePF_proj" value="${s.employeePF_proj}" class="form-control input-sm" ></div>
				
				<div class="col-xs-3"> <br><label> employerPF_proj:</label>
				<input type="number" step="any" name="employerPF_proj" value="${s.employerPF_proj}" class="form-control input-sm" ></div>

				<input type="hidden" name="employee" value="${s.employee}" >

				<!--<div class="col-xs-3"> <br><br><label> empoyee:</label>
				<g:select name="employee" from="${emp}"/> </div>-->
				<input type="hidden" name="kind" value="update">
				<input type="hidden" name="id" value="${s.id}">

				<div class="col-xs-5"><br><br><label> <g:actionSubmit action="save" value="Submit"/></label></div>

			</g:form>
		</div>
	</div>
</body>
</html>