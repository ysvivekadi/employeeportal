<!DOCTYPE html>
<html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="layout" content="mtemplate">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">        
    <link href="http://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
    <!-- <link href="http://yukon.tzdthemes.com/html/assets/lib/select2/select2.css" rel="stylesheet" media="all"> -->
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"> -->
   
    
    <style>   
        input[type=text]{
            width: 80%; 
            padding: 6px 20px;
            border: 1px solid #ccc;
            box-sizing: border-box;
        }
        .wrapper {
            text-align: center;
        }

        .button {
            position: absolute;
            top: 50%;
        }
    </style> 

    <meta http-equiv="Content-type" content="text/html; charset=utf-8"/>
    <title>DHTMLX Gantt Chart </title>

    
        <!-- <asset:javascript src="application.js"/> -->
        <asset:javascript src="dhtmlxcommon.js"/>
        <asset:javascript src="dhtmlxgantt48b2.js?v=4.0"/>
        <asset:javascript src="dhtmlxgantt_marker.js"/>
        <asset:javascript src="api.js"/>
        <asset:javascript src="dhtmlxmenu.js"/>
        <asset:javascript src="dhtmlxmenu_deprecated.js"/>
        <asset:javascript src="dhtmlxmenu_ext.js"/>
        <!-- <asset:stylesheet src="application.css"/> -->


    <link rel="stylesheet" href="${resource(dir:'css',file:'dhtmlxmenu_dhx_terrace.css')}" type="text/css" media="screen"title="no title" charset="utf-8">
<link rel="stylesheet" href="${resource(dir:'css',file:'dhtmlxgantt_skyblue48b2.css')}" type="text/css" media="screen"
          title="no title" charset="utf-8">
    <style type="text/css" media="screen">
        html, body {
            
            height: 100%;
            
        }
        .nested_task .gantt_add{
        display: none !important;
        }
        .weekend {
            background: #f4f7f4 !important;
        }

        .gantt_selected .weekend {
            background: #FFF3A1 !important;
        }

        .controls_bar {
            border-top: 2px solid #bababa;
            border-bottom: 2px solid #bababa;
            clear: both;
            margin-top: 90px;
            height: 28px;
            background: #f1f1f1;
            color: #494949;
            font-family: Arial, sans-serif;
            font-size: 13px;
            padding-left: 10px;
            line-height: 25px
        }
        .status_line{
            background-color: #0ca30a;
        }
    </style>

<script>
    function modSampleHeight() {

        var headHeight = 100;
        var sch = document.getElementById("gantt_here");
        sch.style.height = (parseInt(document.body.offsetHeight) - headHeight) + "px";
        var contbox = document.getElementById("contbox");
        contbox.style.width = (parseInt(document.body.offsetWidth) - 300) + "px";

        gantt.setSizes();
    }
</script>
</head>

<body class="side_menu_active side_menu_expanded" >
          
        <div id="page_wrapper">
            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="../manager/abc">Home</a></li>    
                    <li>Create Employee</li>    
                </ul>
            </nav>
            <!-- id="datepicker" class='datepicker-here' data-language='en' -->
            <!-- main content -->
            <div id="main_wrapper">
            <div> 
                <g:if test="${flash.message=='error'}">
                    <div class="alert alert-danger" role="alert">
                    Fields cannot remain blank
                    </div >
                </g:if> 
                <g:elseif test="${flash.message=="y"}">
                    <div class="alert alert-danger" role="alert">
                        Username already exists
                    </div>
                </g:elseif>
                    <g:form class="form-inline" role="form" controller="Employee">  
                        <div class="form-group">
                            <input class="form-control" id="focusedInput"  type="text" placeholder="Username" name="username">
                            <input class="form-control" id="focusedInput" type="text" placeholder="Password" name="password">
                            <g:actionSubmit id="btn-signup" class="btn btn-info" action="save" value="create employee"/>
                        </div>
                    </g:form>
            </div>
                <br></br>
            <div>

                <table class="table">
                    <thead class="thead-inverse">
                    <tr>
                      <th>ID</th>
                      <th>Username</th>
                      <th>Password</th>
                      
                    </tr>
                    </thead>
                    <tbody>
                      	<g:each in="${emplist}" var="counter">
                        <tr>
                          <th scope="row">${counter.id}</th>
                          <td>${counter.username}</td>
                          <td>${counter.password}</td>
                        </tr>
                        </g:each>
                    </tbody>
                </table>

            </div>
         
        
            </div>
        <!-- page specific plugins -->
       <!--  <script src="http://yukon.tzdthemes.com/html/assets/lib/select2/select2.min.js"></script> -->
            <!-- validation (parsley.js) -->    
            <!-- <script src="http://yukon.tzdthemes.com/html/assets/js/parsley.config.js"></script>
            <script src="http://yukon.tzdthemes.com/html/assets/lib/parsley/dist/parsley.min.js"></script>
             --><!-- wysiwg editor -->
            <!-- <script src="http://yukon.tzdthemes.com/html/assets/lib/ckeditor/ckeditor.js"></script>
            <script src="http://yukon.tzdthemes.com/html/assets/lib/ckeditor/adapters/jquery.js"></script> -->
            
    </body>
</html>