<!DOCTYPE <!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="mtemplate">
		<meta name="viewport" content="width=device-width, initial-scale=1">
  		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
		<style>
		movie_item {
    display: block;
    margin-top: 10px;
    height: 175px;
}
		.button {
		    background-color: #1C587F;
		    border: none;
		    color: white;
		    padding: 15px 32px;
		    text-align: center;
		    text-decoration: none;
		    display: inline-block;
		    font-size: 16px;
		    margin: 4px 2px;
		    cursor: pointer;
		}

		p.indent{padding-left: 1.8em}
		</style>
	</head>
	<body>
		<div id="main_wrapper">
			<div class="container-fluid">
				<div>
					<g:form controller="Salary">
						<br> <h4>DELETE/UPDATE SALARY STRUCTURE</h4> 
						<g:if test="${flash.message}">
							<div class="alert" style="color: black">
							${flash.message}
							</div>
						</g:if>

						<div class="col-xs-3"> <br><label> empoyee:</label>
						<br> <g:select  name="employee" from="${emp}"/> </div>

						<div class="col-xs-2"> <br><label> year:</label>
						<input type="number" name="year" min="1990" max="2050" class="form-control input-sm" >	</div>

						<div >
						<br><br><label> <g:actionSubmit action="c_show" value="show"/></label> 
						</div>

					</g:form>
				</div>
				<br> <br>
				<hr> <hr> <br>
				<div>
						<a href="../manager/salary" class="button">create new</a>
				</div>

			</div>
		</div>
	</body>
</html>