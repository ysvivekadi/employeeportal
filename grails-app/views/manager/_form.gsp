<%@ page import="new1.Manager" %>



<div class="fieldcontain ${hasErrors(bean: managerInstance, field: 'employee', 'error')} ">
	<label for="employee">
		<g:message code="manager.employee.label" default="Employee" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${managerInstance?.employee?}" var="e">
    <li><g:link controller="employee" action="show" id="${e.id}">${e?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="employee" action="create" params="['manager.id': managerInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'employee.label', default: 'Employee')])}</g:link>
</li>
</ul>


</div>

<div class="fieldcontain ${hasErrors(bean: managerInstance, field: 'password', 'error')} required">
	<label for="password">
		<g:message code="manager.password.label" default="Password" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="password" required="" value="${managerInstance?.password}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: managerInstance, field: 'project', 'error')} required">
	<label for="project">
		<g:message code="manager.project.label" default="Project" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="project" name="project.id" from="${new1.Project.list()}" optionKey="id" required="" value="${managerInstance?.project?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: managerInstance, field: 'username', 'error')} required">
	<label for="username">
		<g:message code="manager.username.label" default="Username" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="username" required="" value="${managerInstance?.username}"/>

</div>

