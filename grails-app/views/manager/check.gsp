
<!DOCTYPE html>
<html>
   <head>
	  <meta charset="UTF-8">
	  <title>TE Employee portal</title>
	  <meta name="layout" content="mtemplate">
	  <!-- <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
	  <meta http-equiv="X-UA-Compatible" content="IE=edge">-->
	  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
	  
	  <script src="//code.jquery.com/jquery-1.12.3.js"></script>
	  <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
	  
	   
	  <script type="text/javascript" class="init">
		$(document).ready(function() {
			var radha = "${data}";
			radha = radha.replace(/&quot;/g, '\"');
			radha = radha.replace(/&#39;/g, '\"');
			radha = radha.replace(/&#92;/g, '');            
			var krishna =  JSON.parse(radha);
			$('#example').DataTable( {
			   data: krishna 
			} );
		} );
	  </script>
	  </head>
   <body class="side_menu_active side_menu_expanded">
	  <div id="page_wrapper">
	  	<!-- header -->
            
            <!-- breadcrumbs -->
		 <nav id="breadcrumbs">
			<ul>
			   <li><a href="..\employee\home">Home</a></li>
			   <li>Checkin-Checkout</li>
			</ul>
		 </nav>
		 <!-- main content -->
		 <div id="main_wrapper">
			<div class="container-fluid">
			   <!-- <div class="row">
				  <div class="col-md-4 col-sm-12">
					 <ul class="list-group">
						<li class="list-group-item">
							<div class="pull-right">
								<g:if test="${session.checkout == 1 }">              
									<button style="display:none;" id="1"  class="btn">CheckIn</button>
									<button id="12"  class="btn btn-success" checked>CheckOut</button>
								</g:if>
								<g:else>             
									<button id="1"  class="btn">CheckIn</button>
									<button style="display:none;" id="12"  class="btn btn-success" checked>CheckOut</button>
								</g:else>
							</div>
						   CheckIn Status
						</li>
					 </ul>
				  </div>
			   </div> -->
			   <div class="row">
				  <table id="example" class="display table table-striped table-bordered" cellspacing="0" width="100%">
					 <thead>
						<tr>
						   <th>checkin_date</th>
						   <th>checkout_date</th>
						   <th>time_duration</th>
						   <th>employee</th>
						</tr>
					 </thead>
				  </table>
			   </div>
			</div>
		 </div>
		 <!-- main menu -->
		<!--  -->
	  
	  <script src="http://yukon.tzdthemes.com/html/assets/lib/switchery/dist/switchery.min.js"></script>
	  <script src="http://yukon.tzdthemes.com/html/assets/js/yukon_all.js"></script>
	  
   </body>
</html>