
<%@ page import="new1.Timesheet" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'timesheet.label', default: 'Timesheet')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-timesheet" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-timesheet" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list timesheet">
			
				<g:if test="${timesheetInstance?.fromdate}">
				<li class="fieldcontain">
					<span id="fromdate-label" class="property-label"><g:message code="timesheet.fromdate.label" default="Fromdate" /></span>
					
						<span class="property-value" aria-labelledby="fromdate-label"><g:fieldValue bean="${timesheetInstance}" field="fromdate"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${timesheetInstance?.todate}">
				<li class="fieldcontain">
					<span id="todate-label" class="property-label"><g:message code="timesheet.todate.label" default="Todate" /></span>
					
						<span class="property-value" aria-labelledby="todate-label"><g:fieldValue bean="${timesheetInstance}" field="todate"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${timesheetInstance?.task}">
				<li class="fieldcontain">
					<span id="task-label" class="property-label"><g:message code="timesheet.task.label" default="Task" /></span>
					
						<span class="property-value" aria-labelledby="task-label"><g:fieldValue bean="${timesheetInstance}" field="task"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${timesheetInstance?.description}">
				<li class="fieldcontain">
					<span id="description-label" class="property-label"><g:message code="timesheet.description.label" default="Description" /></span>
					
						<span class="property-value" aria-labelledby="description-label"><g:fieldValue bean="${timesheetInstance}" field="description"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${timesheetInstance?.work_hour}">
				<li class="fieldcontain">
					<span id="work_hour-label" class="property-label"><g:message code="timesheet.work_hour.label" default="Workhour" /></span>
					
						<span class="property-value" aria-labelledby="work_hour-label"><g:fieldValue bean="${timesheetInstance}" field="work_hour"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${timesheetInstance?.employee}">
				<li class="fieldcontain">
					<span id="employee-label" class="property-label"><g:message code="timesheet.employee.label" default="Employee" /></span>
					
						<span class="property-value" aria-labelledby="employee-label"><g:link controller="employee" action="show" id="${timesheetInstance?.employee?.id}">${timesheetInstance?.employee?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${timesheetInstance?.project}">
				<li class="fieldcontain">
					<span id="project-label" class="property-label"><g:message code="timesheet.project.label" default="Project" /></span>
					
						<span class="property-value" aria-labelledby="project-label"><g:fieldValue bean="${timesheetInstance}" field="project"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:timesheetInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${timesheetInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
