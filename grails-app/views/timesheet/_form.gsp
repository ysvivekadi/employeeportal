<%@ page import="new1.Timesheet" %>



<div class="fieldcontain ${hasErrors(bean: timesheetInstance, field: 'fromdate', 'error')} required">
	<label for="fromdate">
		<g:message code="timesheet.fromdate.label" default="Fromdate" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="fromdate" required="" value="${timesheetInstance?.fromdate}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: timesheetInstance, field: 'todate', 'error')} required">
	<label for="todate">
		<g:message code="timesheet.todate.label" default="Todate" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="todate" required="" value="${timesheetInstance?.todate}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: timesheetInstance, field: 'task', 'error')} required">
	<label for="task">
		<g:message code="timesheet.task.label" default="Task" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="task" required="" value="${timesheetInstance?.task}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: timesheetInstance, field: 'description', 'error')} required">
	<label for="description">
		<g:message code="timesheet.description.label" default="Description" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="description" required="" value="${timesheetInstance?.description}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: timesheetInstance, field: 'work_hour', 'error')} required">
	<label for="work_hour">
		<g:message code="timesheet.work_hour.label" default="Workhour" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="work_hour" required="" value="${timesheetInstance?.work_hour}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: timesheetInstance, field: 'employee', 'error')} required">
	<label for="employee">
		<g:message code="timesheet.employee.label" default="Employee" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="employee" name="employee.id" from="${new1.Employee.list()}" optionKey="id" required="" value="${timesheetInstance?.employee?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: timesheetInstance, field: 'project', 'error')} required">
	<label for="project">
		<g:message code="timesheet.project.label" default="Project" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="project" required="" value="${timesheetInstance?.project}"/>

</div>

