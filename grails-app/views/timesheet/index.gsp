
<%@ page import="new1.Timesheet" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'timesheet.label', default: 'Timesheet')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-timesheet" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-timesheet" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="fromdate" title="${message(code: 'timesheet.fromdate.label', default: 'Fromdate')}" />
					
						<g:sortableColumn property="todate" title="${message(code: 'timesheet.todate.label', default: 'Todate')}" />
					
						<g:sortableColumn property="task" title="${message(code: 'timesheet.task.label', default: 'Task')}" />
					
						<g:sortableColumn property="description" title="${message(code: 'timesheet.description.label', default: 'Description')}" />
					
						<g:sortableColumn property="work_hour" title="${message(code: 'timesheet.work_hour.label', default: 'Workhour')}" />
					
						<th><g:message code="timesheet.employee.label" default="Employee" /></th>
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${timesheetInstanceList}" status="i" var="timesheetInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${timesheetInstance.id}">${fieldValue(bean: timesheetInstance, field: "fromdate")}</g:link></td>
					
						<td>${fieldValue(bean: timesheetInstance, field: "todate")}</td>
					
						<td>${fieldValue(bean: timesheetInstance, field: "task")}</td>
					
						<td>${fieldValue(bean: timesheetInstance, field: "description")}</td>
					
						<td>${fieldValue(bean: timesheetInstance, field: "work_hour")}</td>
					
						<td>${fieldValue(bean: timesheetInstance, field: "employee")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${timesheetInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
