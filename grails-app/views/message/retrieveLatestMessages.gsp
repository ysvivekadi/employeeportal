<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Simple Chat</title>
    <g:javascript library="jquery"/>
</head>
<body>

<g:each in="${messages}" var="message">
    <div>
        <span class="nickname">${message.nickname}</span> - <span class="msg">${message.message}</span>
    </div>
</g:each>

</body>
</html>