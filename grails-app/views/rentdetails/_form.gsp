<%@ page import="new1.Rentdetails" %>



<div class="fieldcontain ${hasErrors(bean: rentdetailsInstance, field: 'employee', 'error')} required">
	<label for="employee">
		<g:message code="rentdetails.employee.label" default="Employee" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="employee" name="employee.id" from="${new1.Employee.list()}" optionKey="id" required="" value="${rentdetailsInstance?.employee?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: rentdetailsInstance, field: 'from_date', 'error')} required">
	<label for="from_date">
		<g:message code="rentdetails.from_date.label" default="Fromdate" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="from_date" required="" value="${rentdetailsInstance?.from_date}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: rentdetailsInstance, field: 'ismetro', 'error')} ">
	<label for="ismetro">
		<g:message code="rentdetails.ismetro.label" default="Ismetro" />
		
	</label>
	<g:checkBox name="ismetro" value="${rentdetailsInstance?.ismetro}" />

</div>

<div class="fieldcontain ${hasErrors(bean: rentdetailsInstance, field: 'location', 'error')} required">
	<label for="location">
		<g:message code="rentdetails.location.label" default="Location" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="location" required="" value="${rentdetailsInstance?.location}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: rentdetailsInstance, field: 'to_date', 'error')} required">
	<label for="to_date">
		<g:message code="rentdetails.to_date.label" default="Todate" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="to_date" required="" value="${rentdetailsInstance?.to_date}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: rentdetailsInstance, field: 'total_rent', 'error')} required">
	<label for="total_rent">
		<g:message code="rentdetails.total_rent.label" default="Totalrent" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="total_rent" type="number" value="${rentdetailsInstance.total_rent}" required=""/>

</div>

