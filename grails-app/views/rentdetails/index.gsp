
<%@ page import="new1.Rentdetails" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'rentdetails.label', default: 'Rentdetails')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-rentdetails" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-rentdetails" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<th><g:message code="rentdetails.employee.label" default="Employee" /></th>
					
						<g:sortableColumn property="from_date" title="${message(code: 'rentdetails.from_date.label', default: 'Fromdate')}" />
					
						<g:sortableColumn property="ismetro" title="${message(code: 'rentdetails.ismetro.label', default: 'Ismetro')}" />
					
						<g:sortableColumn property="location" title="${message(code: 'rentdetails.location.label', default: 'Location')}" />
					
						<g:sortableColumn property="to_date" title="${message(code: 'rentdetails.to_date.label', default: 'Todate')}" />
					
						<g:sortableColumn property="total_rent" title="${message(code: 'rentdetails.total_rent.label', default: 'Totalrent')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${rentdetailsInstanceList}" status="i" var="rentdetailsInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${rentdetailsInstance.id}">${fieldValue(bean: rentdetailsInstance, field: "employee")}</g:link></td>
					
						<td>${fieldValue(bean: rentdetailsInstance, field: "from_date")}</td>
					
						<td><g:formatBoolean boolean="${rentdetailsInstance.ismetro}" /></td>
					
						<td>${fieldValue(bean: rentdetailsInstance, field: "location")}</td>
					
						<td>${fieldValue(bean: rentdetailsInstance, field: "to_date")}</td>
					
						<td>${fieldValue(bean: rentdetailsInstance, field: "total_rent")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${rentdetailsInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
