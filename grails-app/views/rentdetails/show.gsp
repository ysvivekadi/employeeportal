
<%@ page import="new1.Rentdetails" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'rentdetails.label', default: 'Rentdetails')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-rentdetails" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-rentdetails" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list rentdetails">
			
				<g:if test="${rentdetailsInstance?.employee}">
				<li class="fieldcontain">
					<span id="employee-label" class="property-label"><g:message code="rentdetails.employee.label" default="Employee" /></span>
					
						<span class="property-value" aria-labelledby="employee-label"><g:link controller="employee" action="show" id="${rentdetailsInstance?.employee?.id}">${rentdetailsInstance?.employee?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${rentdetailsInstance?.from_date}">
				<li class="fieldcontain">
					<span id="from_date-label" class="property-label"><g:message code="rentdetails.from_date.label" default="Fromdate" /></span>
					
						<span class="property-value" aria-labelledby="from_date-label"><g:fieldValue bean="${rentdetailsInstance}" field="from_date"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${rentdetailsInstance?.ismetro}">
				<li class="fieldcontain">
					<span id="ismetro-label" class="property-label"><g:message code="rentdetails.ismetro.label" default="Ismetro" /></span>
					
						<span class="property-value" aria-labelledby="ismetro-label"><g:formatBoolean boolean="${rentdetailsInstance?.ismetro}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${rentdetailsInstance?.location}">
				<li class="fieldcontain">
					<span id="location-label" class="property-label"><g:message code="rentdetails.location.label" default="Location" /></span>
					
						<span class="property-value" aria-labelledby="location-label"><g:fieldValue bean="${rentdetailsInstance}" field="location"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${rentdetailsInstance?.to_date}">
				<li class="fieldcontain">
					<span id="to_date-label" class="property-label"><g:message code="rentdetails.to_date.label" default="Todate" /></span>
					
						<span class="property-value" aria-labelledby="to_date-label"><g:fieldValue bean="${rentdetailsInstance}" field="to_date"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${rentdetailsInstance?.total_rent}">
				<li class="fieldcontain">
					<span id="total_rent-label" class="property-label"><g:message code="rentdetails.total_rent.label" default="Totalrent" /></span>
					
						<span class="property-value" aria-labelledby="total_rent-label"><g:fieldValue bean="${rentdetailsInstance}" field="total_rent"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:rentdetailsInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${rentdetailsInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
