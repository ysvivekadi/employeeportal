<%@ page import="new1.Investment" %>



<div class="fieldcontain ${hasErrors(bean: investmentInstance, field: 'amount', 'error')} required">
	<label for="amount">
		<g:message code="investment.amount.label" default="Amount" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="amount" type="number" value="${investmentInstance.amount}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: investmentInstance, field: 'employee', 'error')} required">
	<label for="employee">
		<g:message code="investment.employee.label" default="Employee" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="employee" name="employee.id" from="${new1.Employee.list()}" optionKey="id" required="" value="${investmentInstance?.employee?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: investmentInstance, field: 'taxrebate', 'error')} required">
	<label for="taxrebate">
		<g:message code="investment.taxrebate.label" default="Taxrebate" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="taxrebate" required="" value="${investmentInstance?.taxrebate}"/>

</div>

