<!DOCTYPE html>
<html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="layout" content="etemplate">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">        

    <link rel="stylesheet" type="text/css" href="http://t1m0n.name/air-datepicker/dist/css/datepicker.min.css">
    <link rel="stylesheet" type="text/css" href="http://t4t5.github.io/sweetalert/dist/sweetalert.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css">
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.3.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>

    <script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script> 
    <script type="text/javascript" src="http://t1m0n.name/air-datepicker/dist/js/datepicker.js">  </script>
    <script type="text/javascript" src="http://t1m0n.name/air-datepicker/dist/js/i18n/datepicker.en.js">  </script> 
    <script type="text/javascript" src="http://t4t5.github.io/sweetalert/dist/sweetalert-dev.js"></script>
    <script type="text/javascript" class="init">
            $(document).ready(function() {
                var radha = "${data}";
                var emp = "${emp}";
                console.log(emp);

                radha = radha.replace(/&quot;/g, '\"');
                radha = radha.replace(/&#39;/g, '\"');
                radha = radha.replace(/&#92;/g, '');            
                var krishna =  JSON.parse(radha);
                console.log(krishna);
                console.log(radha);
                $('#example').DataTable( {
                    data: krishna 
                } );
            } );
    </script>
    <script type="text/javascript">
        $(document).ready(function(){
            $("#n").click(function(){
            sweetAlert("The paragraph was clicked.");
            });
        });
    </script>
    <style>   
        input[type=text]{
            width: 80%; 
            padding: 6px 20px;
            border: 1px solid #ccc;
            box-sizing: border-box;
        }
        .wrapper {
            text-align: center;
        }

        .button {
            position: absolute;
            top: 50%;
        }
    </style> 
</head>

<body class="side_menu_active side_menu_expanded">
        
        <div id="page_wrapper">
            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="..\employee\home">Home</a></li>    
                    <li>Timesheet</li>    
                </ul>
            </nav>
            <div id="main_wrapper">
                <div class="container-fluid">
                    <table id="example" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>fromdate</th>
                                <th>todate</th>
                                <th>project</th>
                                <th>task</th>
                                <th>description</th>
                                <th>work hour</th>
                                <th>Employee Id</th>
                            </tr>
                        </thead>
                    </table>
                    <br>
                    <br>
                        <div class="wrapper" >
                            <a href="newtime">
                            <button class="btn btn-primary">Fill New</button></a>
                        </div>
                </div>
            </div>   
    </body>
</html>