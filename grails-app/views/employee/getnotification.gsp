<%@ page import="new1.Employee" %>
<!DOCTYPE html>
<html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="layout" content="etemplate">
    
    
    

   <link href="http://bootstrapmaster.com/live/simpliq2/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="http://bootstrapmaster.com/live/simpliq2/assets/css/style.min.css" rel="stylesheet">
    <asset:stylesheet src="newstyle.css"/>


        
</head>

<body class="side_menu_active side_menu_expanded">
         
        <div id="page_wrapper">

            <!-- header -->
            <header id="main_header">
                <div class="container-fluid">
                    <div class="brand_section">
                        <a href="home"><asset:image src="logo.png" alt="site_logo" width="150" height="60"/></a>
                    </div>
                    <div class="header_user_actions dropdown">
                        <div data-toggle="dropdown" class="dropdown-toggle user_dropdown">
                            <div class="user_avatar">
                                <asset:image src="avatars/avatar08_tn.png" alt="" title="Carrol Clark (carrol@example.com)" width="38" height="38"/>
                            </div>
                            <span class="caret"></span>
                        </div>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a href="pages-user_profile.html">User Profile</a></li>
                            <li><a href="#"><div>Check in</div></a></li>
                            <li><a href="login_page.html">Log Out</a></li>
                            </ul>
                    </div>
                </div>
            </header>
            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="home">Home</a></li>    
                    <li>Timesheet</li>    
                </ul>
            </nav>
            
            <!-- main content -->
            <div id="main_wrapper">
                <div class="container-fluid">
                    <!-- <div class="first"> -->
                    
                 

                  <div class="row">
                
                <div class="col-lg-12 discussions">
                    
                            
                                                       
                            <ul>
                                
                                <g:each in="${messages}"  var="i">
                                
                                <li>
                                    <div class="author">
                                        <img src="assets/img/avatar4.jpg" alt="avatar">
                                    </div>
                                    <div class="name">${i.nickname}</div>
                                    <div class="date">${i.date}</div>
                                    

                                    <div class="message">
                                        ${i.message}
                                    </div>  
                                </li>
                                </g:each>
                                
                            </ul>   
                                    
                           
                    
                </div><!--/col-->
            
            </div><!--/row-->           


                    
                    
                </div>
            </div>

            <!-- jQuery -->
       <script src="http://bootstrapmaster.com/live/simpliq2/assets/js/jquery-ui-1.10.3.custom.min.js"></script>
    <script src="http://bootstrapmaster.com/live/simpliq2/assets/js/jquery.ui.touch-punch.min.js"></script>
    <script src="http://bootstrapmaster.com/live/simpliq2/assets/js/jquery.sparkline.min.js"></script>
    <script src="assets/js/fullcalendar.min.js"></script>
    <!--[if lte IE 8]><script language="javascript" type="text/javascript" src="assets/js/excanvas.min.js"></script><![endif]-->
    <script src="http://bootstrapmaster.com/live/simpliq2/assets/js/fullcalendar.min.js"></script>
    <script src="http://bootstrapmaster.com/live/simpliq2/assets/js/jquery.flot.min.js"></script>
    <script src="http://bootstrapmaster.com/live/simpliq2/assets/js/jquery.flot.stack.min.js"></script>
    <script src="http://bootstrapmaster.com/live/simpliq2/assets/js/jquery.flot.time.min.js"></script>
    <script src="http://bootstrapmaster.com/live/simpliq2/assets/js/jquery.placeholder.min.js"></script>
    <script src="http://bootstrapmaster.com/live/simpliq2/assets/js/jquery-migrate-1.2.1.min.js"></script>
    <script src="http://bootstrapmaster.com/live/simpliq2/assets/js/bootstrap.min.js"></script>
    <script src="http://bootstrapmaster.com/live/simpliq2/assets/js/jquery-2.1.0.min.js"></script>
           
            
    </body>
</html>