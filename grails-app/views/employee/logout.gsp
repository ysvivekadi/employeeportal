<!DOCTYPE html>
<html >
  <head>
    <meta charset="UTF-8">
    <title>Sign-Up/Login Form</title>
   <!--  <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'> -->
    
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
        <asset:stylesheet src="style.css"/>

   
        <!--  <asset:stylesheet src="normalize.css"/>
          -->
    <!-- elegant icons -->
                <link href="http://yukon.tzdthemes.com/html/assets/icons/elegant/style.css" rel="stylesheet" media="all">
            <!-- elusive icons -->
                <link href="http://yukon.tzdthemes.com/html/assets/icons/elusive/css/elusive-webfont.css" rel="stylesheet" media="all">

         <asset:stylesheet src="normalize.css"/>
         
    <style type="text/css">
   div.relative {
    position: relative;
    left: 580px;
    bottom: -10px
    
}
h2.newhead{
  font-family: "Open Sans", arial-thin;
  font-size: 18px;
  colour: blue;
  position: relative;
  left: 535px;
  bottom:10px;
}
    </style>

  </head>

  <body>
    
      <div class="relative">
      <asset:image  src="logo.png" alt="site_logo" />
    
    </div>
    <h2 class="newhead">Sign in to continue to Employee Portal</h2>
<div>
  <span style="display:block; width:400px; position: relative; left: 485px;">
       
      <g:if test="${flash.message=='Error'}">
  <div class="alert alert-warning" role="alert" style="text-align:center ; padding:15px;margin-bottom:20px;border:1px solid transparent;border-radius:4px;border-top-color:#f7e1b5 ; color:black;background-color:#ff5555;border-color:#faebcc">
    <span class="el-icon-warning-sign bs_ttip" title="" data-original-title=".el-icon-warning-sign"></span>
   &nbsp;&nbsp;&nbsp;Username and Password donot match
  </div >
  </g:if> 
  </span> 
    </div>
    <div class="form">
      


      <ul class="tab-group">
        <li class="tab active"><a href="#signup"> <span class="el-icon-user bs_ttip" title="" data-original-title=".el-icon-user"></span> Manager</a></li>
        <li class="tab"><a href="#login"><i class="fa fa-users"></i> Employee</a></li>


      </ul>
      
      <div class="tab-content">
        <div id="signup"> 
          
          
          <form  action="./login" method="post">
          
            <div class="field-wrap">
            <label>

             <i class="fa fa-user"></i> UserName<span class="req"></span>

            </label>
            <input style="height:45px" required autocomplete="off" type="text" name="username"/>
          </div>
          
          <div class="field-wrap">
            <label>
              <i class="fa fa-key aria-hidden="true""></i> Password<span class="req"></span>
            </label>
            <input style="height:45px" type="password"required autocomplete="off" name="password"/>
          </div>
          
          
          


          <button class="button button-block" type="submit"/><span class="el-icon-unlock bs_ttip" title="" data-original-title=".el-icon-unlock"></span>
&nbsp;Log In</button>


          
          </form>

        </div>
        
        <div id="login">   
          
          
          <form  action="../employee/login" method="post">
          
            <div class="field-wrap">
            <label>

              <i class="fa fa-user"></i> UserName<span class="req"></span>

            </label>
            <input style="height:45px" type="text"required autocomplete="off" name="username"/>
          </div>
          
          <div class="field-wrap">
            <label>
               <i class="fa fa-key"></i> Password<span class="req"></span>
            </label>
            <input style="height:45px" type="password"required autocomplete="off" name="password"/>
          </div>
          
         
          


          <button class="button button-block" type="submit"/><span class="el-icon-unlock bs_ttip" title="" data-original-title=".el-icon-unlock"></span>
Log In</button>


          
          </form>

        </div>
        
      </div><!-- tab-content -->
      
</div> <!-- /form -->
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>

            <script type="text/javascript">

            $('.form').find('input, textarea').on('keyup blur focus', function (e) {
  
  var $this = $(this),
      label = $this.prev('label');

    if (e.type === 'keyup') {
      if ($this.val() === '') {
          label.removeClass('active highlight');
        } else {
          label.addClass('active highlight');
        }
    } else if (e.type === 'blur') {
      if( $this.val() === '' ) {
        label.removeClass('active highlight'); 
      } else {
        label.removeClass('highlight');   
      }   
    } else if (e.type === 'focus') {
      
      if( $this.val() === '' ) {
        label.removeClass('highlight'); 
      } 
      else if( $this.val() !== '' ) {
        label.addClass('highlight');
      }
    }

});

$('.tab a').on('click', function (e) {
  
  e.preventDefault();
  
  $(this).parent().addClass('active');
  $(this).parent().siblings().removeClass('active');
  
  target = $(this).attr('href');

  $('.tab-content > div').not(target).hide();
  
  $(target).fadeIn(600);
  
});
            </script>  
    
    
    
  </body>
</html>
