<!DOCTYPE html>
<html>
<html lang="en">
<head>
 <meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">

        <!-- bootstrap framework -->
        <link href="http://yukon.tzdthemes.com/html/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="all">
        
        <!-- icon sets -->
            <!-- elegant icons -->
                <link href="http://yukon.tzdthemes.com/html/assets/icons/elegant/style.css" rel="stylesheet" media="all">
            <!-- elusive icons -->
                <link href="http://yukon.tzdthemes.com/html/assets/icons/elusive/css/elusive-webfont.css" rel="stylesheet" media="all">
            <!-- flags -->
                <link rel="stylesheet" href="http://yukon.tzdthemes.com/html/assets/icons/flags/flags.css" media="all">
            <!-- scrollbar -->
                <link rel="stylesheet" href="http://yukon.tzdthemes.com/html/assets/lib/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css">


        <!-- google webfonts -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>

        <!-- main stylesheet -->
        <link href="http://yukon.tzdthemes.com/html/assets/css/main.min.css" rel="stylesheet" media="all" id="mainCss">

        <!-- print stylesheet -->
        <link href="http://yukon.tzdthemes.com/html/assets/css/print.css" rel="stylesheet" media="print">

        <!-- moment.js (date library) -->
        <script src="http://yukon.tzdthemes.com/html/assets/js/moment-with-langs.min.js"></script>

<!--  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
<link href="http://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
      <script src="http://code.jquery.com/jquery-1.10.2.js"></script>
      <script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script> -->

      <link rel="stylesheet" type="text/css" href="http://t1m0n.name/air-datepicker/dist/css/datepicker.min.css">

  <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
  <script type="text/javascript" src="http://t1m0n.name/air-datepicker/dist/js/datepicker.js">  </script>
  <script type="text/javascript" src="http://t1m0n.name/air-datepicker/dist/js/i18n/datepicker.en.js">  </script> 
  
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
  <script type="text/javascript" src="https://code.jquery.com/jquery-2.2.3.min.js"></script>
  <style>
input[type=text], select {
    width: 10%;
    padding: 2px 2px;
    margin: 8px 10px;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
}
input[type=submit] {
    width: 10%;
    background-color: #4CAF50;
    color: white;
    padding: 2px 2px;
    margin-left: 8px;
    border: none;
    border-radius: 4px;
    cursor: pointer;
}

input[type=submit]:hover {
    background-color: #45a049;
}

.first {
    border-radius: 5px;
    background-color: #f2f2f2;
    padding: 20px;
}

</style> 
</head>
<body>
   <script type="text/javascript">
    function myFunction() {
    alert("___" ${flash.message});
    }   
    $( document ).ready(function() {
      var $promo = $('#datepicker');
      $promo.datepicker({language: 'en'})
    });
  </script>
    <g:if test="${flash.message == 'from'}">
        <div class="alert alert-danger" role="alert">formdate cannot be empty</div> 
    </g:if>
    <g:elseif test="${flash.message == 'to'}">
        <div class="alert alert-danger" role="alert">Todate cannot be empty</div> 
    </g:elseif>
    <g:elseif test="${flash.message == 'task'}">
        <div class="alert alert-danger" role="alert">Task cannot be empty</div> 
    </g:elseif>
    <g:elseif test="${flash.message == 'des'}">
        <div class="alert alert-danger" role="alert">Description cannot be empty</div> 
    </g:elseif>
    <g:elseif test="${flash.message == 'work'}">
        <div class="alert alert-danger" role="alert">Work Hour cannot be empty</div> 
    </g:elseif>
     <g:elseif test="${flash.message}">
        <div class="alert alert-success" role="alert">Submitted</div> 
    </g:elseif>
  <div class="first">
    <g:form controller="Timesheet">
    <div>
     from date <input type='text' name="fromdate" id="datepicker" data-timepicker="true" class='datepicker-here' data-language='en' />
     to date <input type='text' name="todate" id="datepicker" data-timepicker="true" class='datepicker-here' data-language='en' />
     Project
     <select name="project" class="2">
      <g:each var="project" in="${proj}">
          <g:if test="${project.manager.id==employee.manager.id}">
          <option>${project.name}</option>
          </g:if>
      </g:each>
     </select>
     task
     <select name="task">
      <g:each var="tas" in="${task}">
          <g:if test="${tas.project.manager.id==employee.manager.id}">
          <option>${tas.task}</option>
          </g:if>
      </g:each>
     </select>
     description <input type="text" class="2" name="description"></input>
     work hour <input type="text" name="workhour"></input>
    </div>
     <input type="hidden" name="username" value="${employee.username}"></input>

        <g:actionSubmit value="Submit" action="save" onclick="myfunction()">   
        </g:actionSubmit>
         <!-- <input type="submit" value="Submit"> -->
    </g:form>
  </div>  
</body>
</html>