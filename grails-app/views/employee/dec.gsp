<!DOCTYPE html>
<html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="layout" content="etemplate">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">        
    <style>   
        input[type=text]{
            width: 80%; 
            padding: 6px 20px;
            border: 1px solid #ccc;
            box-sizing: border-box;
        }
        .wrapper {
            text-align: center;
        }

        .button {
            position: absolute;
            top: 50%;
        }
    </style> 
    <script>
                              $(function() {
                              $( "#datepicker1" ).kendoDatepicker();
                                });
                            </script>

    <!-- <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"> -->
  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
   --><!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  <link rel="stylesheet" href="/resources/demos/style.css"> -->
      
  <style>
  #middle {
  float:right;
    
  }
  #nav {
      
    
      width:48%;
      float:left;
  }
  #section {
      width:48%;
      float:right;
      
      
      }

  #taba {

  padding:8px;
  background-color:#184876;
  font-size:18px;
  color:white;}
  #header {
      background-color: skyblue;
      color:black;
      
      padding:5px;
  }
  #header1 {
      background-color: #5D231F;
      color:white;
      
      padding:5px;
  }
  #header2 {
      background-color: #f1f1c1;
      
      
      padding:5px; }
     
  #logo {

    background-color: #03152B;
    padding:19px;
    margin-left: 11px;
    margin-right: 11px;
    margin-top: 20px;
  }

  </style>

    <meta http-equiv="Content-type" content="text/html; charset=utf-8"/>
    <title>DHTMLX Gantt Chart </title>

    
        <!-- <asset:javascript src="application.js"/> -->
        <asset:javascript src="dhtmlxcommon.js"/>
        <asset:javascript src="dhtmlxgantt48b2.js?v=4.0"/>
        <asset:javascript src="dhtmlxgantt_marker.js"/>
        <asset:javascript src="api.js"/>
        <asset:javascript src="dhtmlxmenu.js"/>
        <asset:javascript src="dhtmlxmenu_deprecated.js"/>
        <asset:javascript src="dhtmlxmenu_ext.js"/>
        <!-- <asset:stylesheet src="application.css"/> -->


    <link rel="stylesheet" href="${resource(dir:'css',file:'dhtmlxmenu_dhx_terrace.css')}" type="text/css" media="screen"title="no title" charset="utf-8">
<link rel="stylesheet" href="${resource(dir:'css',file:'dhtmlxgantt_skyblue48b2.css')}" type="text/css" media="screen"
          title="no title" charset="utf-8">
    <style type="text/css" media="screen">
        html, body {
            
            height: 100%;
            
        }
        .nested_task .gantt_add{
        display: none !important;
        }
        .weekend {
            background: #f4f7f4 !important;
        }

        .gantt_selected .weekend {
            background: #FFF3A1 !important;
        }

        .controls_bar {
            border-top: 2px solid #bababa;
            border-bottom: 2px solid #bababa;
            clear: both;
            margin-top: 90px;
            height: 28px;
            background: #f1f1f1;
            color: #494949;
            font-family: Arial, sans-serif;
            font-size: 13px;
            padding-left: 10px;
            line-height: 25px
        }
        .status_line{
            background-color: #0ca30a;
        }
    </style>

<script>
    function modSampleHeight() {

        var headHeight = 100;
        var sch = document.getElementById("gantt_here");
        sch.style.height = (parseInt(document.body.offsetHeight) - headHeight) + "px";
        var contbox = document.getElementById("contbox");
        contbox.style.width = (parseInt(document.body.offsetWidth) - 300) + "px";

        gantt.setSizes();
    }
</script>



</head>

<body class="side_menu_active side_menu_expanded" onresize="modSampleHeight()" onload="modSampleHeight()">
          <script type="text/javascript">
            function myFunction() {
            alert("___" ${flash.message});
            }   
            $( document ).ready(function() {
              var $promo = $('#datepicker');
              $promo.datepicker({language: 'en'})
            });
          </script>
        <div id="page_wrapper">
            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="home">Home</a></li>    
                    <li>Table For Declaration</li>    
                </ul>
            </nav>
            <!-- id="datepicker" class='datepicker-here' data-language='en' -->
            <!-- main content -->
          <div id="main_wrapper">
            <div style="width:99%" class="container">
                      <g:if test="${flash.message=='SUCCESS! Rentdeatails submitted '}">
                              <div class="alert alert-success" role="alert" style="display: block"><strong>${flash.message}</strong></div>
                      </g:if>
                      <g:elseif test="${flash.message=='FAILED! Rentdetails not submitted'}">
                              <div class="alert alert-danger" role="alert" style="display: block"><strong>${flash.message}</strong></div>
                      </g:elseif> 
                      <g:elseif test="${flash.message=='SUCCESS! Billdetails submitted '}">
                              <div class="alert alert-success" role="alert" style="display: block"><strong>${flash.message}</strong></div>
                      </g:elseif>
                      <g:elseif test="${flash.message=='FAILED! Billdetails not submitted'}">
                              <div class="alert alert-danger" role="alert" style="display: block"><strong>${flash.message}</strong></div>
                      </g:elseif>
                      <g:elseif test="${flash.message=='SUCCESS! Eduloans submitted '}">
                              <div class="alert alert-success" role="alert" style="display: block"><strong>${flash.message}</strong></div>
                      </g:elseif>
                      <g:elseif test="${flash.message=='FAILED! Eduloans not submitted'}">
                              <div class="alert alert-danger" role="alert" style="display: block"><strong>${flash.message}</strong></div>
                      </g:elseif>
                      <g:elseif test="${flash.message=='SUCCESS! Homeloan submitted '}">
                              <div class="alert alert-success" role="alert" style="display: block"><strong>${flash.message}</strong></div>
                      </g:elseif>
                      <g:elseif test="${flash.message=='FAILED! Homeloan not submitted'}">
                              <div class="alert alert-danger" role="alert" style="display: block"><strong>${flash.message}</strong></div>
                      </g:elseif>
                      <g:elseif test="${flash.message=='SUCCESS! Investment80c submitted '}">
                              <div class="alert alert-success" role="alert" style="display: block"><strong>${flash.message}</strong></div>
                      </g:elseif>
                      <g:elseif test="${flash.message=='FAILED! Investment80c not submitted'}">
                              <div class="alert alert-danger" role="alert" style="display: block"><strong>${flash.message}</strong></div>
                      </g:elseif>
                      <g:elseif test="${flash.message=='SUCCESS! Lifeins submitted '}">
                              <div class="alert alert-success" role="alert" style="display: block"><strong>${flash.message}</strong></div>
                      </g:elseif>
                      <g:elseif test="${flash.message=='FAILED! Lifeins not submitted'}">
                              <div class="alert alert-danger" role="alert" style="display: block"><strong>${flash.message}</strong></div>
                      </g:elseif>
                      <g:elseif test="${flash.message=='SUCCESS! Medins submitted '}">
                              <div class="alert alert-success" role="alert" style="display: block"><strong>${flash.message}</strong></div>
                      </g:elseif>
                      <g:elseif test="${flash.message=='FAILED! Medins not submitted'}">
                              <div class="alert alert-danger" role="alert" style="display: block"><strong>${flash.message}</strong></div>
                      </g:elseif>
                      <g:elseif test="${flash.message=='SUCCESS! Mtreatment submitted '}">
                              <div class="alert alert-success" role="alert" style="display: block"><strong>${flash.message}</strong></div>
                      </g:elseif>
                      <g:elseif test="${flash.message=='FAILED! Mtreatment not submitted'}">
                              <div class="alert alert-danger" role="alert" style="display: block"><strong>${flash.message}</strong></div>
                      </g:elseif>
                      <g:elseif test="${flash.message=='SUCCESS! Nps80c submitted '}">
                              <div class="alert alert-success" role="alert" style="display: block"><strong>${flash.message}</strong></div>
                      </g:elseif>
                      <g:elseif test="${flash.message=='FAILED! Nps80c not submitted'}">
                              <div class="alert alert-danger" role="alert" style="display: block"><strong>${flash.message}</strong></div>
                      </g:elseif>
                      <g:elseif test="${flash.message=='SUCCESS! Penfund submitted '}">
                              <div class="alert alert-success" role="alert" style="display: block"><strong>${flash.message}</strong></div>
                      </g:elseif>
                      <g:elseif test="${flash.message=='FAILED! Penfund not submitted'}">
                              <div class="alert alert-danger" role="alert" style="display: block"><strong>${flash.message}</strong></div>
                      </g:elseif>

                      
                     <div id="taba" >Declaration for income tax calculation for the tax year April 16-March 17<br>
                     </div>
                     <br>
                     <ul class="nav nav-pills">
                        <li class="active"><a data-toggle="pill" href="#p"><b><font size="3.8" >Bill Details</font></b></a></li>
                        <li><a data-toggle="pill" href="#k"><b><font size="3.8" >Rent Details</font></b></a></li>
                        <li><a data-toggle="pill" href="#menu3"><b><font size="3.8" >Medical Insurance </font></b></a></li>
                        <li><a data-toggle="pill" href="#d"><b><font size="3.8" >Home Loan</font></b></a></li>
                        <li><a data-toggle="pill" href="#o"><b><font size="3.8" >Otherdeb</font></b></a></li>
                        <li><a data-toggle="pill" href="#g"><b><font size="3.8" >Investments</font></b></a></li>
                     </ul>
                     <div class="tab-content">
                        <div id="p" class="tab-pane fade in active">
                        
                           <br>
                           <br>
                           <div class="form-group">
                              <g:form class="form-inline" role="form" controller="billdetails">
                                 <h4><font color="#4A0404"><b><u>Medical Allowance(Rs)</u></b></font></h4>
                                 <br>
                                  
                                 <input type="hidden" name="username" value="${emp.username}">
                                 <input class="form-control" id="focusedInput"placeholder="₹" type="int" name="mallow">
                                 <g:actionSubmit  action="save" class="btn btn-success"value="Submit" />
                                 <br>
                              </g:form>
                           </div>
                           <br><br>

                          
                              <table   class="table table-bordered"  >
                                 <thead>
                                    <tr>
                                       <th id=header1>Medical Allowance</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <input type="hidden" name="username" value="${emp.username}">
                                    <g:each in="${billdetailss}" var="billdetails"  >
                                       <tr>
                                          <td id="header2">${billdetails.mallowance}</td>
                                       </tr>
                                    </g:each>
                                 </tbody>
                              </table>
                           
                        </div>
                        <div id="k" class="tab-pane fade">
                           <h4><b><u><font color="#4A0404">Rent details for house rent allowance exemption</font></u></b></h4>
                           
                           <g:form class="form-inline" role="form" controller="Rentdetails">
                              <table class="table table-bordered" >
                                 <thead>
                                    <tr>
                                       <th id=header>From date</th>
                                       <th id=header>ToDate</th>
                                       <th id=header>Total rent </th>
                                       <th id=header>Location</th>
                                       <th id=header>Is metro</th>
                                    </tr>
                                 </thead>
                                 <div class="form-group">
                                    <tbody id="abc">
                                       <tr>
                                          <input type="hidden" name="username" value="${emp.username}">
                                          <td>  <input class="form-control" type="date" name="fromdate" value="0000-00-00"></td>
                                          <td><input class="form-control" id="datepicker1"  type="date" name="todate" value="0000-00-00"></td>
                                          <td> <input style="width:75%" class="form-control" id="focusedInput" type="int" placeholder="₹" name="totalrent" value="0"></td>
                                          <td> <input style="width:75%"class="form-control" id="focusedInput" type="text" name="location" value="-----"></td>
                                          <td><input style="width:95%"class="form-control" id="focusedInput" type="checkbox" name="ismetro" value="checked"></td>
                                       </tr>
                                    </tbody>
                                 </div>
                              </table>
                              <g:actionSubmit action="save" class="btn btn-success" value="Submit"/>
                           </g:form>
                           <br><br>
                    
                              <table  class="table table-bordered"  >
                                 <thead>
                                    <tr>
                                       <th id=header1>Id</th>
                                       <th id=header1>From Date</th>
                                       <th id=header1>To Date</th>
                                       <th id=header1>Total Rent </th>
                                       <th id=header1>Locations </th>
                                       <th id=header1>Is Metro </th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <input type="hidden" name="username" value="${emp.username}">
                                    <g:each in="${rentdetailss}" var="rentdetails"  >
                                       <tr>
                                          <td id="header2">${rentdetails.id}</td>
                                          <td id="header2">${rentdetails.from_date}</td>
                                          <td id="header2">${rentdetails.to_date}</td>
                                          <td id="header2">${rentdetails.total_rent}</td>
                                          <td id="header2">${rentdetails.location}</td>
                                          <td id="header2">${rentdetails.ismetro}</td>
                                       </tr>
                                    </g:each>
                                 </tbody>
                              </table>
                          
                        </div>
                        <div id="menu3" class="tab-pane fade">
                           <h4><b><u><font color="#4A0404">Medical insurance premium(self and family)</font></u></b></h4>
                           <g:form class="form-inline" role="form" controller="medins">
                              <table id="gaur" class="table table-bordered" >
                                 <thead>
                                    <tr>
                                      
                                       <th id=header>Policy No</th>
                                       <th id=header>Annual Premium(‎₹)</th>
                                    </tr>
                                 </thead>
                                 <div class="form-group">
                                    <tbody>
                                       <tr>
                                          <!--  <label for="focusedInput">medical allowance(Rs)</label> -->
                                          <input type="hidden" name="username" value="${emp.username}">
                                          
                                          <td><input class="form-control" id="focusedInput" type="int" name="policyno" value="0"></td>
                                          <td>  <input class="form-control" id="focusedInput" type="int"placeholder="₹" name="apremium" value="0"></td>
                                          <br>
                                       </tr>
                                    </tbody>
                                 </div>
                              </table>
                              <br>
                              <h4><b><u><font color="#4A0404">Medical insurance premium(Parents)</font></u></b></h4>
                              <table id="gaur" class="table table-bordered" >
                                 <thead>
                                    <tr>
                                       
                                       <th id=header>Policy No</th>
                                       <th id=header>Annual Premium(Rs)</th>
                                    </tr>
                                 </thead>
                                 <div class="form-group">
                                    <tbody>
                                       <tr>
                                    
                                          <td><input class="form-control" id="focusedInput" type="int" name="policyno2" value="0"></td>
                                          <td><input class="form-control" id="focusedInput" type="int" placeholder="₹" name="apremium2" value="0"></td>
                                          <br>
                                       </tr>
                                    </tbody>
                                 </div>
                              </table>
                              <g:actionSubmit action="save"class="btn btn-success" value="Submit"/>
                           </g:form>
                           <br><br>
                        
                              <div id="nav">
                                 <h4><font color="brown" ><b><u>For Self and Family</u></b></font></h4>
                                 <table   class="table table-bordered"  >
                                    <thead>
                                       <tr>
                                          <th id="header1">Id</th>
                                          <th id="header1">Policy No</th>
                                          <th id="header1">Annual Premium(Rs)</th>
                                       </tr>
                                    </thead>
                                    <tbody>
                                       <input type="hidden" name="username" value="${emp.username}">
                                       <g:each in="${medinss}" var="medins" >
                                          <tr>
                                             <td id="header2">${medins.id}</td>
                                             <td id="header2">${medins.policynumber1}</td>
                                             <td id="header2">${medins.annualpremium1}</td>
                                          </tr>
                                       </g:each>
                                    </tbody>
                                 </table>
                              </div>
                              <div id="section">
                                 <h4><font color="brown" ><b><u>For Parents</u></b></font></h4>
                                 <table   class="table table-bordered"  >
                                    <thead>
                                       <tr>
                                          <th id=header1>Id</th>
                                          <th id=header1>Policy No</th>
                                          <th id=header1>Annual Premium(Rs)</th>
                                       </tr>
                                    </thead>
                                    <tbody>
                                       <input type="hidden" name="username" value="${emp.username}">
                                       <g:each in="${medinss}" var="medins" >
                                          <tr>
                                             <td id="header2">${medins.id}</td>
                                             <td id="header2">${medins.policynumber2}</td>
                                             <td id="header2">${medins.annualpremium2}</td>
                                          </tr>
                                       </g:each>
                                    </tbody>
                                 </table>
                              
                           </div>
                        </div>
                        <div id="d" class="tab-pane fade">
                           <h4><b><u><font color="#4A0404">Income from house property</font></u></b></h4>
                           <g:form class="form-inline" role="form" controller="homeloan">
                              <table  class="table table-bordered" >
                                 <thead>
                                    <tr>
                                       
                                       <th id=header>Self Occupied</th>
                                       <th id=header>Annual rent recieved </th>
                                       <th id=header>Municipal taxes paid</th>
                                       <th id=header>Home loan interest (before)</th>
                                       <th id=header>Home loan interest (after)</th>
                                       <th id=header>Home loan sanction date </th>
                                       <th id=header>Is home first loan</th>
                                       <th id=header>Home loan amount</th>
                                       <th id=header>Cost of Home</th>
                                    </tr>
                                 </thead>
                                 <div class="form-group">
                                    <tbody>
                                       <tr>
                                          <!--  <label for="focusedInput">medical allowance(Rs)</label> -->
                                          <input type="hidden" name="username" value="${emp.username}">
                                  
                                          <td><input style="width:70%"class="form-control" id="focusedInput" type="checkbox" name="soccupied"></td>
                                          <td style="width:8%">  <input style="width:99%"class="form-control" id="focusedInput" type="int" placeholder="₹"name="arent" value="0"></td>
                                          <td style="width:8%">  <input style="width:99%" class="form-control" id="focusedInput" type="int" placeholder="₹" name="mtpaid" value="0"></td>
                                          <td style="width:8%">  <input style="width:99%" class="form-control" id="focusedInput" type="int" name="hlib" value="0"></td>
                                          <td style="width:8%"><input style="width:99%" class="form-control" id="focusedInput" type="int" name="hlia" value="0"></td>
                                          <td style="width:8%"><input style="width:99%" class="form-control"  id="datepicker3" class="datepicker-here" type="date" name="hlsd" value="0000-00-00"></td>
                                          <td><input style="width:70%" class="form-control" id="focusedInput" type="checkbox" name="ihfl"></td>
                                          <td><input style="width:70%" class="form-control" id="focusedInput" type="int" placeholder="₹"name="hlamount" value="0"></td>
                                          <td><input style="width:70%" class="form-control" id="focusedInput" type="int" placeholder="₹" name="cohome" value="0"></td>
                                          <br>
                                       </tr>
                                    </tbody>
                                 </div>
                              </table>
                              <g:actionSubmit action="save" class="btn btn-success" value="Submit"/>
                           </g:form>
                           <br><br>
                      
                              <table   class="table table-bordered"  >
                                 <thead>
                                    <tr>
                                       <th id=header1>Id </th>
                                       <th id=header1>Self Occupied</th>
                                       <th id=header1>Annual rent recieved </th>
                                       <th id=header1>Municipal taxes paid</th>
                                       <th id=header1>Home loan interest (before)</th>
                                       <th id=header1>Home loan interest (after)</th>
                                       <th id=header1>Home loan sanction date </th>
                                       <th id=header1>Is home first loan</th>
                                       <th id=header1>Home loan amount</th>
                                       <th id=header1>Cost of Home</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <input type="hidden" name="username" value="${emp.username}">
                                    <g:each in="${homeloans}" var="homeloan"  >
                                       <tr>
                                          <td id="header2">${homeloan.id}</td>
                                          <td id="header2">${homeloan.soccupied}</td>
                                          <td id="header2">${homeloan.arent}</td>
                                          <td id="header2">${homeloan.mtpaid}</td>
                                          <td id="header2">${homeloan.hli1}</td>
                                          <td id="header2">${homeloan.hli2}</td>
                                          <td id="header2">${homeloan.hldate}</td>
                                          <td id="header2">${homeloan.ifloan}</td>
                                          <td id="header2">${homeloan.hlamount}</td>
                                          <td id="header2">${homeloan.cohome}</td>
                                       </tr>
                                    </g:each>
                                 </tbody>
                              </table>
                           
                           
                        </div>
                        <div id="o" class="tab-pane fade">
              
                              <br>
                              <ul class="nav nav-tabs">
                                 <li class="active"><a data-toggle="tab" href="#o1"><i><b><font size="3.4" color="#9C0602">Medical Treatment</font></b></i></a></li>
                                 <li><a data-toggle="tab" href="#o2"><i><b><font size="3.4" color="#9C0602" >Eduloan</font></b></i></a></li>
                                 <li><a data-toggle="tab" href="#o3"><i><b><font size="3.4" color="#9C0602" >Donations </font></b></i></a></li>
                              </ul>
                              <div class="tab-content">
                                 <div id="o1" class="tab-pane fade in active">
                                    <br>
                                    <br>
                                    <g:form class="form-inline" role="form" controller="mtreatment">
                                       <div class="form-group">
                                          <label for="focusedInput"><b>Total Expenditure(Rs)</b></label>
                                          <input type="hidden" name="username"  value="${emp.username}">
                                          <input class="form-control" id="focusedInput" type="int" placeholder="₹" name="texpend" value="0">
                                          <br>
                                          <br>
                                          <table  class="table table-bordered">
                                             <thead>
                                                <tr>
                                                   <th id="header">Resident ( <= 60 years)</th>
                                                   <th id="header">Resident (between 60 and 80 years) </th>
                                                   <th id="header">Resident ( > 80 years)</th>
                                                </tr>
                                             </thead>
                                             <tbody>
                                                <tr>
                                                   <td><input style="width:98%"class="form-control" id="focusedInput" type="checkbox" name="res1"></td>
                                                   <td><input style="width:98%"class="form-control" id="focusedInput" type="checkbox" name="res2"> </td>
                                                   <td><input style="width:98%" class="form-control" id="focusedInput" type="checkbox" name="res3"> </td>
                                                </tr>
                                             </tbody>
                                          </table>
                                       </div>
                                       <br>
                                       <g:actionSubmit action="save" class="btn btn-success"value="Submit" />
                                       <br>
                                    </g:form>
                                    <br><br>
                              
                                       <table class="table table-bordered"  >
                                          <thead>
                                             <tr>
                                                <td id="header1">Id</td>
                                                <th id=header1>Total expenditure</th>
                                                <th id="header1">Resident ( <= 60 years)</th>
                                                <th id="header1">Resident (between 60 and 80 years) </th>
                                                <th id="header1">Resident (>= 80 years)</th>
                                             </tr>
                                          </thead>
                                          <tbody>
                                             <input type="hidden" name="username" value="${emp.username}">
                                             <g:each in="${mtreatments}" var="mtreatment"  >
                                                <tr>
                                                   <td id="header2">${mtreatment.id}</td> 
                                                   <td id="header2">${mtreatment.texpend}</td>
                                                   <td id="header2">${mtreatment.res1}</td>
                                                   <td id="header2">${mtreatment.res2}</td>
                                                   <td id="header2">${mtreatment.res3}</td>
                                                </tr>
                                             </g:each>
                                          </tbody>
                                       </table>
                                    
                                 </div>
                                 <div id="o2" class="tab-pane fade">
                                    <h4><b><u><font color="#4A0404">Deduction for edu loan interest under section 80E</font></u></b></h4>
                                    <g:form class="form-inline" role="form" controller="eduloan">
                                       <table id="gaur" class="table table-bordered" >
                                          <thead>
                                             <tr>
                                                
                                                <th id=header>Tax Rebate</th>
                                                <th id=header>Amount(Rs) </th>
                                             </tr>
                                          </thead>
                                          <div class="form-group">
                                             <tbody>
                                                <tr>
                                                   <!--  <label for="focusedInput">medical allowance(Rs)</label> -->
                                                   <input type="hidden" name="username" value="${emp.username}">
                                            
                                                   <td><input class="form-control" id="focusedInput" type="text"  name="trebe"value="-"></td>
                                                   <td>  <input class="form-control" id="focusedInput" type="int" placeholder="₹"name="tamounte" value="0"></td>
                                                   <br>
                                                </tr>
                                             </tbody>
                                          </div>
                                       </table>
                                       <g:actionSubmit action="save" class="btn btn-success" value="Submit"/>
                                    </g:form>
                                    <br><br>
                              
                                       <table  class="table table-bordered"  >
                                          <thead>
                                             <tr>
                                                <th id=header1>Id</th>
                                                <th id=header1>Tax Rebate</th>
                                                <th id=header1>Amount(Rs) </th>
                                             </tr>
                                          </thead>
                                          <tbody>
                                             <input type="hidden" name="username" value="${emp.username}">
                                             <g:each in="${eduloans}" var="eduloan"  >
                                                <tr>
                                                   <td id="header2">${eduloan.id}</td>
                                                   <td id="header2">${eduloan.treb}</td>
                                                   <td id="header2">${eduloan.tamount}</td>
                                                </tr>
                                             </g:each>
                                          </tbody>
                                       </table>
                                    
                                 </div>
                                 <div id="o3" class="tab-pane fade">
                                    <h4><b><u><font color="#4A0404">Donation under section 80G </font></u></b></h4>
                                    <g:form class="form-inline" role="form" controller="donations">
                                       <table id="gaur" class="table table-bordered" >
                                          <thead>
                                             <tr>
                                                
                                                <th id=header>Tax Rebate</th>
                                                <th id=header>Amount(Rs) </th>
                                             </tr>
                                          </thead>
                                          <div>
                                             <tbody>
                                                <tr>
                                                   <!--  <label for="focusedInput">medical allowance(Rs)</label> -->
                                                   <input type="hidden" name="username" value="${emp.username}">
                                                   
                                                   <td><input class="form-control" id="focusedInput" type="text" name="trebd" value="-"></td>
                                                   <td>  <input class="form-control" id="focusedInput" type="int" placeholder="₹" name="tamountd" value="0"></td>
                                                   <br>
                                                </tr>
                                             </tbody>
                                          </div>
                                       </table>
                                       <g:actionSubmit action="save" class="btn btn-success" value="Submit"/>
                                    </g:form>
                                    <br><br>
                                    
                                       <table   class="table table-bordered"  >
                                          <thead>
                                             <tr>
                                                <th id=header1>Id</th>
                                                <th id=header1>Tax Rebate</th>
                                                <th id=header1>Amount(Rs) </th>
                                             </tr>
                                          </thead>
                                          <tbody>
                                             <input type="hidden" name="username" value="${emp.username}">
                                             <g:each in="${donationss}" var="donations"  >
                                                <tr>
                                                   <td id="header2">${donations.id}</td>
                                                   <td id="header2">${donations.treb}</td>
                                                   <td id="header2">${donations.tamount}</td>
                                                </tr>
                                             </g:each>
                                          </tbody>
                                       </table>
                                    
                                 
                              </div>
                           </div>
                        </div>
                        <div id="g" class="tab-pane fade">
                           
                              <br>
                              <br>
                              <ul class="nav nav-tabs">
                                 <li class="active"><a data-toggle="tab" href="#i1"><i><b><font size="3.4" color="#9C0602">Life Insurance</font></b></i></a></li>
                                 <li><a data-toggle="tab" href="#i2"><i><b><font size="3.4" color="#9C0602">Investments-80C</font></b></i></a></li>
                                 <li><a data-toggle="tab" href="#i3"><i><b><font size="3.4"color="#9C0602" >Pension fund-80CCC </font></b></i></a></li>
                                 <li><a data-toggle="tab" href="#i4"><i><b><font size="3.4" color="#9C0602">NPS-80CCD </font></b></i></a></li>
                              </ul>
                              <div class="tab-content">
                                 <div id="i1" class="tab-pane fade in active">
                                    <g:form class="form-inline" role="form" controller="lifeins">
                                       <table  class="table table-bordered" >
                                          <thead>
                                             <tr>
                                                
                                                <th id=header>Policy No</th>
                                                <th id=header>Issue Date </th>
                                                <th id=header>Insured Person</th>
                                                <th id=header>Relationship</th>
                                                <th id=header>Sum Assured(Rs)</th>
                                                <th id=header>Annual Premium </th>
                                                <th id=header>Eligible Deduction</th>
                                             </tr>
                                          </thead>
                                          <div class="form-group">
                                             <tbody>
                                                <tr>
                                                   <!--  <label for="focusedInput">medical allowance(Rs)</label> -->
                                                   <input  type="hidden" name="username" value="${emp.username}">
                                                  
                                                   <td><input style="width:70%"class="form-control" id="focusedInput" type="text" name="pno"value="0" ></td>
                                                   <td  style="width:1%"><input style="width:70%"class="form-control" id="datepicker4" type="date" name="idate" value="0000-00-00"></td>
                                                   <td><input style="width:70%"class="form-control" id="focusedInput" type="text" name="iperson" value="-"></td>
                                                   <td><input style="width:70%" class="form-control" id="focusedInput" type="text" name="rship" value="-"></td>
                                                   <td><input style="width:70%"class="form-control" id="focusedInput" type="int" placeholder="₹" name="suma"value="0"></td>
                                                   <td><input style="width:70%"class="form-control" id="focusedInput" type="int" placeholder="₹"name="annualp"value="0"></td>
                                                   <td><input style="width:70%"class="form-control" id="focusedInput" type="int"placeholder="₹"  name="eligibled"value="0"></td>
                                                   <br>
                                                </tr>
                                             </tbody>
                                          </div>
                                       </table>
                                       <g:actionSubmit action="save" class="btn btn-success" value="Submit"/>
                                    </g:form>
                                    <br><br>
                                    
                                       <table  class="table table-bordered"  >
                                          <thead>
                                             <tr>
                                                <th id=header1>Id </th>
                                                <th id=header1>Policy No</th>
                                                <th id=header1>Issue Date </th>
                                                <th id=header1>Insured Person</th>
                                                <th id=header1>Relationship</th>
                                                <th id=header1>Sum Assured(Rs)</th>
                                                <th id=header1>Annual Premium </th>
                                                <th id=header1>Eligible Deduction</th>
                                             </tr>
                                          </thead>
                                          <tbody>
                                             <input type="hidden" name="username" value="${emp.username}">
                                             <g:each in="${lifeinss}" var="lifeins"  >
                                                <tr>
                                                   <td id="header2">${lifeins.id}</td>
                                                   <td id="header2">${lifeins.polno}</td>
                                                   <td id="header2">${lifeins.idate}</td>
                                                   <td id="header2">${lifeins.iperson}</td>
                                                   <td id="header2">${lifeins.rships}</td>
                                                   <td id="header2">${lifeins.sassured}</td>
                                                   <td id="header2">${lifeins.anpre}</td>
                                                   <td id="header2">${lifeins.ededuct}</td>
                                                </tr>
                                             </g:each>
                                          </tbody>
                                       </table>
                                    
                                 </div>
                                 <div id="i2" class="tab-pane fade">
                                    <h4><b><u><font color="#4A0404">Investments u/s 80C</font></u></b></h4>
                                    <g:form class="form-inline" role="form" controller="investment80c">
                                       <table id="gaur" class="table table-bordered" >
                                          <thead>
                                             <tr>
                                                
                                                <th id=header>Tax Rebate</th>
                                                <th id=header>Amount(Rs) </th>
                                             </tr>
                                          </thead>
                                          <div class="form-group">
                                             <tbody>
                                                <tr>
                                                   <!--  <label for="focusedInput">medical allowance(Rs)</label> -->
                                                   <input type="hidden" name="username" value="${emp.username}">
                                                
                                                   <td><input class="form-control" id="focusedInput" type="text"   name="trebii"value="-"></td>
                                                   <td>  <input class="form-control" id="focusedInput" type="int" placeholder="₹" name="tamountii"value="0"></td>
                                                   <br>
                                                </tr>
                                             </tbody>
                                          </div>
                                       </table>
                                       <g:actionSubmit action="save" class="btn btn-success" value="Submit"/>
                                    </g:form>
                                    <br><br>
                                    
                                       <table   class="table table-bordered"  >
                                          <thead>
                                             <tr>
                                                <th id=header1>Id</th>
                                                <th id=header1>Tax Rebate</th>
                                                <th id=header1>Amount(Rs) </th>
                                             </tr>
                                          </thead>
                                          <tbody>
                                             <input type="hidden" name="username" value="${emp.username}">
                                             <g:each in="${investment80cs}" var="investment80c"  >
                                                <tr>
                                                   <td id="header2">${investment80c.id}</td>
                                                   <td id="header2">${investment80c.treb}</td>
                                                   <td id="header2">${investment80c.tamount}</td>
                                                </tr>
                                             </g:each>
                                          </tbody>
                                       </table>
                                    
                                    
                                 </div>
                                 <div id="i3" class="tab-pane fade">
                                 <h4><u>Pension Fund</u></h4>
                                 <g:form class="form-inline" role="form" controller="penfund">
                                 <table id="gaur" class="table table-bordered" >
                                 <thead>
                                 <tr>
                                
                                 <th id=header>Tax Rebate</th>
                                 <th id=header>Amount(Rs) </th>
                                 </tr>
                                 </thead>
                                 <div class="form-group">
                                 <tbody>
                                 <tr>
                                 <!--  <label for="focusedInput">medical allowance(Rs)</label> -->
                                 <input type="hidden" name="username" value="${emp.username}">
                              
                                 <td><input class="form-control" id="focusedInput" type="text" name="trebip"value="-"></td>
                                 <td>  <input class="form-control" id="focusedInput" type="int" placeholder="₹" name="tamountip"value="0"></td>
                                 <br>
                                 </tr>
                                 </tbody>
                                 </div>
                                 </table>
                                 <g:actionSubmit action="save" class="btn btn-success" value="Submit"/>
                                 </g:form>
                                 <br><br>
                                 
                                 <table   class="table table-bordered"  >
                                 <thead>
                                 <tr>
                                 <th id=header1>Id</th>
                                 <th id=header1>Tax Rebate</th>
                                 <th id=header1>Amount(Rs) </th>
                                 </tr>
                                 </thead>
                                 <tbody>
                                 <input type="hidden" name="username" value="${emp.username}">
                                 <g:each in="${penfunds}" var="penfund"  >
                                 <tr>
                                 <td id="header2">${penfund.id}</td>
                                 <td id="header2">${penfund.treb}</td>
                                 <td id="header2">${penfund.tamount}</td>
                                 </tr>
                                 </g:each>
                                 </tbody>
                                 </table>                          
                                 
                                 </div>
                                 <div id="i4" class="tab-pane fade">
                                 <h4><b><u><font color="#4A0404">NPS-80CCD</font></u></b></h4>
                                 <g:form class="form-inline" role="form" controller="nps80c">  
                                 <table id="gaur" class="table table-bordered" >
                                 <thead>
                                 <tr>
                                 
                                 <th id=header>Tax Rebate</th>
                                 <th id=header>Amount(Rs) </th>
                                 </tr>
                                 </thead>
                                 <div class="form-group">
                                 <tbody>
                                 <tr>
                                 <!--  <label for="focusedInput">medical allowance(Rs)</label> -->
                                 <input type="hidden" name="username" value="${emp.username}">
                                
                                 <td><input class="form-control" id="focusedInput" type="text" name="trebin"value="-"></td>
                                 <td>  <input class="form-control" id="focusedInput" type="int" placeholder="₹" name="tamountin"value="0"></td>
                                 <br>
                                 </tr>
                                 </tbody>
                                 </div>
                                 </table>
                                 <g:actionSubmit action="save" class="btn btn-success" value="Submit"/>
                                 </g:form>
                                 <br><br>
                                 
                                 <table   class="table table-bordered"  >
                                 <thead>
                                 <tr>
                                 <th id=header1>Id</th>
                                 <th id=header1>Tax Rebate</th>
                                 <th id=header1>Amount(Rs) </th>
                                 </tr>
                                 </thead>
                                 <tbody>
                                 <input type="hidden" name="username" value="${emp.username}">
                                 <g:each in="${nps80cs}" var="nps80c"  >
                                 <tr>
                                 <td id="header2">${nps80c.id}</td>
                                 <td id="header2">${nps80c.treb}</td>
                                 <td id="header2">${nps80c.tamount}</td>
                                 </tr>
                                 </g:each>
                                 </tbody>
                                 </table>                          
                                 
                                 </div>
                              
                           </div>
                        </div>
                     </div>
            </div>
          </div>
        </div>
        <!-- page specific plugins -->
       <!--  <script src="http://yukon.tzdthemes.com/html/assets/lib/select2/select2.min.js"></script> -->
            <!-- validation (parsley.js) -->
            <!-- <script src="http://yukon.tzdthemes.com/html/assets/js/parsley.config.js"></script>
            <script src="http://yukon.tzdthemes.com/html/assets/lib/parsley/dist/parsley.min.js"></script>
             --><!-- wysiwg editor -->
            <!-- <script src="http://yukon.tzdthemes.com/html/assets/lib/ckeditor/ckeditor.js"></script>
            <script src="http://yukon.tzdthemes.com/html/assets/lib/ckeditor/adapters/jquery.js"></script> -->
            
    </body>
</html>