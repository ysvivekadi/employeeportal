<!DOCTYPE html>
<html>
<head>
	<title>DatePicker</title>
		<link rel="stylesheet" type="text/css" href="http://t1m0n.name/air-datepicker/dist/css/datepicker.min.css">

		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
		<script type="text/javascript" src="http://t1m0n.name/air-datepicker/dist/js/datepicker.js">	</script>
		<script type="text/javascript" src="http://t1m0n.name/air-datepicker/dist/js/i18n/datepicker.en.js">	</script> 
</head>
<body>
	<input type='text' id="datepicker" data-timepicker="true" class='datepicker-here' data-language='en' />
</body>
	<script type="text/javascript">
		$( document ).ready(function() {
			var $promo = $('#datepicker');
			$promo.datepicker({language: 'en'})
		});
	</script>
</html>