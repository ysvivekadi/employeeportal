<!DOCTYPE html>
<html>
    
<!-- Mirrored from yukon.tzdthemes.com/html/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 23 May 2016 11:25:52 GMT -->
<head>
        <meta charset="UTF-8">
        <title>TE Employee portal</title>
        <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">

        <!-- bootstrap framework -->
        <link href="http://yukon.tzdthemes.com/html/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="all">
        
        <!-- icon sets -->
            <!-- elegant icons -->
                <link href="http://yukon.tzdthemes.com/html/assets/icons/elegant/style.css" rel="stylesheet" media="all">
            <!-- elusive icons -->
                <link href="http://yukon.tzdthemes.com/html/assets/icons/elusive/css/elusive-webfont.css" rel="stylesheet" media="all">
            <!-- flags -->
                <link rel="stylesheet" href="http://yukon.tzdthemes.com/html/assets/icons/flags/flags.css" media="all">
            <!-- scrollbar -->
                <link rel="stylesheet" href="http://yukon.tzdthemes.com/html/assets/lib/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css">


        <!-- google webfonts -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>

        <!-- main stylesheet -->
        <link href="http://yukon.tzdthemes.com/html/assets/css/main.min.css" rel="stylesheet" media="all" id="mainCss">

        <!-- print stylesheet -->
        <link href="http://yukon.tzdthemes.com/html/assets/css/print.css" rel="stylesheet" media="print">

        <!-- moment.js (date library) -->
        <script src="http://yukon.tzdthemes.com/html/assets/js/moment-with-langs.min.js"></script>

    </head>
    <body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">

            <!-- header -->
            <header id="main_header">
                <div class="container-fluid">
                    <div class="brand_section">
                        <a href="index"><asset:image src="logo.png" alt="site_logo" width="150" height="60"/></a>
                    </div>
                    <div class="header_user_actions dropdown">
                        <div data-toggle="dropdown" class="dropdown-toggle user_dropdown">
                            <div class="user_avatar">
                                <asset:image src="avatars/avatar08_tn.png" alt="" title="Carrol Clark (carrol@example.com)" width="38" height="38"/>
                            </div>
                            <span class="caret"></span>
                        </div>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a href="pages-user_profile.html">User Profile</a></li>
                            <li><a href="#">Check in</a></li>
                            <li><a href="login_page.html">Log Out</a></li>
                        </ul>
                    </div>
                </div>
            </header>
            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="index">Home</a></li>    
                </ul>
            </nav>

            <!-- main content -->
            <div id="main_wrapper">
                <div class="container-fluid">
                </div>
            
            <!-- main menu -->
                <nav id="main_menu">
                    <div class="menu_wrapper">
                        <ul>
                            <li class="first_level">
                                <a href="dashboard.html">
                                    <span class="icon_house_alt first_level_icon"></span>
                                    <span class="menu-title">Dashboard</span>
                                </a>
                            </li>
                            <li class="first_level">
                                <a href="#">
                                    <span class="icon_document_alt first_level_icon"></span>
                                    <span class="menu-title">Timesheet</span>
                                </a>
                            </li>
                            <li class="first_level">
                                <a href="#">
                                    <span class="icon_folder-alt first_level_icon"></span>
                                    <span class="menu-title">Gantt Chart</span>
                                    <!-- <span class="label label-danger">12</span> -->
                                </a>
                            </li>
                            <li class="first_level">
                                <a href="#">
                                    <span class="icon_puzzle first_level_icon"></span>
                                    <span class="menu-title">Salary structure</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </nav>

            </div>

        <!-- jQuery -->
        <script src="http://yukon.tzdthemes.com/html/assets/js/jquery.min.js"></script>
        <!-- jQuery Cookie -->
        <script src="http://yukon.tzdthemes.com/html/assets/js/jqueryCookie.min.js"></script>
        <!-- Bootstrap Framework -->
        <script src="http://yukon.tzdthemes.com/html/assets/bootstrap/js/bootstrap.min.js"></script>
        <!-- retina images -->
        <script src="http://yukon.tzdthemes.com/html/assets/js/retina.min.js"></script>
        <!-- switchery -->
        <script src="http://yukon.tzdthemes.com/html/assets/lib/switchery/dist/switchery.min.js"></script>
        <!-- typeahead -->
        <script src="http://yukon.tzdthemes.com/html/assets/lib/typeahead/typeahead.bundle.min.js"></script>
        <!-- fastclick -->
        <script src="http://yukon.tzdthemes.com/html/assets/js/fastclick.min.js"></script>
        <!-- match height -->
        <script src="http://yukon.tzdthemes.com/html/assets/lib/jquery-match-height/jquery.matchHeight-min.js"></script>
        <!-- scrollbar -->
        <script src="http://yukon.tzdthemes.com/html/assets/lib/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>

        <!-- Yukon Admin functions -->
        <script src="http://yukon.tzdthemes.com/html/assets/js/yukon_all.js"></script>

        <!-- page specific plugins -->

            <!-- c3 charts -->
            <script src="http://yukon.tzdthemes.com/html/assets/lib/d3/d3.min.js"></script>
            <script src="http://yukon.tzdthemes.com/html/assets/lib/c3/c3.min.js"></script>
            <!-- vector maps -->
            <script src="http://yukon.tzdthemes.com/html/assets/lib/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
            <script src="http://yukon.tzdthemes.com/html/assets/lib/jvectormap/maps/jquery-jvectormap-world-mill-en.js"></script>
            <!-- countUp animation -->
            <script src="http://yukon.tzdthemes.com/html/assets/js/countUp.min.js"></script>
            <!-- easePie chart -->
            <script src="http://yukon.tzdthemes.com/html/assets/lib/easy-pie-chart/dist/jquery.easypiechart.min.js"></script>

            <script>
                $(function() {
                    // c3 charts
                    yukon_charts.p_dashboard();
                    // countMeUp
                    yukon_count_up.init();
                    // easy pie chart
                    yukon_easyPie_chart.p_dashboard();
                    // vector maps
                    yukon_vector_maps.p_dashboard();
                    // match height
                    yukon_matchHeight.p_dashboard();
                })
            </script>
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','../../www.google-analytics.com/analytics.js','ga');
      
        ga('create', 'UA-54304677-1', 'auto');
        ga('send', 'pageview');
    </script>
    
    </body>

<!-- Mirrored from yukon.tzdthemes.com/html/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 23 May 2016 11:26:21 GMT -->
</html>