<!DOCTYPE html>
<html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="layout" content="etemplate">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">         -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="http://t4t5.github.io/sweetalert/dist/sweetalert.css">
    <link   rel="stylesheet" type="text/css" href="http://t1m0n.name/air-datepicker/dist/css/datepicker.min.css">
    <asset:javascript src="datepicker.js"/> 
    <asset:javascript src="datepicker.en.js"/>
    
          <script type="text/javascript">
            function myFunction() {
                $( document ).ready(function() {
                    var $promo = $('#datepicker');
                    $promo.datepicker({language: 'en'});
                });
            }
          </script>
<style>   
input[type=text]{
    width: 80%; 
    padding: 6px 20px;
    border: 1px solid #ccc;
    box-sizing: border-box;
}
</style> 
</head>
<body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">
            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="..\employee\home">Home</a></li>    
                    <li><a href="..\employee\time">Timesheet</a></li>    
                    <li>New</li>
                </ul>
            </nav>
            <!-- id="datepicker" class='datepicker-here' data-language='en' -->
            <!-- main content -->
            <div id="main_wrapper">
                <div class="container-fluid">
                    <!-- <div class="first"> -->
                        <div class="row">
                        
                        <div class="col-md-12" >
                            <g:form controller="Timesheet" method="post">
                                <div class="heading_a"><span><h3>TimeLive</h3></span></div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="val_first_name" class="req">From date</label><br>
                                        <input type="text" name="fromdate" id="datepicker" class="datepicker-here" data-language='en'   required class="form-control"/>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="val_last_name" class="req">To date</label><br>
                                        <input type="text" name="todate" id="datepicker" class="datepicker-here" data-language='en'   required class="form-control"/>  
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="val_select" class="req">Project</label>
                                        <select name="project" id="val_select" class="form-control" required>
                                            <option value="">---</option>
                                            <g:each var="projec" in="${proj}">
                                                <g:if test ="${projec.manager.id == employee.manager.id}">
                                                <option>${projec.project_name}</option>
                                                </g:if>
                                            </g:each>  
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <label for="val_select" class="req">Task</label>
                                        <select name="task" id="val_select" class="form-control" required>
                                            <option value="">---</option>
                                            <g:each var="tas" in="${task}">
                                                <g:if test="${tas.project.manager.id==employee.manager.id}">
                                                  <option>${tas.name}</option>
                                                </g:if>
                                            </g:each>
                                        </select>
                                    </div>
                                    <div class="col-md-2">
                                        <label for="val_last_name" class="req">Work hour</label>
                                        <input type="text" name="workhour" id="val_last_name"  required class="form-control"/>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label for="val_textarea_message" class="req">Description</label>
                                        <textarea name="description" cols="30" rows="4" id="trumbowyg" class="form-control" data-parsley-required="true" data-parsley-minlength="2"></textarea>
                                    </div>
                                </div>
                                <br>
                                <input type="hidden" name="username" value="${employee.username}"></input>
                                <div class="row">
                                    <div class="col-md-12" id="an">
                                        <g:actionSubmit class="btn btn-primary" value="Submit" action="save">   
                                </g:actionSubmit>
                                 <!-- <button class="btn btn-primary">Submit</button> -->
                                    </div>
                                </div>
                            </g:form>
                            <!-- <button class="btn btn-success">alert</button> -->
                        </div>
                    
                    </div>  
                </div>
            </div>
        <!-- page specific plugins -->
       <!--  <script src="http://yukon.tzdthemes.com/html/assets/lib/select2/select2.min.js"></script> -->
            <!-- validation (parsley.js) -->
            <!-- <script src="http://yukon.tzdthemes.com/html/assets/js/parsley.config.js"></script>
            <script src="http://yukon.tzdthemes.com/html/assets/lib/parsley/dist/parsley.min.js"></script>
             --><!-- wysiwg editor -->
            <!-- <script src="http://yukon.tzdthemes.com/html/assets/lib/ckeditor/ckeditor.js"></script>
            <script src="http://yukon.tzdthemes.com/html/assets/lib/ckeditor/adapters/jquery.js"></script> -->
        
            <!-- wysiwg editor -->
            <!-- <script src="http://yukon.tzdthemes.com/html/assets/lib/ckeditor/adapters/jquery.js"></script> -->
     <!--  <script type="text/javascript">
        $(document).ready(function() {
            alert("Hari Bol");
            $('#trumbowyg').trumbowyg();
        });
      </script> -->
    </body>
</html>