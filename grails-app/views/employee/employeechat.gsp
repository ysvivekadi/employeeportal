<%@ page import="new1.Employee" %>
<!DOCTYPE html>
<html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="layout" content="etemplate">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">        
    <link href="http://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
    <link href="http://yukon.tzdthemes.com/html/assets/lib/select2/select2.css" rel="stylesheet" media="all">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
     <link rel="stylesheet" href="http://yukon.tzdthemes.com/html/assets/lib/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css">
   <link href='http://fonts.googleapis.com/css?family=Open+Sans&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
<script src="http://yukon.tzdthemes.com/html/assets/js/moment-with-langs.min.js"></script>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.3.js"></script>
  <script type="text/javascript" src="https://www.google-analytics.com/analytics.js"></script>

    <script src="http://code.jquery.com/ui/1.10.4/jquery-ui.js"></script> 
    
        
</head>

<body class="side_menu_active side_menu_expanded">
         
        <div id="page_wrapper">

            <!-- header -->
            <header id="main_header">
                <div class="container-fluid">
                    <div class="brand_section">
                        <a href="home"><asset:image src="logo.png" alt="site_logo" width="150" height="60"/></a>
                    </div>
                    <div class="header_user_actions dropdown">
                        <div data-toggle="dropdown" class="dropdown-toggle user_dropdown">
                            <div class="user_avatar">
                                <asset:image src="avatars/avatar08_tn.png" alt="" title="Carrol Clark (carrol@example.com)" width="38" height="38"/>
                            </div>
                            <span class="caret"></span>
                        </div>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a href="pages-user_profile.html">User Profile</a></li>
                            <li><a href="#"><div>Check in</div></a></li>
                            <li><a href="login_page.html">Log Out</a></li>
                            </ul>
                    </div>
                </div>
            </header>
            <!-- breadcrumbs -->
            <nav id="breadcrumbs">
                <ul>
                    <li><a href="home">Home</a></li>    
                    <li>Timesheet</li>    
                </ul>
            </nav>
            
            <!-- main content -->
            <div id="main_wrapper">
                <div class="container-fluid">
                    <!-- <div class="first"> -->
                    
                    <div class="row">
                        <div class="col-md-9">
                            <div class="chat_messages">

                               <g:each in="${messages}" status="i" var="c">
                                <g:if test="${c.receiver=='all'}">  
                               <div class="message_date">${c.date}</div>
                                <ul>
                                    <g:if test="${c.nickname!=currentuser}">
                                    <li class="msg_left">
                                        <p class="msg_user">${c.nickname}</p>
                                       ${c.message}
                                    </li>
                                    </g:if>
                                    <g:if test="${c.nickname==currentuser}">
                                    <li class="msg_right">
                                        <p class="msg_user">${c.nickname}</p>
                                        ${c.message}
                                    </li>
                                    </g:if>
                                </ul>

                                  </g:if>
                        
                               </g:each>

                                
                            </div>
                            <div class="chat_message_send">

                                 <g:form  role="form" controller="Employee">  
          
                                 <textarea rows="4" cols="50" class="form-control" type="text" name="comments"></textarea>
                                  
                                   <label for="val_select" class="req">Employee</label>
                        <g:select name="emplo" from="${employee}" value="${emplo?.id}" noSelection="['':'select employee']"/>

                                  <div>
                                 <g:actionSubmit id="btn-signup" type="submit" class="btn btn-sm btn-primary" action="submitMessage" value="Send"/>
                                  </div>
                                  <div class="col-md-3">
                    
                                 </div>
                                 </g:form>
                                                            
                            </div>
                            

                        </div>
                        <div class="col-md-3 col-sep">
                            <div class="chat_users">
                                <h3 class="heading_b"><span class="heading_text">Online</span></h3>
                                <ul>
                                   
                                      <g:each in="${empstatus}"  var="i">

                                     <g:if test="${i.status==true}">

                                      <li class="online">
                                        <a>
                                            <img src="assets/img/avatars/avatar02_tn.png" alt="" width="38" height="38">
                                            ${i.username}
                                        </a>
                                      </li>
                                    
                                    </g:if>
                                
                                     </g:each>
                           
                                </ul>

                         
                              

                                  

                                <h3 class="heading_b"><span class="heading_text">Offline</span></h3>
                                <ul>
                                    <g:each in="${empstatus}"  var="j">

                                     <g:if test="${j.status==false}">

                                      <li class="offline">
                                        <a>
                                            <img src="assets/img/avatars/avatar02_tn.png" alt="" width="38" height="38">
                                            ${j.username}
                                        </a>
                                      </li>
                                    
                                    </g:if>
                                
                                     </g:each>
                                </ul>
                            </div>
                        </div>
                    </div>
                    
                    
                </div>
            </div>

            <!-- jQuery -->
        <script src="http://yukon.tzdthemes.com/html/assets/js/jquery.min.js"></script>
        <!-- jQuery Cookie -->
        <script src="http://yukon.tzdthemes.com/html/assets/js/jqueryCookie.min.js"></script>
        <!-- Bootstrap Framework -->
        <script src="http://yukon.tzdthemes.com/html/assets/bootstrap/js/bootstrap.min.js"></script>
        <!-- retina images -->
        <script src="http://yukon.tzdthemes.com/html/assets/js/retina.min.js"></script>
        <!-- switchery -->
        <script src="http://yukon.tzdthemes.com/html/assets/lib/switchery/dist/switchery.min.js"></script>
        <!-- typeahead -->
        <script src="http://yukon.tzdthemes.com/html/assets/lib/typeahead/typeahead.bundle.min.js"></script>
        <!-- fastclick -->
        <script src="http://yukon.tzdthemes.com/html/assets/js/fastclick.min.js"></script>
        <!-- match height -->
        <script src="http://yukon.tzdthemes.com/html/assets/lib/jquery-match-height/jquery.matchHeight-min.js"></script>
        <!-- scrollbar -->
        <script src="http://yukon.tzdthemes.com/html/assets/lib/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>

        <!-- Yukon Admin functions -->
        <script src="http://yukon.tzdthemes.com/html/assets/js/yukon_all.js"></script>

            <script>
                $(function() {
                    // chat
                    yukon_chat.init();
                })
            </script>
        <!-- page specific plugins -->
       <!--  <script src="http://yukon.tzdthemes.com/html/assets/lib/select2/select2.min.js"></script> -->
            <!-- validation (parsley.js) -->
            <!-- <script src="http://yukon.tzdthemes.com/html/assets/js/parsley.config.js"></script>
            <script src="http://yukon.tzdthemes.com/html/assets/lib/parsley/dist/parsley.min.js"></script>
             --><!-- wysiwg editor -->
            <!-- <script src="http://yukon.tzdthemes.com/html/assets/lib/ckeditor/ckeditor.js"></script>
            <script src="http://yukon.tzdthemes.com/html/assets/lib/ckeditor/adapters/jquery.js"></script> -->

            <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
      
        ga('create', 'UA-54304677-1', 'auto');
        ga('send', 'pageview');

    </script>
            
    </body>
</html>