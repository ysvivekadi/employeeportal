<!DOCTYPE html>
<html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="layout" content="etemplate">
</head>
<body>
	  <div id="main_wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <asset:image class="user_profile_img" src="avatar08.jpg" alt="" width="76" height="76" />
                        <h1 class="user_profile_name">${pro.first} ${pro.last}</h1>
                        <p class="user_profile_info">${pro.designation}</p>
                        <p class="user_profile_info">${pro.organization}</p>
                    </div>
                </div>
                <hr/>
                <div class="row">
                    <g:form action="save" controller="Profile" class="form-horizontal" role="form">
                        <div class="col-lg-12">
                            <h4 class="heading_a"><span>General info</span></h4>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="profile_username" class="col-sm-4 control-label">First Name</label>
                                    <div  class="col-sm-6">
                                        <input type="text" name="first" class="form-control" id="profile_username" value="${pro.first}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="profile_name" class="col-sm-4 control-label">Last Name</label>
                                    <div class="col-sm-6">
                                        <input type="text" name="last" class="form-control" id="profile_name" value="${pro.last}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="profile_name" class="col-sm-4 control-label">Designation</label>
                                    <div class="col-sm-6">
                                        <input type="text" name="designation" class="form-control" id="profile_name" value="${pro.designation}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="profile_name" class="col-sm-4 control-label">Gender</label>
                                    <div class="col-sm-6">
                                        <select name="gender" class="form-control">
                                            <g:if test="${!pro.gender}">
                                                <option disabled selected value>Gender</option>
                                                <option value="Male">Male</option>
                                                <option value="Female">Female</option>
                                            </g:if>
                                            <g:elseif test="${pro.gender=='Male'}">
                                                <option value="Male" selected>Male</option>
                                                <option value="Female">Female</option>
                                            </g:elseif>
                                            <g:elseif test="${pro.gender=='Female'}">
                                                <option value="Male" >Male</option>
                                                <option value="Female" selected>Female</option>
                                            </g:elseif>
                                        </select>
                                        <!-- <input type="text" name="gender" class="form-control" id="profile_name" value="${pro.gender}"> -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="profile_name" class="col-sm-4 control-label">ReportingTo</label>
                                    <div class="col-sm-6">
                                        <input type="text" name="reportingto" class="form-control" id="profile_name" value="${pro.reportingto}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="profile_name" class="col-sm-4 control-label">Location</label>
                                    <div class="col-sm-6">
                                        <input type="text" name="location" class="form-control" id="profile_name" value="${pro.location}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="profile_name" class="col-sm-4 control-label">Organization</label>
                                    <div class="col-sm-6">
                                        <input type="text" name="organization" class="form-control" id="profile_name" value="${pro.organization}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="profile_name" class="col-sm-4 control-label">Department</label>
                                    <div class="col-sm-6">
                                        <input type="text" name="department" class="form-control" id="profile_name" value="${pro.department}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="profile_bday" class="col-sm-4 control-label">Date of joining</label>
                                    <div class="col-sm-6">
                                        <input type="date" name="date" class="form-control" id="profile_bday" value="${pro.date}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <h4 class="heading_a"><span >Contact info</span></h4>
                            <div class="col-sm-10">
                                <div class="form-group">
                                    <label for="profile_email" class="col-sm-4 control-label">Email</label>
                                    <div class="col-sm-6">
                                        <input type="text" name="email" class="form-control" id="profile_email" value="${pro.email}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-10">   
                                <div class="form-group">
                                    <label for="profile_skype" class="col-sm-4 control-label">Mobile No</label>
                                    <div class="col-sm-6">
                                        <input type="text" name="mobile" class="form-control" id="profile_skype" value="${pro.mobile}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <h4 class="heading_a"><span>Primary Info</span></h4>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="profile_name" class="col-sm-4 control-label">Nationality</label>
                                    <div class="col-sm-6">
                                        <input type="text" name="nationality" class="form-control" id="profile_name" value="${pro.nationality}">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="profile_name" class="col-sm-4 control-label">Pan No</label>
                                    <div class="col-sm-6">
                                        <input type="text" name="panno" class="form-control" id="profile_name" value="${pro.panno}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="username" value="${emp.username}"></input>
                            <div class="form-group">
                                <div class="col-sm-12 col-sm-offset-5">
                                    <g:actionSubmit class="btn-primary btn" value="Save" />
                                    <button onclick="window.location.reload()" class="btn-default btn">Cancel</button>
                                </div>
                            </div>
                    </g:form>
                </div>
            </div>
      </div>
</body>
</html>