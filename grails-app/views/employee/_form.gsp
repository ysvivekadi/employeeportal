<%@ page import="new1.Employee" %>



<div class="fieldcontain ${hasErrors(bean: employeeInstance, field: 'checkin_checkout', 'error')} ">
	<label for="checkin_checkout">
		<g:message code="employee.checkin_checkout.label" default="Checkincheckout" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${employeeInstance?.checkin_checkout?}" var="c">
    <li><g:link controller="checkin_checkout" action="show" id="${c.id}">${c?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="checkin_checkout" action="create" params="['employee.id': employeeInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'checkin_checkout.label', default: 'Checkin_checkout')])}</g:link>
</li>
</ul>


</div>

<div class="fieldcontain ${hasErrors(bean: employeeInstance, field: 'manager', 'error')} required">
	<label for="manager">
		<g:message code="employee.manager.label" default="Manager" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="manager" name="manager.id" from="${new1.Manager.list()}" optionKey="id" required="" value="${employeeInstance?.manager?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: employeeInstance, field: 'password', 'error')} required">
	<label for="password">
		<g:message code="employee.password.label" default="Password" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="password" required="" value="${employeeInstance?.password}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: employeeInstance, field: 'salary', 'error')} ">
	<label for="salary">
		<g:message code="employee.salary.label" default="Salary" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${employeeInstance?.salary?}" var="s">
    <li><g:link controller="salary" action="show" id="${s.id}">${s?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="salary" action="create" params="['employee.id': employeeInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'salary.label', default: 'Salary')])}</g:link>
</li>
</ul>


</div>

<div class="fieldcontain ${hasErrors(bean: employeeInstance, field: 'timesheet', 'error')} ">
	<label for="timesheet">
		<g:message code="employee.timesheet.label" default="Timesheet" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${employeeInstance?.timesheet?}" var="t">
    <li><g:link controller="timesheet" action="show" id="${t.id}">${t?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="timesheet" action="create" params="['employee.id': employeeInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'timesheet.label', default: 'Timesheet')])}</g:link>
</li>
</ul>


</div>

<div class="fieldcontain ${hasErrors(bean: employeeInstance, field: 'username', 'error')} required">
	<label for="username">
		<g:message code="employee.username.label" default="Username" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="username" required="" value="${employeeInstance?.username}"/>

</div>

