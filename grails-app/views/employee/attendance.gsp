<!DOCTYPE html>
<html>
	<head>
		
		<title></title>
	</head>
	<body>
		<body>
		<h3>
			<style>
                table, th, td {
                border: 1px solid black;
                border-collapse: collapse;
                }
                th, td {
                padding: 5px;
                }
            </style>
		</h3>
		<table border="1" style="width:50%">
		<tr>
            <th>Checkin_date</th>
            <th>checkout_date</th>
            <th>time_duration[hh:mm:ss]</th>
        </tr>

        <g:each in="${persons}" var="person" status="i">   
        <g:if test="${person.employee.id==employee.id}">
            <tr>
            <td>${person.checkin_date} </td>
            <td>${person.checkout_date} </td>
            <td>${person.time_duration} </td>
            </tr>
        </g:if>
        </g:each>
        </table> 
	</body>
</html>
	</body>