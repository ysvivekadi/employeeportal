
<%@ page import="new1.Employee" %>
<!doctype html>
<html lang="en">
    
<!-- Mirrored from yukon.tzdthemes.com/html/login_page.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 23 May 2016 11:27:47 GMT -->
<head>
        <meta charset="UTF-8">
        <title>Yukon Admin Login Page</title>
        <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!-- bootstrap framework -->
        <link href="http://yukon.tzdthemes.com/html/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <!-- google webfonts -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
        <!-- elegant icons -->
        <link href="http://yukon.tzdthemes.com/html/assets/icons/elegant/style.css" rel="stylesheet" media="screen">
        <!-- main stylesheet -->
        <link href="http://yukon.tzdthemes.com/html/assets/css/main.min.css" rel="stylesheet" media="screen">

        <!-- jQuery -->
        <script src="http://yukon.tzdthemes.com/html/assets/js/jquery.min.js"></script>

    </head>
    <body class="login_page">

        <div class="login_header">
            <asset:image src="logo.png" alt="site_logo"/>
        </div>
        <div class="login_register_form">
            <div class="form_wrapper animated-short" id="login_form">
                <h3 class="sepH_c"><span>Login</span>   
                <!-- <a href="javascript:void(0)" class="form-switch" data-switch-form="register_form">Register</a> -->
                </h3>
                <form action="login">
                    <div class="input-group input-group-lg sepH_a">
                        <span class="input-group-addon"><span class="icon_profile"></span></span>
                        <input type="text" class="form-control" name="username" placeholder="Username">
                    </div>
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon"><span class="icon_key_alt"></span></span>
                        <input type="password" class="form-control" name="password" placeholder="Password">
                    </div>
                    <div class="sepH_c text-right">
                        <a href="../manager/index" class="small">Sign in as Manager</a>
                    </div>
                    <div class="form-group sepH_c">
                        <input type="submit" class="btn btn-lg btn-primary btn-block" value="login">
                    </div>
                </form>
            </div>
        </div>
    
    
    </body>

<!-- Mirrored from yukon.tzdthemes.com/html/login_page.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 23 May 2016 11:27:47 GMT -->
</html>