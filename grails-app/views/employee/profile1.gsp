<!DOCTYPE html>
<html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="layout" content="etemplate">
</head>
<body>
	  <div id="main_wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm-6">
                        <asset:image class="user_profile_img" src="avatar08.jpg" alt="" width="76" height="76" />
                        <h1 class="user_profile_name">${pro.first} ${pro.last}</h1>
                        <p class="user_profile_info">${pro.designation}</p>
                        <p class="user_profile_info">${pro.organization}</p>
                    </div>
                </div>
                <!-- <hr/> -->
                        <div class="col-sm-12 col-sm-offset-10">
                            <a href="profile">Edit</a>
                        </div>                
                <div class="row">
                            <h4 class="heading_a"><span>General info</span></h4>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label style="padding-right: 20px">First Name : </label>
                                    <label>${pro.first}</label>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label style="padding-right: 20px">Last Name: </label>
                                    <label>${pro.last}</label>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label style="padding-right: 20px">Designation :</label>
                                    <label>${pro.designation}</label>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label style="padding-right: 20px">Gender :</label>
                                    <label>${pro.gender}</label>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label style="padding-right: 20px">ReportingTo :</label>
                                    <label>${pro.reportingto}</label>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label style="padding-right: 20px">Location :</label>
                                    <label>${pro.location}</label>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label style="padding-right: 20px">Organization :</label>
                                    <label>${pro.organization}</label>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label style="padding-right: 20px">Department :</label>
                                    <label>${pro.department}</label>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label style="padding-right: 20px">Date of joining :</label>
                                    <label>${pro.date}</label>
                                </div>
                            </div>
                        
                        <div class="col-lg-12">
                            <h4 class="heading_a"><span >Contact info</span></h4>
                            <div class="col-sm-10">
                                <div class="form-group">
                                    <label style="padding-right: 20px">Email :</label>
                                    <label>${pro.email}</label>
                                </div>
                            </div>
                            <div class="col-lg-10">   
                                <div class="form-group">
                                    <label style="padding-right: 20px">Mobile No :</label>
                                    <label>${pro.mobile}</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <h4 class="heading_a"><span>Primary Info</span></h4>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label style="padding-right: 20px">Nationality :</label>
                                    <label>${pro.nationality}</label>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label style="padding-right: 20px">Pan No :</label>
                                    <label>${pro.panno}</label>
                                </div>
                            </div>
                        </div>
                        
                </div>
            </div>
      </div>
</body>
</html>