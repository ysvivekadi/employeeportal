<%@ page import="new1.Billdetails" %>



<div class="fieldcontain ${hasErrors(bean: billdetailsInstance, field: 'employee', 'error')} required">
	<label for="employee">
		<g:message code="billdetails.employee.label" default="Employee" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="employee" name="employee.id" from="${new1.Employee.list()}" optionKey="id" required="" value="${billdetailsInstance?.employee?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: billdetailsInstance, field: 'mallowance', 'error')} required">
	<label for="mallowance">
		<g:message code="billdetails.mallowance.label" default="Mallowance" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="mallowance" type="number" value="${billdetailsInstance.mallowance}" required=""/>

</div>

