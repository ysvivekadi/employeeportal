
<%@ page import="new1.Billdetails" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'billdetails.label', default: 'Billdetails')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-billdetails" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-billdetails" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<th><g:message code="billdetails.employee.label" default="Employee" /></th>
					
						<g:sortableColumn property="mallowance" title="${message(code: 'billdetails.mallowance.label', default: 'Mallowance')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${billdetailsInstanceList}" status="i" var="billdetailsInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${billdetailsInstance.id}">${fieldValue(bean: billdetailsInstance, field: "employee")}</g:link></td>
					
						<td>${fieldValue(bean: billdetailsInstance, field: "mallowance")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${billdetailsInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
