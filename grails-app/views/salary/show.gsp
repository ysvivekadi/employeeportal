
<%@ page import="new1.Salary" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'salary.label', default: 'Salary')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-salary" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-salary" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list salary">
			
				<g:if test="${salaryInstance?.basic}">
				<li class="fieldcontain">
					<span id="basic-label" class="property-label"><g:message code="salary.basic.label" default="Basic" /></span>
					
						<span class="property-value" aria-labelledby="basic-label"><g:fieldValue bean="${salaryInstance}" field="basic"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${salaryInstance?.basic_proj}">
				<li class="fieldcontain">
					<span id="basic_proj-label" class="property-label"><g:message code="salary.basic_proj.label" default="Basicproj" /></span>
					
						<span class="property-value" aria-labelledby="basic_proj-label"><g:fieldValue bean="${salaryInstance}" field="basic_proj"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${salaryInstance?.conveyance}">
				<li class="fieldcontain">
					<span id="conveyance-label" class="property-label"><g:message code="salary.conveyance.label" default="Conveyance" /></span>
					
						<span class="property-value" aria-labelledby="conveyance-label"><g:fieldValue bean="${salaryInstance}" field="conveyance"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${salaryInstance?.conveyance_proj}">
				<li class="fieldcontain">
					<span id="conveyance_proj-label" class="property-label"><g:message code="salary.conveyance_proj.label" default="Conveyanceproj" /></span>
					
						<span class="property-value" aria-labelledby="conveyance_proj-label"><g:fieldValue bean="${salaryInstance}" field="conveyance_proj"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${salaryInstance?.employee}">
				<li class="fieldcontain">
					<span id="employee-label" class="property-label"><g:message code="salary.employee.label" default="Employee" /></span>
					
						<span class="property-value" aria-labelledby="employee-label"><g:link controller="employee" action="show" id="${salaryInstance?.employee?.id}">${salaryInstance?.employee?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${salaryInstance?.employeePF}">
				<li class="fieldcontain">
					<span id="employeePF-label" class="property-label"><g:message code="salary.employeePF.label" default="Employee PF" /></span>
					
						<span class="property-value" aria-labelledby="employeePF-label"><g:fieldValue bean="${salaryInstance}" field="employeePF"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${salaryInstance?.employeePF_proj}">
				<li class="fieldcontain">
					<span id="employeePF_proj-label" class="property-label"><g:message code="salary.employeePF_proj.label" default="Employee PF proj" /></span>
					
						<span class="property-value" aria-labelledby="employeePF_proj-label"><g:fieldValue bean="${salaryInstance}" field="employeePF_proj"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${salaryInstance?.employerPF}">
				<li class="fieldcontain">
					<span id="employerPF-label" class="property-label"><g:message code="salary.employerPF.label" default="Employer PF" /></span>
					
						<span class="property-value" aria-labelledby="employerPF-label"><g:fieldValue bean="${salaryInstance}" field="employerPF"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${salaryInstance?.employerPF_proj}">
				<li class="fieldcontain">
					<span id="employerPF_proj-label" class="property-label"><g:message code="salary.employerPF_proj.label" default="Employer PF proj" /></span>
					
						<span class="property-value" aria-labelledby="employerPF_proj-label"><g:fieldValue bean="${salaryInstance}" field="employerPF_proj"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${salaryInstance?.hra}">
				<li class="fieldcontain">
					<span id="hra-label" class="property-label"><g:message code="salary.hra.label" default="Hra" /></span>
					
						<span class="property-value" aria-labelledby="hra-label"><g:fieldValue bean="${salaryInstance}" field="hra"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${salaryInstance?.hra_proj}">
				<li class="fieldcontain">
					<span id="hra_proj-label" class="property-label"><g:message code="salary.hra_proj.label" default="Hraproj" /></span>
					
						<span class="property-value" aria-labelledby="hra_proj-label"><g:fieldValue bean="${salaryInstance}" field="hra_proj"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${salaryInstance?.medical}">
				<li class="fieldcontain">
					<span id="medical-label" class="property-label"><g:message code="salary.medical.label" default="Medical" /></span>
					
						<span class="property-value" aria-labelledby="medical-label"><g:fieldValue bean="${salaryInstance}" field="medical"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${salaryInstance?.medical_proj}">
				<li class="fieldcontain">
					<span id="medical_proj-label" class="property-label"><g:message code="salary.medical_proj.label" default="Medicalproj" /></span>
					
						<span class="property-value" aria-labelledby="medical_proj-label"><g:fieldValue bean="${salaryInstance}" field="medical_proj"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${salaryInstance?.special}">
				<li class="fieldcontain">
					<span id="special-label" class="property-label"><g:message code="salary.special.label" default="Special" /></span>
					
						<span class="property-value" aria-labelledby="special-label"><g:fieldValue bean="${salaryInstance}" field="special"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${salaryInstance?.special_proj}">
				<li class="fieldcontain">
					<span id="special_proj-label" class="property-label"><g:message code="salary.special_proj.label" default="Specialproj" /></span>
					
						<span class="property-value" aria-labelledby="special_proj-label"><g:fieldValue bean="${salaryInstance}" field="special_proj"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${salaryInstance?.year}">
				<li class="fieldcontain">
					<span id="year-label" class="property-label"><g:message code="salary.year.label" default="Year" /></span>
					
						<span class="property-value" aria-labelledby="year-label"><g:fieldValue bean="${salaryInstance}" field="year"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:salaryInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${salaryInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
