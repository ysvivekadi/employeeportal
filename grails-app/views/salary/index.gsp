
<%@ page import="new1.Salary" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'salary.label', default: 'Salary')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-salary" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-salary" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="basic" title="${message(code: 'salary.basic.label', default: 'Basic')}" />
					
						<g:sortableColumn property="basic_proj" title="${message(code: 'salary.basic_proj.label', default: 'Basicproj')}" />
					
						<g:sortableColumn property="conveyance" title="${message(code: 'salary.conveyance.label', default: 'Conveyance')}" />
					
						<g:sortableColumn property="conveyance_proj" title="${message(code: 'salary.conveyance_proj.label', default: 'Conveyanceproj')}" />
					
						<th><g:message code="salary.employee.label" default="Employee" /></th>
					
						<g:sortableColumn property="employeePF" title="${message(code: 'salary.employeePF.label', default: 'Employee PF')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${salaryInstanceList}" status="i" var="salaryInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${salaryInstance.id}">${fieldValue(bean: salaryInstance, field: "basic")}</g:link></td>
					
						<td>${fieldValue(bean: salaryInstance, field: "basic_proj")}</td>
					
						<td>${fieldValue(bean: salaryInstance, field: "conveyance")}</td>
					
						<td>${fieldValue(bean: salaryInstance, field: "conveyance_proj")}</td>
					
						<td>${fieldValue(bean: salaryInstance, field: "employee")}</td>
					
						<td>${fieldValue(bean: salaryInstance, field: "employeePF")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${salaryInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
