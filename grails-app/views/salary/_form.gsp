<%@ page import="new1.Salary" %>



<div class="fieldcontain ${hasErrors(bean: salaryInstance, field: 'basic', 'error')} required">
	<label for="basic">
		<g:message code="salary.basic.label" default="Basic" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="basic" value="${fieldValue(bean: salaryInstance, field: 'basic')}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: salaryInstance, field: 'basic_proj', 'error')} required">
	<label for="basic_proj">
		<g:message code="salary.basic_proj.label" default="Basicproj" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="basic_proj" value="${fieldValue(bean: salaryInstance, field: 'basic_proj')}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: salaryInstance, field: 'conveyance', 'error')} required">
	<label for="conveyance">
		<g:message code="salary.conveyance.label" default="Conveyance" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="conveyance" value="${fieldValue(bean: salaryInstance, field: 'conveyance')}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: salaryInstance, field: 'conveyance_proj', 'error')} required">
	<label for="conveyance_proj">
		<g:message code="salary.conveyance_proj.label" default="Conveyanceproj" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="conveyance_proj" value="${fieldValue(bean: salaryInstance, field: 'conveyance_proj')}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: salaryInstance, field: 'employee', 'error')} required">
	<label for="employee">
		<g:message code="salary.employee.label" default="Employee" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="employee" name="employee.id" from="${new1.Employee.list()}" optionKey="id" required="" value="${salaryInstance?.employee?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: salaryInstance, field: 'employeePF', 'error')} required">
	<label for="employeePF">
		<g:message code="salary.employeePF.label" default="Employee PF" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="employeePF" value="${fieldValue(bean: salaryInstance, field: 'employeePF')}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: salaryInstance, field: 'employeePF_proj', 'error')} required">
	<label for="employeePF_proj">
		<g:message code="salary.employeePF_proj.label" default="Employee PF proj" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="employeePF_proj" value="${fieldValue(bean: salaryInstance, field: 'employeePF_proj')}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: salaryInstance, field: 'employerPF', 'error')} required">
	<label for="employerPF">
		<g:message code="salary.employerPF.label" default="Employer PF" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="employerPF" value="${fieldValue(bean: salaryInstance, field: 'employerPF')}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: salaryInstance, field: 'employerPF_proj', 'error')} required">
	<label for="employerPF_proj">
		<g:message code="salary.employerPF_proj.label" default="Employer PF proj" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="employerPF_proj" value="${fieldValue(bean: salaryInstance, field: 'employerPF_proj')}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: salaryInstance, field: 'hra', 'error')} required">
	<label for="hra">
		<g:message code="salary.hra.label" default="Hra" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="hra" value="${fieldValue(bean: salaryInstance, field: 'hra')}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: salaryInstance, field: 'hra_proj', 'error')} required">
	<label for="hra_proj">
		<g:message code="salary.hra_proj.label" default="Hraproj" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="hra_proj" value="${fieldValue(bean: salaryInstance, field: 'hra_proj')}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: salaryInstance, field: 'medical', 'error')} required">
	<label for="medical">
		<g:message code="salary.medical.label" default="Medical" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="medical" value="${fieldValue(bean: salaryInstance, field: 'medical')}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: salaryInstance, field: 'medical_proj', 'error')} required">
	<label for="medical_proj">
		<g:message code="salary.medical_proj.label" default="Medicalproj" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="medical_proj" value="${fieldValue(bean: salaryInstance, field: 'medical_proj')}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: salaryInstance, field: 'special', 'error')} required">
	<label for="special">
		<g:message code="salary.special.label" default="Special" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="special" value="${fieldValue(bean: salaryInstance, field: 'special')}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: salaryInstance, field: 'special_proj', 'error')} required">
	<label for="special_proj">
		<g:message code="salary.special_proj.label" default="Specialproj" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="special_proj" value="${fieldValue(bean: salaryInstance, field: 'special_proj')}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: salaryInstance, field: 'year', 'error')} required">
	<label for="year">
		<g:message code="salary.year.label" default="Year" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="year" type="number" value="${salaryInstance.year}" required=""/>

</div>

