<%@ page import="new1.Employeestatus" %>



<div class="fieldcontain ${hasErrors(bean: employeestatusInstance, field: 'status', 'error')} ">
	<label for="status">
		<g:message code="employeestatus.status.label" default="Status" />
		
	</label>
	<g:checkBox name="status" value="${employeestatusInstance?.status}" />

</div>

<div class="fieldcontain ${hasErrors(bean: employeestatusInstance, field: 'username', 'error')} required">
	<label for="username">
		<g:message code="employeestatus.username.label" default="Username" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="username" required="" value="${employeestatusInstance?.username}"/>

</div>

