<%@ page import="new1.Checkin_checkout" %>
<!DOCTYPE html>
<html>
	<head>
	<style type="text/css">
	.dropbtn {
    background-color: #4CAF50;
    color: white;
    padding: 16px;
    font-size: 16px;
    border: none;
    cursor: pointer;
}

/* Dropdown button on hover & focus */
.dropbtn:hover, .dropbtn:focus {
    background-color: #3e8e41;
}

/* The container <div> - needed to position the dropdown content */
.dropdown {
    position: relative;
    display: inline-block;
}

/* Dropdown Content (Hidden by Default) */
.dropdown-content {
    display: none;
    position: absolute;
    background-color: #f9f9f9;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
}

/* Links inside the dropdown */
.dropdown-content a {
    color: black;
    padding: 12px 16px;
    text-decoration: none;
    display: block;
}

/* Change color of dropdown links on hover */
.dropdown-content a:hover {background-color: #f1f1f1}

/* Show the dropdown menu (use JS to add this class to the .dropdown-content container when the user clicks on the dropdown button) */
.show {display:block;}
	</style>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'checkin_checkout.label', default: 'Checkin_checkout')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
		<script type="text/javascript">
			$(document).ready(function(){
				$("#1").click(function(){
					$.ajax({
						method: "GET",
						url:  "./entry"					 
					})
					.done(function( msg ) {
						$("#1").hide();
						$("#12").show();
					});
				});
				$("#12").click(function(){
					$.ajax({
						method: "GET",
						url:  "./exit" 					 
					})
					.done(function( msg ) {
						$("#12").hide();
						$("#1").show();
					});
				});
			})
		</script>
	</head>
	<body>
		<span class="info_box_icon icon_group"></span>
	<table style= " width:50%">
	<tr>
		<td>  
			<button id="1"> checkin </button>
			<button style="display:none;" class="btn-success" id="12"> checkout </button>
	    </td> 
	<td>
	    <div class="pull-right">
           <input type="checkbox" class="js-switch mini-switch" checked>
           harekrishna
          </div>
		 
	    <div class="dropdown">
            <button onclick="myFunction()" class="dropbtn"> attendance </button>
                <div id="myDropdown" class="dropdown-content">
                    <form action = "./lastweek">
                    	<button>  	 lastweek </button>
                    </form>
                    <form action= "./lastmonth">
                    	<button> Last month </button>
                    </form>
                    <form action = "./lastyear"> 
                    <button>Last Year </button>
                    </form>
                </div>
        </div>
	</td>
	</tr>
	</table>
	<script type="text/javascript">
		
		/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
        function myFunction() {
            document.getElementById("myDropdown").classList.toggle("show");
}

// Close the dropdown menu if the user clicks outside of it
                window.onclick = function(event) {
                    if (!event.target.matches('.dropbtn')) {

                        var dropdowns = document.getElementsByClassName("dropdown-content");
                        var i;
                        for (i = 0; i < dropdowns.length; i++) {
                            var openDropdown = dropdowns[i];
                            if (openDropdown.classList.contains('show')) {
                                 openDropdown.classList.remove('show');
      }
    }
  }
}
</script>
	
		<a href="#list-checkin_checkout" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-checkin_checkout" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="checkin_date" title="${message(code: 'checkin_checkout.checkin_date.label', default: 'Checkindate')}" />
					
						<g:sortableColumn property="checkout_date" title="${message(code: 'checkin_checkout.checkout_date.label', default: 'Checkoutdate')}" />
					
						<g:sortableColumn property="employee_id" title="${message(code: 'checkin_checkout.employee_id.label', default: 'Employeeid')}" />
					
						<g:sortableColumn property="time_duration" title="${message(code: 'checkin_checkout.time_duration.label', default: 'Timeduration')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${checkin_checkoutInstanceList}" status="i" var="checkin_checkoutInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${checkin_checkoutInstance.id}">${fieldValue(bean: checkin_checkoutInstance, field: "checkin_date")}</g:link></td>
					
						<td><g:formatDate date="${checkin_checkoutInstance.checkout_date}" /></td>
					
						<td>${fieldValue(bean: checkin_checkoutInstance, field: "employee_id")}</td>
					
						<td>${fieldValue(bean: checkin_checkoutInstance, field: "time_duration")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${checkin_checkoutInstanceCount ?: 0}" />
			</div>
		</div>
		  <script src="http://yukon.tzdthemes.com/html/assets/lib/switchery/dist/switchery.min.js"></script>
	</body>
</html>
