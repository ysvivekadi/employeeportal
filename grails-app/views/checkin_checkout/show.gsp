
<%@ page import="checkin2.Checkin_checkout" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'checkin_checkout.label', default: 'Checkin_checkout')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-checkin_checkout" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-checkin_checkout" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list checkin_checkout">
			
				<g:if test="${checkin_checkoutInstance?.checkin_date}">
				<li class="fieldcontain">
					<span id="checkin_date-label" class="property-label"><g:message code="checkin_checkout.checkin_date.label" default="Checkindate" /></span>
					
						<span class="property-value" aria-labelledby="checkin_date-label"><g:formatDate date="${checkin_checkoutInstance?.checkin_date}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${checkin_checkoutInstance?.checkout_date}">
				<li class="fieldcontain">
					<span id="checkout_date-label" class="property-label"><g:message code="checkin_checkout.checkout_date.label" default="Checkoutdate" /></span>
					
						<span class="property-value" aria-labelledby="checkout_date-label"><g:formatDate date="${checkin_checkoutInstance?.checkout_date}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${checkin_checkoutInstance?.employee_id}">
				<li class="fieldcontain">
					<span id="employee_id-label" class="property-label"><g:message code="checkin_checkout.employee_id.label" default="Employeeid" /></span>
					
						<span class="property-value" aria-labelledby="employee_id-label"><g:fieldValue bean="${checkin_checkoutInstance}" field="employee_id"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${checkin_checkoutInstance?.time_duration}">
				<li class="fieldcontain">
					<span id="time_duration-label" class="property-label"><g:message code="checkin_checkout.time_duration.label" default="Timeduration" /></span>
					
						<span class="property-value" aria-labelledby="time_duration-label"><g:fieldValue bean="${checkin_checkoutInstance}" field="time_duration"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:checkin_checkoutInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${checkin_checkoutInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
