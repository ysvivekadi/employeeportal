<%@ page import="checkin2.Checkin_checkout" %>



<div class="fieldcontain ${hasErrors(bean: checkin_checkoutInstance, field: 'checkin_date', 'error')} required">
	<label for="checkin_date">
		<g:message code="checkin_checkout.checkin_date.label" default="Checkindate" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="checkin_date" precision="day"  value="${checkin_checkoutInstance?.checkin_date}"  />

</div>

<div class="fieldcontain ${hasErrors(bean: checkin_checkoutInstance, field: 'checkout_date', 'error')} required">
	<label for="checkout_date">
		<g:message code="checkin_checkout.checkout_date.label" default="Checkoutdate" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="checkout_date" precision="day"  value="${checkin_checkoutInstance?.checkout_date}"  />

</div>

<div class="fieldcontain ${hasErrors(bean: checkin_checkoutInstance, field: 'employee_id', 'error')} required">
	<label for="employee_id">
		<g:message code="checkin_checkout.employee_id.label" default="Employeeid" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="employee_id" type="number" value="${checkin_checkoutInstance.employee_id}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: checkin_checkoutInstance, field: 'time_duration', 'error')} required">
	<label for="time_duration">
		<g:message code="checkin_checkout.time_duration.label" default="Timeduration" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="time_duration" required="" value="${checkin_checkoutInstance?.time_duration}"/>

</div>

