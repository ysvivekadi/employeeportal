<%@ page import="new1.Project" %>



<div class="fieldcontain ${hasErrors(bean: projectInstance, field: 'manager', 'error')} required">
	<label for="manager">
		<g:message code="project.manager.label" default="Manager" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="manager" name="manager.id" from="${new1.Manager.list()}" optionKey="id" required="" value="${projectInstance?.manager?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: projectInstance, field: 'name', 'error')} required">
	<label for="name">
		<g:message code="project.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="name" required="" value="${projectInstance?.name}"/>

</div>

