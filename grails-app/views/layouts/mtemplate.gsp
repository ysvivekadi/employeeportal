<!DOCTYPE html>
<html
    <head>
        <meta charset="UTF-8">
        <title>TE Employee portal</title>
        <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
        <asset:stylesheet href="bootstrap.min.css" rel="stylesheet" media="all"/>
        <asset:stylesheet href="icons/elegant/style.css" rel="stylesheet" media="all"/>
        <asset:stylesheet href="icons/elusive/css/elusive-webfont.css" rel="stylesheet" media="all"/>
        <asset:stylesheet href="icons/flags/flags.css" rel="stylesheet" media="all"/>
        <asset:javascript src="malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css" rel="stylesheet" media="all"/>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
        <asset:stylesheet href="main.min.css" rel="stylesheet" media="all" id="mainCss"/>
        <asset:stylesheet href="print.css" rel="stylesheet" media="print"/>
        <asset:javascript src="jquery.min.js"/>
        <asset:javascript src="jqueryCookie.min.js"/>
        <asset:javascript src="bootstrap.min.js"/>
        <asset:javascript src="yukon_all.js"/>
        <script>
            history.pushState(null, document.title, location.href);
            window.addEventListener('popstate', function (event)
            {
              history.pushState(null, document.title, location.href);
            });
        </script> 

        <g:layoutHead/>
    </head>
    <body class="side_menu_active side_menu_expanded">
        <div id="page_wrapper">
        <header id="main_header">
            <div class="container-fluid">
                <div class="brand_section">
                    <a href="..\manager\abc">
                        <asset:image src="logo.png" alt="site_logo" width="150" height="60"/>
                    </a>
                </div>
                <div class="header_user_actions dropdown">
                    <div data-toggle="dropdown" class="dropdown-toggle user_dropdown">
                        <div class="user_avatar">
                            <asset:image src="avatars/avatar08_tn.png" alt="" title="Carrol Clark (carrol@example.com)" width="38" height="38"/>
                        </div>
                        <span class="caret"></span>
                    </div>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="pages-user_profile.html">User Profile</a></li>
                        <!-- <li><a href="#">Check in</a></li> -->
                        <li><a href="..\manager\index">Log Out</a></li>
                    </ul>
                </div>
            </div>
        </header>
        <g:layoutBody/>
        <div id="main_wrapper">
            <nav id="main_menu">
                <div class="menu_wrapper">
                    <ul>
                        <li class="first_level">
                            <a href="..\manager\abc">
                            <span class="icon_house_alt first_level_icon"></span>
                            <span class="menu-title">Dashboard</span>
                            </a>
                        </li>

                        <li class="first_level">
                            <a href="..\    manager\empcreation">
                            <span style="padding:0px 4px 0px 1px"><asset:image src="person.png"/></span>
                            <span class="menu-title">New employee</span>
                            </a>
                        </li>
                        <li class="first_level">
                            <a href="..\manager\check">
                            <span style="padding:0px 4px 0px 1px"><asset:image src="timer.png"/></span>
                            <span class="menu-title">Checkin/Checkout</span>
                            </a>
                        </li>
                        <li class="first_level">
                            <a href="..\manager\timesheet">
                            <span class="icon_document_alt first_level_icon"></span>
                            <span class="menu-title">Timesheet</span>
                            </a>
                        </li>
                        <li class="first_level">
                            <a href="../project_subtask/gantt">
                                <span style="padding:0px 5px 0px 1px"><asset:image src="leftalign.png"/></span>
                                <span class="menu-title">Gantt Chart</span>
                       
                            </a>
                        </li>
                        <li class="first_level">
                            <a href="../manager/sal">
                            <span style="padding:0px 3px 0px 2px"> <asset:image src="dollar-20.png"/></span>
                            <span class="menu-title">Salary structure</span>
                            </a>
                        </li>
                        <li class="first_level">
                            <a href="#">
                            <span style="padding:0px 3px 0px 2px"> <asset:image src="tax.png"/></span>
                            <span class="menu-title">Tax declaration</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div><!-- 
        <script src="http://yukon.tzdthemes.com/html/assets/js/jquery.min.js"></script>
        <script src="http://yukon.tzdthemes.com/html/assets/js/jqueryCookie.min.js"></script>
        <script src="http://yukon.tzdthemes.com/html/assets/bootstrap/js/bootstrap.min.js"></script>
        <script src="http://yukon.tzdthemes.com/html/assets/js/retina.min.js"></script>
        <script src="http://yukon.tzdthemes.com/html/assets/lib/switchery/dist/switchery.min.js"></script>
        <script src="http://yukon.tzdthemes.com/html/assets/lib/typeahead/typeahead.bundle.min.js"></script>
        <script src="http://yukon.tzdthemes.com/html/assets/js/fastclick.min.js"></script>
        <script src="http://yukon.tzdthemes.com/html/assets/lib/jquery-match-height/jquery.matchHeight-min.js"></script>
        <script src="http://yukon.tzdthemes.com/html/assets/lib/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
        <script src="http://yukon.tzdthemes.com/html/assets/js/yukon_all.js"></script>
        <script src="http://yukon.tzdthemes.com/html/assets/lib/d3/d3.min.js"></script>
        <script src="http://yukon.tzdthemes.com/html/assets/lib/c3/c3.min.js"></script> -->
    </body>
</html>