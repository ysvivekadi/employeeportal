<%@ page import="new1.Project_task" %>



<div class="fieldcontain ${hasErrors(bean: project_taskInstance, field: 'name', 'error')} required">
	<label for="name">
		<g:message code="project_task.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="name" required="" value="${project_taskInstance?.name}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: project_taskInstance, field: 'project', 'error')} required">
	<label for="project">
		<g:message code="project_task.project.label" default="Project" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="project" name="project.id" from="${new1.Project.list()}" optionKey="id" required="" value="${project_taskInstance?.project?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: project_taskInstance, field: 'start_date', 'error')} required">
	<label for="start_date">
		<g:message code="project_task.start_date.label" default="Startdate" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="start_date" precision="day"  value="${project_taskInstance?.start_date}"  />

</div>

<div class="fieldcontain ${hasErrors(bean: project_taskInstance, field: 'sub_task', 'error')} ">
	<label for="sub_task">
		<g:message code="project_task.sub_task.label" default="Subtask" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${project_taskInstance?.sub_task?}" var="s">
    <li><g:link controller="project_subtask" action="show" id="${s.id}">${s?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="project_subtask" action="create" params="['project_task.id': project_taskInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'project_subtask.label', default: 'Project_subtask')])}</g:link>
</li>
</ul>


</div>

