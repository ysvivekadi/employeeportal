
<%@ page import="new1.Project_task" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'project_task.label', default: 'Project_task')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-project_task" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-project_task" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="name" title="${message(code: 'project_task.name.label', default: 'Name')}" />
					
						<th><g:message code="project_task.project.label" default="Project" /></th>
					
						<g:sortableColumn property="start_date" title="${message(code: 'project_task.start_date.label', default: 'Startdate')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${project_taskInstanceList}" status="i" var="project_taskInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${project_taskInstance.id}">${fieldValue(bean: project_taskInstance, field: "name")}</g:link></td>
					
						<td>${fieldValue(bean: project_taskInstance, field: "project")}</td>
					
						<td><g:formatDate date="${project_taskInstance.start_date}" /></td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${project_taskInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
