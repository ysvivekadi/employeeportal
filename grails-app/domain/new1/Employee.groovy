package new1
 
class Employee {
 
	String username
	String password
	static hasOne = [profile: Profile] 
	static belongsTo = [manager : Manager]
	static hasMany = [timesheet: Timesheet, checkin_checkout: Checkin_checkout, salary:Salary , subtask : Project_subtask];
	static constraints = {
		subtask nullable: true;
	}
	/*String toString(){
		return username;
	}*/
 }
