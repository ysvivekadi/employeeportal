package new1

class Project {
	String project_name
	static hasMany = [tasks:Project_task]
	static belongsTo = [manager:Manager]
 	Date start_time
    static constraints = {
		
    }
    String toString(){
		return project_name
	}
}
