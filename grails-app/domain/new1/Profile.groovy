package new1

class Profile {
	String first
	String last
	String designation
	String gender
	Date date
	String reportingto
	String department
	String location
	String organization
	String panno
	String email
	String nationality
	String mobile
	static belongsTo = [employee: Employee]
    static constraints = {
    }
}
