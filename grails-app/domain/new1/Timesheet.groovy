package new1

class Timesheet {
	Date fromdate;
	Date todate;
	String project;
	String task;
	String description;
	int work_hour;
	Employee employee;
    static constraints = {
    	fromdate(blank:false)
    	todate(blank:false)
    	task(blank:false)
    	description(blank:false)
    	work_hour(blank:false)
    }	
}
