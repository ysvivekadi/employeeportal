package new1


import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import org.json.JSONObject;
import org.json.JSONArray;
import java.text.SimpleDateFormat;
import java.text.DateFormat;

//@Transactional(readOnly = true)
class Project_subtaskController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE" , testing:"GET", deleting:"GET", editing:"GET"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Project_subtask.list(params), model:[project_subtaskInstanceCount: Project_subtask.count()]
    }

    def show(Project_subtask project_subtaskInstance) {
        respond project_subtaskInstance
    }
    def gantt(Integer max) { 

        
        def _projectlist = Project.findAllByManager(session.manage) 
        JSONObject top = new JSONObject();
        JSONArray  data = new JSONArray();
        JSONArray  links = new JSONArray();
        int id = 1, proj_id=0, proj_task_id=0;
        int  link_id=0;
        int task_id =0;
        DateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            
        for(project in _projectlist){
            JSONObject proj = new JSONObject();
            JSONObject pro_task_link = new JSONObject();
            proj.put("id",id);
            proj_id=id++;
            task_id=id;
            proj.put("text",project.project_name);
            proj.put("type","project"); 
            proj.put("start_date",outputFormat.format(project.start_time));
            proj.put("open",true);
            proj.put("parent",0);
            proj.put("owner"," ");
            def tasks =  Project_task.findAllByProject(project);
                int sums=0;
                double avgs;
                int lens=tasks.size();
                if(lens>0)
                {
                    for(int i=0; i <lens ;i++)
                    {    def st =  Project_subtask.findAllByProject_task(tasks[i]);
                        for(int k=0; k<st.size() ;k++)
                        {
                        sums=sums+st[k].progress
                    }
                    }
                    avgs=(sums*1.0)/lens
                    proj.put("progress",avgs/100);
                }
                else
                proj.put("progress",0.0);
            
            pro_task_link.put("id",link_id++);
            pro_task_link.put("source",proj_id);
            pro_task_link.put("target",task_id);
            pro_task_link.put("type","1");
            links.put(pro_task_link);
            data.put(proj);
            
            def proj_tasks =  Project_task.findAllByProject(project);
            for(projec_task in proj_tasks) {
                JSONObject task = new JSONObject();
                JSONObject link = new JSONObject();
                task.put("id",id);
                proj_task_id = id++;
                task.put("owner"," ");
                task.put("text",projec_task.name);
                task.put("type","task"); 
                task.put("start_date",outputFormat.format(projec_task.start_date));
                task.put("open",true);
                task.put("parent",proj_id);
                def subtasks =  Project_subtask.findAllByProject_task(projec_task);
                int sum=0;
                double avg;
                int len=subtasks.size();
                if(len>0)
                {
                    for(int i=0; i < subtasks.size();i++)
                    {
                        sum=sum+subtasks[i].progress
                    }
                    avg=(sum*1.0)/len
                    task.put("progress",avg/100);
                }
                else
                task.put("progress",0.0);
                data.put(task);
                if(task_id != proj_task_id){

                    link.put("id",link_id++);
                    link.put("source",task_id);
                    link.put("target",proj_task_id);
                    link.put("type","0");
                    links.put(link);
                }
                
                def proj_subtasks =  Project_subtask.findAllByProject_task(projec_task);
                for(projec_subtask in proj_subtasks) {
                    JSONObject sub_task = new JSONObject();
                    JSONObject linkin = new JSONObject();
                    sub_task.put("id",id);              
                    sub_task.put("text",projec_subtask.subtask_name);
                    sub_task.put("type","sub_task"); 
                    sub_task.put("start_date",outputFormat.format(projec_subtask.start_time));
                    sub_task.put("end_date",outputFormat.format(projec_subtask.end_time));
                    sub_task.put("open",false);
                    sub_task.put("parent",proj_task_id);
                    
                    
                    sub_task.put("owner", projec_subtask.employee.id);
                    
                    sub_task.put("progress",projec_subtask.progress/100);
                    data.put(sub_task);
                    linkin.put("id",link_id++);
                    linkin.put("source",proj_task_id);
                    linkin.put("target",id++);
                    linkin.put("type","2");
                    links.put(linkin);
                }
            } 
        }
        top.put("data",data);
        top.put("links",links);
        
        def employee_list = Employee.findAllByManager(session.manage);
            //  JSONObject emp_obj = new JSONObject();
            JSONArray  emp_data = new JSONArray();
            int eid =1;
            for(employee in employee_list){
                JSONObject employ = new JSONObject();
                employ.put("key",employee.id);
                employ.put("label",employee.username);
                emp_data.put(employ);
            }
        //emp_obj.put("emp_data",emp_data);
        System.out.println("done it")
        System.out.println(top.toString())
        print(flash.message)
        render(view:'gantt',model:[jsonData1:top.toString(), jsonData2:emp_data.toString(), abc:flash.meassage]);
    }
    def testing()
    {
       System.out.println("hvjgvgjvjvjvjvjgvgjvjvjvhjjvjvgvjgjvjvvjvjhvjhvjhvjhvjgvjjjvj");
       def emp = Employee.findAllById(params.emp_id)
       /*def hasgotwork=Project_subtask.findAllByEmployee(emp[0])
       if(hasgotwork)
       {
        print("went inside if")
        flash.message="error"
        render(view:"gantt")
        return
       }
       print("went beyond if")*/
       java.util.Date fecha = new java.util.Date(params.end_time);
       java.util.Date fecha1 = new java.util.Date(params.start_time);
       DateFormat formatter = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy");
       Date date1
       Date date2
       date1 = (Date)formatter.parse(fecha.toString());
       date2 = (Date)formatter.parse(fecha1.toString());
       def sample=new Project_subtask();
       def pro=Project.findAllByProject_nameAndManager(params.pro_name , session.manage)
       def xyz=Project_task.findAllByNameAndProject(params.task_name , pro)
       
       
       
       
       
       sample.end_time=date1
       int p=Integer.parseInt(params.progress);
       sample.progress=p
       
       
       sample.start_time=date2
       sample.subtask_name=params.subtask_name
       System.out.println (date1)
       System.out.println (date2)
       System.out.println (params.subtask_name)
       System.out.println (params.progress)
       System.out.println (xyz)
       System.out.println(emp)
       sample.employee=emp[0]
       sample.save()
       
       xyz[0].addToSub_task(sample)
       sample.save(flush:true)
       System.out.println(xyz[0])
       System.out.println("done it")


       
   }
   def deleting()
   {
    System.out.println("inside")
    def pro=Project.findAllByProject_nameAndManager(params.pro_name , session.manage)
    def task=Project_task.findAllByNameAndProject(params.task_name , pro)
    def xyz=Project_subtask.findAllBySubtask_nameAndProject_task(params.subtask_name , task)
        //def emp = Employee.findAllBySubtask(xyz)
        //emp[0].delete(flush:true)
        //emp[0].save(flush:true)
        
        xyz[0].delete(flush:true)
        
        
        
        System.out.println("done it")
    }
    
    def editing()
    {
        
        
    }
    
    def create() {
        respond new Project_subtask(params)
    }

    @Transactional
    def save(Project_subtask project_subtaskInstance) {
        if (project_subtaskInstance == null) {
            notFound()
            return
        }

        if (project_subtaskInstance.hasErrors()) {
            respond project_subtaskInstance.errors, view:'create'
            return
        }

        project_subtaskInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'project_subtask.label', default: 'Project_subtask'), project_subtaskInstance.id])
                redirect project_subtaskInstance
            }
            '*' { respond project_subtaskInstance, [status: CREATED] }
        }
    }

    def edit(Project_subtask project_subtaskInstance) {
        respond project_subtaskInstance
    }

    @Transactional
    def update(Project_subtask project_subtaskInstance) {
        if (project_subtaskInstance == null) {
            notFound()
            return
        }

        if (project_subtaskInstance.hasErrors()) {
            respond project_subtaskInstance.errors, view:'edit'
            return
        }

        project_subtaskInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Project_subtask.label', default: 'Project_subtask'), project_subtaskInstance.id])
                redirect project_subtaskInstance
            }
            '*'{ respond project_subtaskInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Project_subtask project_subtaskInstance) {
        System.out.println("inside")
        if (project_subtaskInstance == null) {
            notFound()
            return
        }

        project_subtaskInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Project_subtask.label', default: 'Project_subtask'), project_subtaskInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'project_subtask.label', default: 'Project_subtask'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
