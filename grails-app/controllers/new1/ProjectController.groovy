package new1



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import java.text.DateFormat
import java.text.SimpleDateFormat

//@Transactional(readOnly = true)
class ProjectController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE" , testing:"GET" , deleting:"GET"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Project.list(params), model:[projectInstanceCount: Project.count()]
    }

    def show(Project projectInstance) {
        respond projectInstance
    }

    def create() {
        respond new Project(params)
    }

    def testing()
    {
        System.out.println("yes u are inside jhvjgcg nbvhtfgvnvnvgv")
        
        java.util.Date fecha1 = new java.util.Date(params.start_time);
        DateFormat formatter = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy");
        
        Date date2
        
        date2 = (Date)formatter.parse(fecha1.toString());
        def sample=new Project();
        def xyz=Manager.findAllByUsernameAndPassword(session.manage.username , session.manage.password)  
        
        sample.start_time=date2
        sample.project_name=params.pro_name
        
        System.out.println (date2)
        
        
        
        sample.save()
        xyz[0].addToProjects(sample)
        sample.save(flush:true)
        System.out.println("///////"+sample.start_time+" "+sample.project_name+" "+sample.manager+"///////////")
System.out.println("completed completed completed completed")

    }

    def deleting()
    {
        System.out.println("inside")
        def xyz=Project.findAllByProject_nameAndManager(params.pro_name , session.manage)
        System.out.println(xyz[0])
        xyz[0].delete(flush:true)
        System.out.println("done it")
    }

    @Transactional
    def save(){
        if(params == null){
            redirect(controller:"Manager" ,action:"abc")
            flash.message=''
            return
        }
        System.out.println(params.id);
        def project = Project.findByName(params.name)
        if(project!=null){
        flash.message="projectname already exist"
            render(view:"index")
        }
        else{
            System.out.println(params.name);
            def newPro = new Project();
            newPro.name = params.name;
            newPro.manager = Manager.findById(params.id)
            if(newPro.save(flush:true)){
                flash.message="Project saved"
                redirect(controller: "Manager", action:"abc", params:[username: newPro.manager.username])
            }
            else{
                System.out.println("else"+params.id);
                flash.message="Please enter valid data"
                render('Not saved')
            }
        }
    }
    def edit(Project projectInstance) {
        respond projectInstance
    }

    @Transactional
    def update(Project projectInstance) {
        if (projectInstance == null) {
            notFound()
            return
        }

        if (projectInstance.hasErrors()) {
            respond projectInstance.errors, view:'edit'
            return
        }

        projectInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Project.label', default: 'Project'), projectInstance.id])
                redirect projectInstance
            }
            '*'{ respond projectInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Project projectInstance) {

        if (projectInstance == null) {
            notFound()
            return
        }

        projectInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Project.label', default: 'Project'), projectInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'project.label', default: 'Project'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
