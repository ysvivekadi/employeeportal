package new1



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class RentdetailsController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Rentdetails.list(params), model:[rentdetailsInstanceCount: Rentdetails.count()]
    }

    def show(Rentdetails rentdetailsInstance) {
        respond rentdetailsInstance
    }

    def create() {
        respond new Rentdetails(params)
    }

    @Transactional
    def save() {
         System.out.println("")
       if(params == null){
            redirect(controller:"Employee", action:"dec");
            flash.message=''
            return
        }
        def mall = Rentdetails.findByLocation(params.location)
        if(mall){
            flash.message="alredy given";
            redirect(controller:"Employee", action:"dec");
        }
        else{
            def newM = new Rentdetails();
            int b = params.totalrent.toInteger();
            newM.total_rent=b
            System.out.println(b)
            String from=params.fromdate.toString()
            String to=params.todate.toString()
            newM.from_date= from
            newM.to_date= to
            System.out.println( newM.from_date)

            String loc=params.location.toString();
             System.out.println(loc)
             newM.location=loc 
              println params.ismetro as boolean
             newM.ismetro=params.ismetro as boolean
              System.out.println()
            newM.employee = Employee.findByUsername(params.username)
            System.out.println(params.username)
            if(newM.save(flush:true)){
                flash.message =='SUCCESS! Rentdetails submitted '
                println 'harihari'
               redirect(controller:"Employee", action:"dec");
            }
            else{
                 flash.message =='FAILED! Rentdetails not submitted'
                 redirect(controller:"Employee", action:"dec");
            }
        }
    }

    def edit(Rentdetails rentdetailsInstance) {
        respond rentdetailsInstance
    }

    @Transactional
    def update(Rentdetails rentdetailsInstance) {
        if (rentdetailsInstance == null) {
            notFound()
            return
        }

        if (rentdetailsInstance.hasErrors()) {
            respond rentdetailsInstance.errors, view:'edit'
            return
        }

        rentdetailsInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Rentdetails.label', default: 'Rentdetails'), rentdetailsInstance.id])
                redirect rentdetailsInstance
            }
            '*'{ respond rentdetailsInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Rentdetails rentdetailsInstance) {

        if (rentdetailsInstance == null) {
            notFound()
            return
        }

        rentdetailsInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Rentdetails.label', default: 'Rentdetails'), rentdetailsInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'rentdetails.label', default: 'Rentdetails'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
