package new1



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class LifeinsController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Lifeins.list(params), model:[lifeinsInstanceCount: Lifeins.count()]
    }

    def show(Lifeins lifeinsInstance) {
        respond lifeinsInstance
    }

    def create() {
        respond new Lifeins(params)
    }

    @Transactional
     def save() {
        System.out.println("")
       if(params == null){
            redirect(controller:"Employee", action:"dec");
            flash.message=''
            return
        }
        def mall = Lifeins.findByPolno(params.pno)
        if(mall){
            flash.message="alredy given";
            redirect(controller:"Employee", action:"dec");
        }
        else{
            def newM = new Lifeins();
            
            String from=params.idate.toString()
            
            int to=params.pno.toInteger()
            newM.polno= to
            newM.idate=from
             String fr=params.iperson.toString()
             newM.iperson=fr
             String fro=params.rship.toString()
             newM.rships=fro
             int sa=params.suma.toInteger()
            newM.sassured= sa
            int t=params.annualp.toInteger()
            newM.anpre= t
            int tom=params.eligibled.toInteger()
            newM.ededuct= tom

            System.out.println( )

            
            
            newM.employee = Employee.findByUsername(params.username)
            System.out.println(params.username)
            if(newM.save(flush:true)){
                flash.message =='SUCCESS! Lifeins submitted '
                println 'harihari'
               redirect(controller:"Employee", action:"dec");
            }
            else{
                 flash.message =='FAILED! Lifeins not submitted'
                 redirect(controller:"Employee", action:"dec");
            }
        }
    }


    def edit(Lifeins lifeinsInstance) {
        respond lifeinsInstance
    }

    @Transactional
    def update(Lifeins lifeinsInstance) {
        if (lifeinsInstance == null) {
            notFound()
            return
        }

        if (lifeinsInstance.hasErrors()) {
            respond lifeinsInstance.errors, view:'edit'
            return
        }

        lifeinsInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Lifeins.label', default: 'Lifeins'), lifeinsInstance.id])
                redirect lifeinsInstance
            }
            '*'{ respond lifeinsInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Lifeins lifeinsInstance) {

        if (lifeinsInstance == null) {
            notFound()
            return
        }

        lifeinsInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Lifeins.label', default: 'Lifeins'), lifeinsInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'lifeins.label', default: 'Lifeins'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
