package new1



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class HomeloanController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Homeloan.list(params), model:[homeloanInstanceCount: Homeloan.count()]
    }

    def show(Homeloan homeloanInstance) {
        respond homeloanInstance
    }

    def create() {
        respond new Homeloan(params)
    }

    @Transactional
   def save(){
        if(params == null){
            redirect(controller:"Employee", action:"dec");
            flash.message=''
            return
        }
        def mall = Homeloan.findByArent(params.arent)
        if(mall){
            flash.message="alredy given";
            redirect(controller:"Employee", action:"dec");
        }
        else{
            def newM = new Homeloan();
            System.out.println()
              println params.soccupied as boolean
             newM.soccupied=params.soccupied as boolean
             int d = params.arent.toInteger();
            newM.arent= d;
             int e = params.mtpaid.toInteger();
            newM.mtpaid= e;
             int f= params.hlib.toInteger();
            newM.hli1= f;
             int g= params.hlia.toInteger();
            newM.hli2= g;
            String h=params.hlsd.toString()
            newM.hldate= h;
              println params.ihfl as boolean
             newM.ifloan=params.ihfl as boolean
              System.out.println()
             int j= params.hlamount.toInteger();
            newM.hlamount= j;
             int k = params.cohome.toInteger();
            newM.cohome= k;
            newM.employee = Employee.findByUsername(params.username)
            System.out.println(params.username)
            if(newM.save(flush:true)){
                flash.message =='SUCCESS! Homeloan submitted '
                println 'harihari'
               redirect(controller:"Employee", action:"dec");
            }
            else{
                 flash.message =='FAILED! Homeloan not submitted'
                 redirect(controller:"Employee", action:"dec");
            }
        }
    }


    def edit(Homeloan homeloanInstance) {
        respond homeloanInstance
    }

    @Transactional
    def update(Homeloan homeloanInstance) {
        if (homeloanInstance == null) {
            notFound()
            return
        }

        if (homeloanInstance.hasErrors()) {
            respond homeloanInstance.errors, view:'edit'
            return
        }

        homeloanInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Homeloan.label', default: 'Homeloan'), homeloanInstance.id])
                redirect homeloanInstance
            }
            '*'{ respond homeloanInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Homeloan homeloanInstance) {

        if (homeloanInstance == null) {
            notFound()
            return
        }

        homeloanInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Homeloan.label', default: 'Homeloan'), homeloanInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'homeloan.label', default: 'Homeloan'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
