package new1



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class EmployeestatusController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Employeestatus.list(params), model:[employeestatusInstanceCount: Employeestatus.count()]
    }

    def show(Employeestatus employeestatusInstance) {
        respond employeestatusInstance
    }

    def create() {
        respond new Employeestatus(params)
    }

    @Transactional
    def save(Employeestatus employeestatusInstance) {
        if (employeestatusInstance == null) {
            notFound()
            return
        }

        if (employeestatusInstance.hasErrors()) {
            respond employeestatusInstance.errors, view:'create'
            return
        }

        employeestatusInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'employeestatus.label', default: 'Employeestatus'), employeestatusInstance.id])
                redirect employeestatusInstance
            }
            '*' { respond employeestatusInstance, [status: CREATED] }
        }
    }

    def edit(Employeestatus employeestatusInstance) {
        respond employeestatusInstance
    }

    @Transactional
    def update(Employeestatus employeestatusInstance) {
        if (employeestatusInstance == null) {
            notFound()
            return
        }

        if (employeestatusInstance.hasErrors()) {
            respond employeestatusInstance.errors, view:'edit'
            return
        }

        employeestatusInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Employeestatus.label', default: 'Employeestatus'), employeestatusInstance.id])
                redirect employeestatusInstance
            }
            '*'{ respond employeestatusInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Employeestatus employeestatusInstance) {

        if (employeestatusInstance == null) {
            notFound()
            return
        }

        employeestatusInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Employeestatus.label', default: 'Employeestatus'), employeestatusInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'employeestatus.label', default: 'Employeestatus'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
