package new1
import java.net.InetAddress;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.commons.net.ntp.NTPUDPClient; 
import org.apache.commons.net.ntp.TimeInfo;

import java.io.File;
import java.io.FileInputStream;
import java.util.Date;

import jxl.Workbook;
import jxl.format.VerticalAlignment;
import jxl.write.*;


import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional


import java.net.InetAddress;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.commons.net.ntp.NTPUDPClient; 
import org.apache.commons.net.ntp.TimeInfo;

import java.io.File;
import java.io.FileInputStream;
import java.util.Date;

import jxl.Workbook;
import jxl.format.VerticalAlignment;
import jxl.write.*;


@Transactional(readOnly = true)
class TaxSheetController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    WritableWorkbook workbook;
    WritableSheet sheet;
    WritableFont cellFont;
    WritableCellFormat newCellFormat;
    WritableCellFormat cellFormat;
    Label lable;
    Date date;
    int decID;
    String decType;
    Date decDate;
    String decStatus;
    String empID;
    String empName;
    String empDesig;
    String empPAN;
    Date dateOfJoin;
    Date currDate;
    int currYear;
    String yearDuration;
    int startMonth;
    int multiFactor;
    String str_Date;
    SimpleDateFormat formatter;
    Date startDate;
    Date endDate;
    long actualDiff;
    long diff;
    int noOfMonths;;
    SimpleDateFormat monthFormat;
    int yearOfJoin;
    int workingMonths;
    ArrayList<String> months;
    ArrayList<String> years;
    WritableCellFormat headingCellFormat;
    WritableCellFormat boldCenterCellFormat;
    WritableCellFormat noBoldCenterCellFormat;
    int rowSal;
    int noOfSalComp;
    double[] salStruct;
    double grossSalYTD,grossTaxSalYTD;
    double grossSalProj,grossTaxSalProj;
    double grossSalTotal, grossTaxSalTotal;
    WritableCellFormat noBoldRightCellFormat;
    double basicGross,basicTaxGross;
    double hraGross,hraTaxGross;
    double conveyanceGross,conveyanceTaxGross;
    double medicalGross,medicalTaxGross;
    double specialGross,specialTaxGross;
    double employerPFGross,employerPFTaxGross;
    double[] salStructProj;
    double basicGrossProj,basicTaxGrossProj;
    double hraGrossProj,hraTaxGrossProj;
    double conveyanceGrossProj,conveyanceTaxGrossProj;
    double medicalGrossProj,medicalTaxGrossProj;
    double specialGrossProj,specialTaxGrossProj;
    double employerPFGrossProj,employerPFTaxGrossProj;
    WritableCellFormat boldRightCellFormat;
    WritableCellFormat boldLeftCellFormat;
    int lastRowSal;
    int rowBill;
    double medAllowanceTotal;
    double submittedMedAllowance;
    int lastRowBill;
    int rowHRA;
    double rentPaid;
    String isMetro;
    WritableCellFormat lNoBoldLeftCellFormat;
    int rowTax;
    WritableCellFormat rNoBoldRightCellFormat;
    double perks;                               // not implemented
    double profits;
    double totGrossSalary;
    double transportAllowance;
    double totAllowanceExemp;
    double balance;
    double entertainmentAllowance;      //assuming it to be zero as it is not present in the salary structure
    double taxOnEmployment;
    double taxOnEmploymentProj;
    double totTaxOnEmployment;
    double deducAggregate;
    double incomeChargeable;
    double incomeHouseProperty;
    double homeLoanInterest;
    double totOtherIncome;
    double grossTotalIncome;
  //  double publicProvidentFund;
    double employeePF;
    double total9a;
    double section80CCC;
    double section80CCD;
    double dedTotal9a;
    double dedSection80CCC;
    double dedSection80CCD;
    double aggregateDedAmount;
    double lRange1,uRange1,lRange2,uRange2,lRange3,uRange3,lRange4;
    double percent1,percent2,percent3,percent4;
    double totalIncome;
    double round;
    double range1,range2,range3,range4,remain;
    double taxRange1,taxRange2,taxRange3,taxRange4;
    double totalTax;
    double eduCess;
    double taxPayable;
    double reliefSec89;


//  this function is just for purpose of generating random date of joining till the time the date is not taken from database
    Date randDateOfJoin() throws Exception{
            GregorianCalendar gc = new GregorianCalendar();
            int year = randBetween(1980, 2015);
            gc.set(gc.YEAR, year);
            int dayOfYear = randBetween(1, gc.getActualMaximum(gc.DAY_OF_YEAR));
            gc.set(gc.DAY_OF_YEAR, dayOfYear);
            String dateOfJoin_Str = (gc.get(gc.YEAR) + "-" + (gc.get(gc.MONTH) + 1) + "-" + gc.get(gc.DAY_OF_MONTH));
            System.out.println(dateOfJoin_Str);
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-M-d");
            Date dateOfJoin = dateFormat.parse(dateOfJoin_Str);
            return dateOfJoin;
        }
     int randBetween(int start, int end) {
            return start + (int)Math.round(Math.random() * (end - start));
        }
    
    Date currentDate() throws Exception
    {       
        String TIME_SERVER = "time-a.nist.gov";   
        NTPUDPClient timeClient = new NTPUDPClient();
        InetAddress inetAddress = InetAddress.getByName(TIME_SERVER);
        TimeInfo timeInfo = timeClient.getTime(inetAddress);
        long returnTime = timeInfo.getReturnTime();
        Date time = new Date(returnTime);
        return time;
    }
    
    /*
    def generateTaxSheet() throws Exception
    {
        response.setContentType('application/vnd.ms-excel')
        response.setHeader('Content-Disposition', 'Attachment;Filename="salary_sheet.xls"')
        
        try
        {       
            workbook = Workbook.createWorkbook(response.outputStream);
    
             sheet = workbook.createSheet(
                    "Tax Sheet", 0);
    
//          String FilePath = "g:/transenigma intern/tax_sheet.xls";
//          FileInputStream fs = new FileInputStream(FilePath);
//          WritableWorkbook workbook = (WritableWorkbook) Workbook.getWorkbook(fs);
//          WritableSheet sheet = (WritableSheet) workbook.getSheet(0);
            sheet.getSettings().setProtected(true);

            for(int i=0;i<=9;i++)
            {
                sheet.setColumnView(i, 18);
            }
            
            
             cellFont = new WritableFont(WritableFont.TIMES, 14);
            cellFont.setBoldStyle(WritableFont.BOLD);
            cellFont.setColour(Colour.BLACK);

             newCellFormat = new WritableCellFormat(cellFont);
            newCellFormat.setBackground(Colour.PALE_BLUE);
            newCellFormat.setAlignment(Alignment.CENTRE);
            newCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
            

             cellFormat = new WritableCellFormat(newCellFormat);
            cellFormat.setBorder(Border.TOP, BorderLineStyle.THICK);
            cellFormat.setBorder(Border.LEFT, BorderLineStyle.THICK);
            cellFormat.setBorder(Border.RIGHT, BorderLineStyle.THICK);
            
            //Merge col[0-9] and row[0]
            sheet.mergeCells(0, 0,9,0);
             lable = new Label(0, 0, 
                "Transenigma Pvt Ltd", cellFormat);
            sheet.addCell(lable);
            
            cellFormat = new WritableCellFormat(newCellFormat);
            cellFormat.setBorder(Border.LEFT, BorderLineStyle.THICK);
            cellFormat.setBorder(Border.RIGHT, BorderLineStyle.THICK);
            
            sheet.mergeCells(0, 1,9,1);
            lable = new Label(0, 1, 
                "Tax Work Sheet", cellFormat);
            sheet.addCell(lable);
            
             date = new Date();
            sheet.mergeCells(0, 2,9,2);
            lable = new Label(0, 2, 
                "Date :"+date.toString(), cellFormat);
            sheet.addCell(lable);
           
            cellFormat = new WritableCellFormat(newCellFormat);
            cellFormat.setBorder(Border.TOP, BorderLineStyle.THICK);
            cellFormat.setBorder(Border.LEFT, BorderLineStyle.THICK);
            cellFormat.setBorder(Border.RIGHT, BorderLineStyle.THICK);          

            sheet.mergeCells(0, 3,9,3);
            lable = new Label(0, 3, 
                "", cellFormat);
            sheet.addCell(lable);
            
//           CELL FONT CHANGING
            
            cellFont = new WritableFont(WritableFont.TIMES, 12);
            cellFont.setBoldStyle(WritableFont.NO_BOLD);
            cellFont.setColour(Colour.BLACK);
            
            newCellFormat = new WritableCellFormat(cellFont);
            newCellFormat.setBackground(Colour.PALE_BLUE);
            newCellFormat.setAlignment(Alignment.LEFT);
            newCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
            
            cellFormat = new WritableCellFormat(newCellFormat);
            cellFormat.setBorder(Border.LEFT, BorderLineStyle.THICK);
            
            
            sheet.mergeCells(0, 4,1,4);
            lable = new Label(0, 4, 
                "", cellFormat);
            sheet.addCell(lable);
            
            cellFormat = new WritableCellFormat(newCellFormat);         
            cellFormat.setBorder(Border.NONE,BorderLineStyle.NONE);
            
//          getting declaration id from Employee database
//             decID = 4;
            
            sheet.mergeCells(2, 4,5,4);
            lable = new Label(2, 4, "", cellFormat);
            sheet.addCell(lable);           
           
            sheet.mergeCells(6, 4,7,4);
            lable = new Label(6, 4, "", cellFormat);
            sheet.addCell(lable);

            cellFormat = new WritableCellFormat(newCellFormat);         
            cellFormat.setBorder(Border.RIGHT,BorderLineStyle.THICK);
            
//          declaration type
//           decType = "Final";
            
            sheet.mergeCells(8, 4,9,4);
            lable = new Label(8, 4, "", cellFormat);
            sheet.addCell(lable);   
            
            cellFormat = new WritableCellFormat(newCellFormat);         
            cellFormat.setBorder(Border.LEFT,BorderLineStyle.THICK);
            cellFormat.setBorder(Border.BOTTOM,BorderLineStyle.THICK);
            
            sheet.mergeCells(0, 5,1,5);
            lable = new Label(0, 5, "", cellFormat);
            sheet.addCell(lable);   
            
            cellFormat = new WritableCellFormat(newCellFormat);         
            cellFormat.setBorder(Border.BOTTOM,BorderLineStyle.THICK);
            
//          declaration date
 //            decDate = new Date();
            
            sheet.mergeCells(2, 5,5,5);
            lable = new Label(2, 5, "", cellFormat);
            sheet.addCell(lable);   
            
            cellFormat = new WritableCellFormat(newCellFormat);         
            cellFormat.setBorder(Border.BOTTOM,BorderLineStyle.THICK);
            
            sheet.mergeCells(6, 5,7,5);
            lable = new Label(6, 5, "", cellFormat);
            sheet.addCell(lable);
            
            cellFormat = new WritableCellFormat(newCellFormat);         
            cellFormat.setBorder(Border.RIGHT,BorderLineStyle.THICK);
            cellFormat.setBorder(Border.BOTTOM,BorderLineStyle.THICK);
            
//          declaration status
//           decStatus = "Approved";
            
            sheet.mergeCells(8, 5,9,5);
            lable = new Label(8, 5, "", cellFormat);
            sheet.addCell(lable);
            
//          CELL FONT CHANGED
            
            cellFont = new WritableFont(WritableFont.TIMES, 12);
            cellFont.setBoldStyle(WritableFont.BOLD);
            cellFont.setColour(Colour.BLACK);
            
            newCellFormat = new WritableCellFormat(cellFont);
            newCellFormat.setBackground(Colour.PALE_BLUE);
            newCellFormat.setAlignment(Alignment.LEFT);
            newCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
            
            cellFormat = new WritableCellFormat(newCellFormat);         
            cellFormat.setBorder(Border.LEFT,BorderLineStyle.THICK);
            
            sheet.mergeCells(0, 6,5,6);
            lable = new Label(0, 6, "Name and Designation of the Employee", cellFormat);
            sheet.addCell(lable);
            
            cellFormat = new WritableCellFormat(newCellFormat);         
            cellFormat.setBorder(Border.LEFT,BorderLineStyle.THICK);
            cellFormat.setAlignment(Alignment.CENTRE);
            
            sheet.mergeCells(6, 6,7,6);
            lable = new Label(6, 6, "PERIOD", cellFormat);
            sheet.addCell(lable);
            
            cellFormat = new WritableCellFormat(newCellFormat);   
            cellFormat.setBorder(Border.LEFT,BorderLineStyle.THICK);
            cellFormat.setBorder(Border.TOP,BorderLineStyle.THICK);
            cellFormat.setBorder(Border.BOTTOM,BorderLineStyle.THICK);
            cellFormat.setAlignment(Alignment.CENTRE);
            
            lable = new Label(6, 7, "FROM", cellFormat);
            sheet.addCell(lable);
            
            cellFormat = new WritableCellFormat(newCellFormat);   
            cellFormat.setBorder(Border.LEFT,BorderLineStyle.THICK);
            cellFormat.setBorder(Border.TOP,BorderLineStyle.THICK);
            cellFormat.setBorder(Border.BOTTOM,BorderLineStyle.THICK);
            cellFormat.setAlignment(Alignment.CENTRE);
            
            lable = new Label(7, 7, "TO", cellFormat);
            sheet.addCell(lable);
            
            cellFormat = new WritableCellFormat(newCellFormat);         
            cellFormat.setBorder(Border.LEFT,BorderLineStyle.THICK);
            cellFormat.setBorder(Border.RIGHT,BorderLineStyle.THICK);
            
//          CELL FONT CHANGED
            
            cellFont = new WritableFont(WritableFont.TIMES, 12);
            cellFont.setBoldStyle(WritableFont.NO_BOLD);
            cellFont.setColour(Colour.BLACK);
            
            newCellFormat = new WritableCellFormat(cellFont);
            newCellFormat.setBackground(Colour.PALE_BLUE);
            newCellFormat.setAlignment(Alignment.LEFT);
            newCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
            
            sheet.mergeCells(8, 6,9,6);
            lable = new Label(8, 6, "ASSESSMENT YEAR", cellFormat);
            sheet.addCell(lable);
            
            cellFormat = new WritableCellFormat(newCellFormat);         
            cellFormat.setBorder(Border.LEFT,BorderLineStyle.THICK);
            
            sheet.mergeCells(0, 7,1,7);
            lable = new Label(0, 7, "Employee ID:", cellFormat);
            sheet.addCell(lable);
            
            cellFormat = new WritableCellFormat(newCellFormat);   
            
            def emp = session.emp
            def profile = Profile.findByEmployeeAndYear(emp,Integer.valueOf(yearQueried))
//          employee ID
             empID = emp.id;
            
            sheet.mergeCells(2, 7,5,7);
            lable = new Label(2, 7, empID, cellFormat);
            sheet.addCell(lable);
            cellFormat = new WritableCellFormat(newCellFormat);         
            cellFormat.setBorder(Border.LEFT,BorderLineStyle.THICK);
            
//          name of the employee
             empName = profile.first+" "+profile.last;
            
            sheet.mergeCells(0, 8,1,8);
            lable = new Label(0, 8, "Name:", cellFormat);
            sheet.addCell(lable);
            
            cellFormat = new WritableCellFormat(newCellFormat);   
            
            sheet.mergeCells(2, 8,5,8);
            lable = new Label(2, 8, empName, cellFormat);
            sheet.addCell(lable);
            
            cellFormat = new WritableCellFormat(newCellFormat);         
            cellFormat.setBorder(Border.LEFT,BorderLineStyle.THICK);
            
            sheet.mergeCells(0, 9,1,9);
            lable = new Label(0, 9, "Designation:", cellFormat);
            sheet.addCell(lable);
            
            cellFormat = new WritableCellFormat(newCellFormat);   
            
//          designation 
             empDesig = profile.designation;
            
            sheet.mergeCells(2, 9,5,9);
            lable = new Label(2, 9, empDesig, cellFormat);
            sheet.addCell(lable);
            
            cellFormat = new WritableCellFormat(newCellFormat);         
            cellFormat.setBorder(Border.LEFT,BorderLineStyle.THICK);
            cellFormat.setBorder(Border.BOTTOM,BorderLineStyle.THICK);
            
            sheet.mergeCells(0, 10,1,10);
            lable = new Label(0, 10, "PAN/GIR No.:", cellFormat);
            sheet.addCell(lable);
            
            cellFormat = new WritableCellFormat(newCellFormat);   
            cellFormat.setBorder(Border.BOTTOM,BorderLineStyle.THICK);
            
//          pan no
             empPAN = profie.panno;
            
            sheet.mergeCells(2, 10,5,10);
            lable = new Label(2, 10, empPAN, cellFormat);
            sheet.addCell(lable);
            
            cellFormat = new WritableCellFormat(newCellFormat);   
            cellFormat.setBorder(Border.LEFT,BorderLineStyle.THICK);
            cellFormat.setBorder(Border.BOTTOM,BorderLineStyle.THICK);
            cellFormat.setAlignment(Alignment.CENTRE);

             dateOfJoin = profile.date;
            
             currDate = currentDate();  
                        
            SimpleDateFormat yearFormat = new SimpleDateFormat("yyyy");
            currYear = Integer.valueOf(yearFormat.format(currDate));
            
//          get year duration through params
             yearDuration= params.year          //"Apr 15 - Mar 16"; 
            
            String yearQueried = yearDuration.substring(4, 6);   // 15 in this case
            if(Integer.valueOf(yearQueried) < 70)
            {
                yearQueried = String.valueOf(20)+yearQueried;
            }
            else
            {
                yearQueried = String.valueOf(19)+yearQueried;
            }
            
//          startMonth of the current year
             startMonth = 0;  // 0 means april
            
//          if difference between current date and 1 april is less than diff b/w 31 mar and 1 apr, 
//              if current year is year of joining also, then noOfMonths = currMonth-monthOfJoin+1 & startMonth=monthOfJoin
//              else  noOfMonths=currMonth-april+1 & startMonth = april
//          else noOfMonths=12       & startMonth = april
            
//          this variable multiFactor stores whether in Annual column, i have to multiply by 12 if current is not year of join,
//          else, multiplying by multiFactor, where multiFactor = 12 - (monthOfJoin - 4) i.e. 16 - monthOfJoin
             multiFactor = 12;
            
             str_Date = "01/04/"+yearQueried;           
             formatter = new SimpleDateFormat("dd/MM/yy");
             startDate = formatter.parse(str_Date); 
            str_Date = "31/03/"+String.valueOf(Integer.valueOf(yearQueried)+1); 
             endDate = (Date)formatter.parse(str_Date); 
             actualDiff = currDate.getTime() - startDate.getTime();
             diff = endDate.getTime() - startDate.getTime();
            
            if(actualDiff < diff)
            {
                 monthFormat = new SimpleDateFormat("M");
                currDate = currentDate();   
                 yearOfJoin = Integer.valueOf(yearFormat.format(dateOfJoin));               
                if(currYear == yearOfJoin)
                { 
                    multiFactor = 16 - Integer.valueOf(monthFormat.format(dateOfJoin));
                    noOfMonths = Integer.valueOf(monthFormat.format(currDate))-Integer.valueOf(monthFormat.format(dateOfJoin))+1;
                    startMonth = Integer.valueOf(monthFormat.format(dateOfJoin)) - 4;
                }
                else
                {
                    noOfMonths = Integer.valueOf(monthFormat.format(currDate))-3;
                }
                
            }
            else noOfMonths = 12;
            

//          writing annual pay under different components in column 1 (starting index 0)            
             workingMonths = 12 - startMonth;

//          array list to store months
            
             months = new ArrayList<String>();
             years = new ArrayList<String>();
            months.add("Apr");years.add(String.valueOf(yearQueried));
            months.add("May");years.add(String.valueOf(yearQueried));
            months.add("Jun");years.add(String.valueOf(yearQueried));
            months.add("Jul");years.add(String.valueOf(yearQueried));
            months.add("Aug");years.add(String.valueOf(yearQueried));
            months.add("Sep");years.add(String.valueOf(yearQueried));
            months.add("Oct");years.add(String.valueOf(yearQueried));
            months.add("Nov");years.add(String.valueOf(yearQueried));
            months.add("Dec");years.add(String.valueOf(yearQueried));
            months.add("Jan");years.add(String.valueOf(Integer.valueOf(yearQueried)+1));
            months.add("Feb");years.add(String.valueOf(Integer.valueOf(yearQueried)+1));
            months.add("Mar");years.add(String.valueOf(Integer.valueOf(yearQueried)+1));

            
            sheet.mergeCells(6,8,6,10);
            lable = new Label(6, 8, "01-"+months.get(startMonth)+"-"+yearQueried, cellFormat);
            sheet.addCell(lable);
            
            cellFormat = new WritableCellFormat(newCellFormat);   
            cellFormat.setBorder(Border.LEFT,BorderLineStyle.THICK);
            cellFormat.setBorder(Border.BOTTOM,BorderLineStyle.THICK);
            cellFormat.setAlignment(Alignment.CENTRE);

            
            sheet.mergeCells(7,8,7,10);
            lable = new Label(7, 8, "31-Mar-"+String.valueOf(Integer.valueOf(yearQueried)+1), cellFormat);
            sheet.addCell(lable);
            
            cellFormat = new WritableCellFormat(newCellFormat);   
            cellFormat.setBorder(Border.ALL,BorderLineStyle.THICK);
            cellFormat.setAlignment(Alignment.CENTRE);

            sheet.mergeCells(8,7,9,10);
            lable = new Label(8, 7, currYear+"-"+(currYear+1), cellFormat);
            sheet.addCell(lable);
            
//          CELL FONT CHANGED
            
            cellFont = new WritableFont(WritableFont.TIMES, 12);
            cellFont.setBoldStyle(WritableFont.BOLD);
            cellFont.setColour(Colour.BLACK);
            
            newCellFormat = new WritableCellFormat(cellFont);
            newCellFormat.setBackground(Colour.PALE_BLUE);
            newCellFormat.setAlignment(Alignment.LEFT);
            newCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
            
             headingCellFormat = new WritableCellFormat(newCellFormat);   
            headingCellFormat.setBorder(Border.NONE,BorderLineStyle.NONE);
            headingCellFormat.setAlignment(Alignment.LEFT);

            sheet.mergeCells(0,11,9,11);
            lable = new Label(0, 11, "", headingCellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(0,12,9,12);
            lable = new Label(0, 12, "Salary Break - Up (in Rs.):", headingCellFormat);
            sheet.addCell(lable);
            
             boldCenterCellFormat = new WritableCellFormat(newCellFormat);    
            boldCenterCellFormat.setBorder(Border.ALL,BorderLineStyle.THICK);
            boldCenterCellFormat.setAlignment(Alignment.CENTRE);

            sheet.mergeCells(0,13,0,14);
            lable = new Label(0, 13, "S.No.", boldCenterCellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(1,13,3,14);
            lable = new Label(1, 13, "Head of Pay", boldCenterCellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(4,13,5,13);
            lable = new Label(4, 13, "Year to Date", boldCenterCellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(4,14,4,14);
            lable = new Label(4, 14, "Gross", boldCenterCellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(5,14,5,14);
            lable = new Label(5, 14, "Taxable Gross", boldCenterCellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(6,13,7,13);
            lable = new Label(6, 13, "Total", boldCenterCellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(6,14,6,14);
            lable = new Label(6, 14, "Gross", boldCenterCellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(7,14,7,14);
            lable = new Label(7, 14, "Taxable Gross", boldCenterCellFormat);
            sheet.addCell(lable);
            
//          CELL FONT CHANGED
            
            cellFont = new WritableFont(WritableFont.TIMES, 12);
            cellFont.setBoldStyle(WritableFont.NO_BOLD);
            cellFont.setColour(Colour.BLACK);
            
            newCellFormat = new WritableCellFormat(cellFont);
            newCellFormat.setBackground(Colour.PALE_BLUE);
            newCellFormat.setAlignment(Alignment.LEFT);
            newCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
            
             noBoldCenterCellFormat = new WritableCellFormat(newCellFormat);      
            noBoldCenterCellFormat.setBorder(Border.LEFT,BorderLineStyle.THICK);
            noBoldCenterCellFormat.setBorder(Border.RIGHT,BorderLineStyle.THICK);
            noBoldCenterCellFormat.setAlignment(Alignment.CENTRE);
            
            for(int i=1;i<=6;i++) {
                lable = new Label(0, 14+i, String.valueOf(i), noBoldCenterCellFormat);
                sheet.addCell(lable);
            }
            
            cellFormat = new WritableCellFormat(newCellFormat);   
            cellFormat.setBorder(Border.RIGHT,BorderLineStyle.THICK);
            cellFormat.setAlignment(Alignment.LEFT);

//          row number for salary break components
             rowSal = 15;
            

            sheet.mergeCells(1,rowSal,3,rowSal);
            lable = new Label(1, rowSal, "Basic", cellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(1,rowSal+1,3,rowSal+1);
            lable = new Label(1, rowSal+1, "HRA", cellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(1,rowSal+2,3,rowSal+2);
            lable = new Label(1, rowSal+2, "Conveyance Allowance", cellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(1,rowSal+3,3,rowSal+3);
            lable = new Label(1,rowSal+3, "Medical Allowance", cellFormat);
            sheet.addCell(lable);

            sheet.mergeCells(1,rowSal+4,3,rowSal+4);
            lable = new Label(1, rowSal+4, "Special Allowance", cellFormat);    
            sheet.addCell(lable);

            sheet.mergeCells(1,rowSal+5,3,rowSal+5);
            lable = new Label(1, rowSal+5, "Employer PF", cellFormat);
            sheet.addCell(lable);
            
//          storing number of different components of salary structure in noOfSalComp
             noOfSalComp = 6;  //basic, hra, conveyance allowance, medical allowance, special allowance, employer pf
                        
             def salary = Salary.findByEmployeeAndYear(emp,Integer.valueOf(yearQueried))

             salStruct = new double[noOfSalComp];
//          make a for loop to enter the data instead of entering the way done below
            salStruct[0] = salary.basic;
            salStruct[1] = salary.hra;
            salStruct[2] = salary.conveyance;
            salStruct[3] = salary.medical;
            salStruct[4] = salary.special;
            salStruct[5] = salary.employerPF;
            
             grossSalYTD=0;grossTaxSalYTD=0;
             grossSalTotal = 0; grossTaxSalTotal = 0;
            
//          FOR YEAR TO DATE
             noBoldRightCellFormat = new WritableCellFormat(cellFormat);
            noBoldRightCellFormat.setAlignment(Alignment.RIGHT);
            
             basicGross = multiFactor*salStruct[0]; basicTaxGross = multiFactor*salStruct[0];
            grossSalYTD+=basicGross;grossTaxSalYTD+=basicTaxGross;
            sheet.mergeCells(4,rowSal,4,rowSal);
            lable = new Label(4, rowSal, String.valueOf(basicGross), noBoldRightCellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(5,rowSal,5,rowSal);
            lable = new Label(5, rowSal, String.valueOf(basicTaxGross), noBoldRightCellFormat);
            sheet.addCell(lable);
                        
             hraGross = multiFactor*salStruct[1]; hraTaxGross = multiFactor*salStruct[1];
            grossSalYTD+=hraGross;grossTaxSalYTD+=hraTaxGross;
            sheet.mergeCells(4,1+rowSal,4,rowSal+1);
            lable = new Label(4, rowSal+1, String.valueOf(hraGross), noBoldRightCellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(5,rowSal+1,5,1+rowSal);
            lable = new Label(5, 1+rowSal, String.valueOf(hraTaxGross), noBoldRightCellFormat);
            sheet.addCell(lable);
            
             conveyanceGross = multiFactor*salStruct[2]; conveyanceTaxGross = multiFactor*salStruct[2];
            grossSalYTD+=conveyanceGross;grossTaxSalYTD+=conveyanceTaxGross;
            sheet.mergeCells(4,rowSal+2,4,rowSal+2);
            lable = new Label(4, rowSal+2, String.valueOf(conveyanceGross), noBoldRightCellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(5,rowSal+2,5,rowSal+2);
            lable = new Label(5, rowSal+2, String.valueOf(conveyanceTaxGross), noBoldRightCellFormat);
            sheet.addCell(lable);
            
             medicalGross = multiFactor*salStruct[3]; medicalTaxGross = multiFactor*salStruct[3];
            grossSalYTD+=medicalGross;grossTaxSalYTD+=medicalTaxGross;
            sheet.mergeCells(4,rowSal+3,4,rowSal+3);
            lable = new Label(4, rowSal+3, String.valueOf(medicalGross), noBoldRightCellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(5,rowSal+3,5,rowSal+3);
            lable = new Label(5, rowSal+3, String.valueOf(medicalTaxGross), noBoldRightCellFormat);
            sheet.addCell(lable);
            
             specialGross = multiFactor*salStruct[4]; specialTaxGross = multiFactor*salStruct[4];
            grossSalYTD+=specialGross;grossTaxSalYTD+=specialTaxGross;
            sheet.mergeCells(4,rowSal+4,4,rowSal+4);
            lable = new Label(4, rowSal+4, String.valueOf(specialGross), noBoldRightCellFormat);
            sheet.addCell(lable);
                        
            sheet.mergeCells(5,rowSal+4,5,rowSal+4);
            lable = new Label(5, rowSal+4, String.valueOf(specialTaxGross), noBoldRightCellFormat);
            sheet.addCell(lable);
            
             employerPFGross = multiFactor*salStruct[5]; employerPFTaxGross = 0;
            grossSalYTD+=employerPFGross;grossTaxSalYTD+=employerPFTaxGross;
            sheet.mergeCells(4,rowSal+5,4,rowSal+5);
            lable = new Label(4, rowSal+5, String.valueOf(employerPFGross), noBoldRightCellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(5,rowSal+5,5,rowSal+5);
            lable = new Label(5, rowSal+5, String.valueOf(employerPFTaxGross), noBoldRightCellFormat);
            sheet.addCell(lable);
            
//          TOTAL           
                    
            
            lable = new Label(6, rowSal, String.valueOf(basicGross), noBoldRightCellFormat);
            sheet.addCell(lable);
            
            lable = new Label(7, rowSal, String.valueOf(basicTaxGross), noBoldRightCellFormat);
            sheet.addCell(lable);
            
            lable = new Label(6, rowSal+1, String.valueOf(hraGross), noBoldRightCellFormat);
            sheet.addCell(lable);
           
            lable = new Label(7, rowSal+1, String.valueOf(hraTaxGross), noBoldRightCellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(6,rowSal+2,6,rowSal+2);
            lable = new Label(6,rowSal+2, String.valueOf(conveyanceGross), noBoldRightCellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(7,rowSal+2,7,rowSal+2);
            lable = new Label(7, rowSal+2, String.valueOf(conveyanceTaxGross), noBoldRightCellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(6,rowSal+3,6,rowSal+3);
            lable = new Label(6, rowSal+3, String.valueOf(medicalGross), noBoldRightCellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(7,rowSal+3,7,rowSal+3);
            lable = new Label(7, rowSal+3, String.valueOf(medicalTaxGross), noBoldRightCellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(6,rowSal+4,6,rowSal+4);
            lable = new Label(6, rowSal+4, String.valueOf(specialGross), noBoldRightCellFormat);
            sheet.addCell(lable);
                        
            sheet.mergeCells(7,rowSal+4,7,rowSal+4);
            lable = new Label(7, rowSal+4, String.valueOf(specialTaxGross), noBoldRightCellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(6,rowSal+5,6,rowSal+5);
            lable = new Label(6, rowSal+5, String.valueOf(employerPFGross), noBoldRightCellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(7,rowSal+5,7,rowSal+5);
            lable = new Label(7, rowSal+5, String.valueOf(employerPFTaxGross), noBoldRightCellFormat);
            sheet.addCell(lable);
            
//          CELL FONT CHANGED
            
            cellFont = new WritableFont(WritableFont.TIMES, 12);
            cellFont.setBoldStyle(WritableFont.BOLD);
            cellFont.setColour(Colour.BLACK);
            
            newCellFormat = new WritableCellFormat(cellFont);
            newCellFormat.setBackground(Colour.PALE_BLUE);
            newCellFormat.setAlignment(Alignment.RIGHT);
            newCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
            
            cellFormat = new WritableCellFormat(newCellFormat);         
            cellFormat.setBorder(Border.ALL,BorderLineStyle.THICK);
            
             boldRightCellFormat = new WritableCellFormat(cellFormat);
            
            sheet.mergeCells(0, rowSal+6,0,rowSal+6);
            lable = new Label(0, rowSal+6, "", boldRightCellFormat);
            sheet.addCell(lable);
            
            cellFormat = new WritableCellFormat(newCellFormat);         
            cellFormat.setBorder(Border.ALL,BorderLineStyle.THICK);
            cellFormat.setAlignment(Alignment.LEFT);
            
             boldLeftCellFormat = new WritableCellFormat(cellFormat);
            sheet.mergeCells(1, rowSal+6,3,rowSal+6);
            lable = new Label(1, rowSal+6, "Gross Salary", boldLeftCellFormat);
            sheet.addCell(lable);
            

            grossSalTotal = grossSalYTD;
            grossTaxSalTotal = grossTaxSalYTD;      
                        
            lable = new Label(4, rowSal+6, String.valueOf(grossSalYTD), boldRightCellFormat);
            sheet.addCell(lable);
            
            lable = new Label(5, rowSal+6, String.valueOf(grossTaxSalYTD), boldRightCellFormat);
            sheet.addCell(lable);
            
            lable = new Label(6, rowSal+6, String.valueOf(grossSalTotal), boldRightCellFormat);
            sheet.addCell(lable);
            
            lable = new Label(7, rowSal+6, String.valueOf(grossTaxSalTotal), boldRightCellFormat);
            sheet.addCell(lable);
            
//          colour filling the remaining riht part
            cellFont = new WritableFont(WritableFont.TIMES, 12);
            cellFont.setBoldStyle(WritableFont.BOLD);
            cellFont.setColour(Colour.BLACK);
            
            newCellFormat = new WritableCellFormat(cellFont);
            newCellFormat.setBackground(Colour.PALE_BLUE);
            
            sheet.mergeCells(8, 13, 9, rowSal+6);
            lable = new Label(8, 13, "", newCellFormat);
            sheet.addCell(lable);
            
//          last row salary
             lastRowSal = rowSal+7;
                        
            sheet.mergeCells(0, lastRowSal,9,lastRowSal);
            lable = new Label(0, lastRowSal, "", headingCellFormat);
            sheet.addCell(lable);                       
            
//          row for bills submitted break-up
             rowBill = lastRowSal+1;
            sheet.mergeCells(0, rowBill,9,rowBill);
            lable = new Label(0, rowBill, "Bills Submitted Break-Up (in Rs.):", headingCellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(0, rowBill+1,0,rowBill+2);
            lable = new Label(0, rowBill+1, "S.No.", boldCenterCellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(1, rowBill+1,3,rowBill+2);
            lable = new Label(1, rowBill+1, "Head of Pay", boldCenterCellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(4, rowBill+1,5,rowBill+1);
            lable = new Label(4, rowBill+1, "Salary Info", boldCenterCellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(4, rowBill+2,4,rowBill+2);
            lable = new Label(4, rowBill+2, "Year to Date", boldCenterCellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(5, rowBill+2,5,rowBill+2);
            lable = new Label(5, rowBill+2, "Total", boldCenterCellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(6, rowBill+1,9,rowBill+1);
            lable = new Label(6, rowBill+1, "Bills Info", boldCenterCellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(6, rowBill+2,7,rowBill+2);
            lable = new Label(6, rowBill+2, "Submitted Amount", boldCenterCellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(8, rowBill+2,9,rowBill+2);
            lable = new Label(8, rowBill+2, "To Be Submitted", boldCenterCellFormat);
            sheet.addCell(lable);
            
//          CELL FONT CHANGED
            
            cellFont = new WritableFont(WritableFont.TIMES, 12);
            cellFont.setBoldStyle(WritableFont.NO_BOLD);
            cellFont.setColour(Colour.BLACK);
            
            newCellFormat = new WritableCellFormat(cellFont);
            newCellFormat.setBackground(Colour.PALE_BLUE);
            newCellFormat.setAlignment(Alignment.RIGHT);
            newCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
            
            cellFormat = new WritableCellFormat(newCellFormat);         
            cellFormat.setBorder(Border.ALL,BorderLineStyle.THICK);
            cellFormat.setAlignment(Alignment.CENTRE);
            
            sheet.mergeCells(0, rowBill+3,0,rowBill+3);
            lable = new Label(0, rowBill+3, "1", cellFormat);
            sheet.addCell(lable);
            
            cellFormat = new WritableCellFormat(newCellFormat);         
            cellFormat.setBorder(Border.ALL,BorderLineStyle.THICK);
            cellFormat.setAlignment(Alignment.LEFT);
            
            sheet.mergeCells(1, rowBill+3,3,rowBill+3);
            lable = new Label(1, rowBill+3, "Medical Allowance", cellFormat);
            sheet.addCell(lable);
                        
//          taking value of medical allowance YEAR TO DATE from salStruct array defined above           
            sheet.mergeCells(4, rowBill+3,4,rowBill+3);
            lable = new Label(4, rowBill+3, String.valueOf(workingMonths*salStruct[3]), noBoldRightCellFormat);
            sheet.addCell(lable);

//          Total
             medAllowanceTotal = workingMonths*salStruct[3];
            sheet.mergeCells(5, rowBill+3,5,rowBill+3);
            lable = new Label(5, rowBill+3, String.valueOf(medAllowanceTotal), noBoldRightCellFormat);
            sheet.addCell(lable);
            
//          Submitted allowance, assuming that whatever is declared is submitted also
             submittedMedAllowance = salStruct[3];
            sheet.mergeCells(6, rowBill+3,7,rowBill+3);
            lable = new Label(6, rowBill+3, String.valueOf(submittedMedAllowance), noBoldRightCellFormat);
            sheet.addCell(lable);
            
//          amount to be submitted, according to previius assumption, this value will be zero always
            sheet.mergeCells(8, rowBill+3,9,rowBill+3);
            lable = new Label(8, rowBill+3, String.valueOf(multiFactor*(salStruct[3] - submittedMedAllowance)), noBoldRightCellFormat);
            sheet.addCell(lable);
            
//          last row of bill 
             lastRowBill = rowBill+4;
            cellFormat = new WritableCellFormat(headingCellFormat);
            cellFormat.setBorder(Border.TOP,BorderLineStyle.THICK);

            sheet.mergeCells(0, lastRowBill,9,lastRowBill);
            lable = new Label(0, lastRowBill, "", cellFormat);
            sheet.addCell(lable);
            
             rowHRA = lastRowBill+1;
            
            sheet.mergeCells(0, rowHRA,9,rowHRA);
            lable = new Label(0, rowHRA, "HRA Exemption Calculation (in Rs.):", headingCellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(0, rowHRA+1,0,rowHRA+1);
            lable = new Label(0, rowHRA+1, "S.No.", boldCenterCellFormat);
            sheet.addCell(lable);
                        
            lable = new Label(1, rowHRA+1, "From Date", boldCenterCellFormat);
            sheet.addCell(lable);
            
            lable = new Label(2, rowHRA+1, "To Date", boldCenterCellFormat);
            sheet.addCell(lable);
            
            lable = new Label(3, rowHRA+1, "Salary*", boldCenterCellFormat);
            sheet.addCell(lable);
            
            lable = new Label(4, rowHRA+1, "HRA", boldCenterCellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(5,rowHRA+1,6,rowHRA+1);
            lable = new Label(5, rowHRA+1, "Rent Paid", boldCenterCellFormat);
            sheet.addCell(lable);
            
            lable = new Label(7, rowHRA+1, "Is Metro", boldCenterCellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(8,rowHRA+1,9,rowHRA+1);
            lable = new Label(8, rowHRA+1, "Exemption", boldCenterCellFormat);
            sheet.addCell(lable);
            
            lable = new Label(0, rowHRA+2, "1", noBoldCenterCellFormat);
            sheet.addCell(lable);
            
            lable = new Label(1, rowHRA+2, "01-"+months.get(startMonth)+"-"+yearQueried, noBoldCenterCellFormat);
            sheet.addCell(lable);
            
            lable = new Label(2, rowHRA+2, "31-Mar-"+String.valueOf(Integer.valueOf(yearQueried)+1), noBoldCenterCellFormat);
            sheet.addCell(lable);
            
//          annual basic pay taken from salStruct array         
            lable = new Label(3, rowHRA+2, String.valueOf(multiFactor*salStruct[0]), noBoldRightCellFormat);
            sheet.addCell(lable);
            
//          annual HRA
            lable = new Label(4, rowHRA+2, String.valueOf(multiFactor*salStruct[1]), noBoldRightCellFormat);
            sheet.addCell(lable);
            
//          rent paid, taken from Rentdetails class, assuming it to be annual
            def rentDetails = Rentdetails.findByEmployeeAndYear(emp,Integer.valueOf(yearQueried))
            rentPaid = 0
            if(rentDetails != null) 
            {
                for(int i=0;i<rentDetails.size();i++)
                {
                    rentPaid += rentDetails[i].total_rent;
                }
            }            
             
            sheet.mergeCells(5, rowHRA+2, 6, rowHRA+2);
            lable = new Label(5, rowHRA+2, String.valueOf(rentPaid), noBoldRightCellFormat);
            sheet.addCell(lable);
            
//          is metro, taken from Rentdetails Class
//              if isMetro == true, then value is yes, else no 
             isMetro = (isMetro==true)?"YES":"NO";
            lable = new Label(7, rowHRA+2, isMetro, noBoldCenterCellFormat);
            sheet.addCell(lable);
            
//          Exemption calculation
            double exemption = multiFactor*salStruct[1];
            if(isMetro.compareTo("yes") == 0)
            {
                if(exemption > (multiFactor*salStruct[0])/2)
                {
                    exemption = (multiFactor*salStruct[0])/2;
                }
            }
            else
            {
                if(exemption > (2*multiFactor*salStruct[0])/5)
                {
                    exemption = (2*multiFactor*salStruct[0])/5;
                }
            }
            if(exemption > rentPaid - (multiFactor*salStruct[0])/10)
            {
                exemption = rentPaid - (multiFactor*salStruct[0])/10;
            }
            sheet.mergeCells(8, rowHRA+2, 9, rowHRA+2);
            lable = new Label(8, rowHRA+2, String.valueOf(exemption), noBoldRightCellFormat);
            sheet.addCell(lable);
            
            lable = new Label(0, rowHRA+3, "", boldLeftCellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(1, rowHRA+3, 2, rowHRA+3);
            lable = new Label(1, rowHRA+3, "Total", boldLeftCellFormat);
            sheet.addCell(lable);
            
            cellFormat = new WritableCellFormat(newCellFormat); 
            cellFormat.setBorder(Border.ALL, BorderLineStyle.THICK);
            cellFormat.setAlignment(Alignment.RIGHT);
            
            lable = new Label(3, rowHRA+3, String.valueOf(multiFactor*salStruct[0]), boldRightCellFormat);
            sheet.addCell(lable);
            
            lable = new Label(4, rowHRA+3, String.valueOf(multiFactor*salStruct[1]), boldRightCellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(5, rowHRA+3, 6, rowHRA+3);
            lable = new Label(5, rowHRA+3, String.valueOf(rentPaid), boldRightCellFormat);
            sheet.addCell(lable);
            
            lable = new Label(7, rowHRA+3, "", boldRightCellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(8, rowHRA+3, 9, rowHRA+3);
            lable = new Label(8, rowHRA+3, String.valueOf(exemption), boldRightCellFormat);
            sheet.addCell(lable);
            
//          CELL FONT CHANGED
            
            cellFont = new WritableFont(WritableFont.TIMES, 12);
            cellFont.setBoldStyle(WritableFont.NO_BOLD);
            cellFont.setColour(Colour.BLACK);
            
            newCellFormat = new WritableCellFormat(cellFont);
            newCellFormat.setBackground(Colour.PALE_BLUE);
            newCellFormat.setAlignment(Alignment.LEFT);
            newCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
                        
            cellFormat = new WritableCellFormat(newCellFormat); 
            cellFormat.setAlignment(Alignment.LEFT);
            
            sheet.mergeCells(0, rowHRA+4, 9, rowHRA+4);
            lable = new Label(0, rowHRA+4, "* Salary referers to Basic Pay and DA, if applicable", cellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(0, rowHRA+5, 9, rowHRA+5);
            lable = new Label(0, rowHRA+5, "Note: Exemption amount is calculated as the least of the following amounts:", cellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(0, rowHRA+6, 9, rowHRA+6);
            lable = new Label(0, rowHRA+6, "      i) HRA received.", cellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(0, rowHRA+7, 9, rowHRA+7);
            lable = new Label(0, rowHRA+7, "      ii) 50% of salary where the residential house is located in a metro or 40% of salary for non-metro locations.", cellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(0, rowHRA+8, 9, rowHRA+8);
            lable = new Label(0, rowHRA+8, "      iii) Excess rent paid over 10% of the salary.", cellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(0, rowHRA+9, 9, rowHRA+9);
            lable = new Label(0, rowHRA+9, "Tax Calculation Details:", headingCellFormat);
            sheet.addCell(lable);           
            
            cellFormat = new WritableCellFormat(newCellFormat); 
            cellFormat.setAlignment(Alignment.CENTRE);
            cellFormat.setBorder(Border.LEFT, BorderLineStyle.THICK);
            cellFormat.setBorder(Border.TOP, BorderLineStyle.THICK);
            
            sheet.mergeCells(0, rowHRA+10, 5, rowHRA+10);
            lable = new Label(0, rowHRA+10, "", cellFormat);
            sheet.addCell(lable);
            
            cellFormat = new WritableCellFormat(newCellFormat); 
            cellFormat.setAlignment(Alignment.CENTRE);
            cellFormat.setBorder(Border.RIGHT, BorderLineStyle.THICK);
            cellFormat.setBorder(Border.TOP, BorderLineStyle.THICK);
            
            lable = new Label(6, rowHRA+10, "Rs.", cellFormat);
            sheet.addCell(lable);
            
            lable = new Label(7, rowHRA+10, "Rs.", cellFormat);
            sheet.addCell(lable);
            
            lable = new Label(8, rowHRA+10, "Rs.", cellFormat);
            sheet.addCell(lable);
            
            lable = new Label(9, rowHRA+10, "Rs.", cellFormat);
            sheet.addCell(lable);
            
//          CELL FONT CHANGED
            
            cellFont = new WritableFont(WritableFont.TIMES, 12);
            cellFont.setBoldStyle(WritableFont.NO_BOLD);
            cellFont.setColour(Colour.BLACK);
            
            newCellFormat = new WritableCellFormat(cellFont);
            newCellFormat.setBackground(Colour.PALE_BLUE);
            newCellFormat.setAlignment(Alignment.LEFT);
            newCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
            
             lNoBoldLeftCellFormat = new WritableCellFormat(newCellFormat); 
            lNoBoldLeftCellFormat.setAlignment(Alignment.LEFT);
            lNoBoldLeftCellFormat.setBorder(Border.LEFT, BorderLineStyle.THICK);
            
//          row for tax calculation
             rowTax = rowHRA+11;   // 39 in this case
            
            sheet.mergeCells(0, rowTax, 5, rowTax);
            lable = new Label(0, rowTax, "1. Gross Salary", lNoBoldLeftCellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(0, rowTax+1, 5, rowTax+1);
            lable = new Label(0, rowTax+1, "      (a) Salary as per provisions contained in sec.17(1)", lNoBoldLeftCellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(0, rowTax+2, 5, rowTax+2);
            lable = new Label(0, rowTax+2, "      (b) Value of perquisites u/s 17(2) (as per Form No. 12BA, wherever applicable)", lNoBoldLeftCellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(0, rowTax+3, 5, rowTax+3);
            lable = new Label(0, rowTax+3, "      (c) Profits in lieu of salary under section 17(3) (as per Form No. 12BA, wherever applicable)", lNoBoldLeftCellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(0, rowTax+4, 5, rowTax+4);
            lable = new Label(0, rowTax+4, "      Total", lNoBoldLeftCellFormat);
            sheet.addCell(lable);
            
             rNoBoldRightCellFormat = new WritableCellFormat(lNoBoldLeftCellFormat);    
            rNoBoldRightCellFormat.setAlignment(Alignment.RIGHT);
            rNoBoldRightCellFormat.setBorder(Border.RIGHT, BorderLineStyle.THICK);
            rNoBoldRightCellFormat.setBorder(Border.LEFT, BorderLineStyle.NONE);
            
            for(int i=rowTax;i<=rowTax+12;i++)
            {
                lable = new Label(6, i, "", rNoBoldRightCellFormat);
                sheet.addCell(lable);
            }
            
            lable = new Label(7, rowTax, "", rNoBoldRightCellFormat);
            sheet.addCell(lable);
            lable = new Label(8, rowTax, "", rNoBoldRightCellFormat);
            sheet.addCell(lable);
            lable = new Label(9, rowTax, "", rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
            for(int i=rowTax;i<=rowTax+3;i++)
            {
                lable = new Label(8, i, "", rNoBoldRightCellFormat);
                sheet.addCell(lable);
            }
            
            for(int i=rowTax+4;i<=rowTax+5;i++)
            {
                lable = new Label(7, i, "", rNoBoldRightCellFormat);
                sheet.addCell(lable);
            }
            
            for(int i=rowTax+5;i<=rowTax+7;i++)
            {
                lable = new Label(8, i, "", rNoBoldRightCellFormat);
                sheet.addCell(lable);
            }
            
            for(int i=rowTax;i<=rowTax+16;i++)
            {
                lable = new Label(9, i, "", rNoBoldRightCellFormat);
                sheet.addCell(lable);
            }
            
//          1(a)
            lable = new Label(7, rowTax+1, String.valueOf(grossSalTotal), rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
//          1(b) perks, not implemented till now
             perks = 0;
            lable = new Label(7, rowTax+2, String.valueOf(perks), rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
//          1(c) profits in lieu of salary under section 17(3)
             profits = 0;
            lable = new Label(7, rowTax+3, String.valueOf(profits), rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
//          total gross salary
             totGrossSalary = perks + grossSalTotal + profits;
            lable = new Label(8, rowTax+4, String.valueOf(totGrossSalary), rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(0, rowTax+5, 5, rowTax+5);
            lable = new Label(0, rowTax+5, "2. Less: Allowance to the extent exempt u/s 10", lNoBoldLeftCellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(0, rowTax+6, 5, rowTax+6);
            lable = new Label(0, rowTax+6, "      (a) House Rent Allowance", lNoBoldLeftCellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(0, rowTax+7, 5, rowTax+7);
            lable = new Label(0, rowTax+7, "      (b) Transport Allowance", lNoBoldLeftCellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(0, rowTax+8, 5, rowTax+8);
            lable = new Label(0, rowTax+8, "      Total", lNoBoldLeftCellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(0, rowTax+9, 5, rowTax+9);
            lable = new Label(0, rowTax+9, "3. Balance (1-2)", lNoBoldLeftCellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(0, rowTax+10, 5, rowTax+10);
            lable = new Label(0, rowTax+10, "4. Deductions", lNoBoldLeftCellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(0, rowTax+11, 5, rowTax+11);
            lable = new Label(0, rowTax+11, "      (a) Entertainment Allowance", lNoBoldLeftCellFormat);
            sheet.addCell(lable);

            sheet.mergeCells(0, rowTax+12, 5, rowTax+12);
            lable = new Label(0, rowTax+12, "      (b) Tax on Employment", lNoBoldLeftCellFormat);
            sheet.addCell(lable);

            sheet.mergeCells(0, rowTax+13, 5, rowTax+13);
            lable = new Label(0, rowTax+13, "            (i) Year to Date", lNoBoldLeftCellFormat);
            sheet.addCell(lable);

            sheet.mergeCells(0, rowTax+14, 5, rowTax+14);
            lable = new Label(0, rowTax+14, "            (ii) Projection", lNoBoldLeftCellFormat);
            sheet.addCell(lable);

            sheet.mergeCells(0, rowTax+15, 5, rowTax+15);
            lable = new Label(0, rowTax+15, "            (iii) Total", lNoBoldLeftCellFormat);
            sheet.addCell(lable);

            sheet.mergeCells(0, rowTax+16, 5, rowTax+16);
            lable = new Label(0, rowTax+16, "5. Aggregate of 4(a) and (b)", lNoBoldLeftCellFormat);
            sheet.addCell(lable);

            sheet.mergeCells(0, rowTax+17, 5, rowTax+17);
            lable = new Label(0, rowTax+17, "6. Income Chargeable Under the head \"Salaries\" (3-5)", lNoBoldLeftCellFormat);
            sheet.addCell(lable);

            sheet.mergeCells(0, rowTax+18, 5, rowTax+18);
            lable = new Label(0, rowTax+18, "7. Any other income reported by the employee", lNoBoldLeftCellFormat);
            sheet.addCell(lable);

            sheet.mergeCells(0, rowTax+19, 5, rowTax+19);
            lable = new Label(0, rowTax+19, "      ADD", lNoBoldLeftCellFormat);
            sheet.addCell(lable);

            sheet.mergeCells(0, rowTax+20, 5, rowTax+20);
            lable = new Label(0, rowTax+20, "            (a) Income from House Property", lNoBoldLeftCellFormat);
            sheet.addCell(lable);

            sheet.mergeCells(0, rowTax+21, 5, rowTax+21);
            lable = new Label(0, rowTax+21, "      LESS", lNoBoldLeftCellFormat);
            sheet.addCell(lable);

            sheet.mergeCells(0, rowTax+22, 5, rowTax+22);
            lable = new Label(0, rowTax+22, "            (a) Home-loan interest", lNoBoldLeftCellFormat);
            sheet.addCell(lable);

            sheet.mergeCells(0, rowTax+23, 5, rowTax+23);
            lable = new Label(0, rowTax+23, "      Total", lNoBoldLeftCellFormat);
            sheet.addCell(lable);

            sheet.mergeCells(0, rowTax+24, 5, rowTax+24);
            lable = new Label(0, rowTax+24, "8. Gross Total Income (6+7)", lNoBoldLeftCellFormat);
            sheet.addCell(lable);

            cellFormat = new WritableCellFormat(newCellFormat); 
            cellFormat.setAlignment(Alignment.LEFT);
            cellFormat.setBorder(Border.LEFT, BorderLineStyle.THICK);
            cellFormat.setBorder(Border.BOTTOM, BorderLineStyle.THICK);
            
            sheet.mergeCells(0, rowTax+50, 5, rowTax+50);
            lable = new Label(0, rowTax+50, "18. Tax Payable/Refundable (16-17)", cellFormat);
            sheet.addCell(lable);
            
//          2(a) house rent allowance
            lable = new Label(7, rowTax+6, String.valueOf(exemption), rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
//          2(b) transport allowance, inside TaxSheet Class decided by the government according to our salary
//          assuming that manager will enter it
            
            def taxSheet = TaxSheet.findByEmployeeAndYear(emp,Integer.valueOf(yearQueried))
             transportAllowance = multiFactor*taxSheet.transportAllowance;
            lable = new Label(7, rowTax+7, String.valueOf(transportAllowance), rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
             totAllowanceExemp = transportAllowance + exemption;
            lable = new Label(8, rowTax+8, String.valueOf(totAllowanceExemp), rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
//          3. Balance (1-2)
             balance =  totGrossSalary - totAllowanceExemp;
            lable = new Label(8, rowTax+9, String.valueOf(balance), rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
//          4(a) entertainment allowance assuming it to be zero as it is not present in the salary structure
             entertainmentAllowance=multiFactor*0;
            lable = new Label(7, rowTax+11, String.valueOf(entertainmentAllowance), rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
//          4.Deductions , (b) Tax on Employment YTD i.e. profession Tax for months already worked
             taxOnEmployment = workingMonths*taxSheet.professionTax;
            lable = new Label(6, rowTax+13, String.valueOf(taxOnEmployment), rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
//          4(b) projection
             taxOnEmploymentProj = 0;
            lable = new Label(6, rowTax+14, String.valueOf(taxOnEmploymentProj), rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
//          4(b)(iii) Total
             totTaxOnEmployment = taxOnEmployment + taxOnEmploymentProj;
            lable = new Label(7, rowTax+15, String.valueOf(totTaxOnEmployment), rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
//          5. aggregate of 4(a) and 4(b)
             deducAggregate = totTaxOnEmployment + entertainmentAllowance;
            lable = new Label(8, rowTax+16, String.valueOf(deducAggregate), rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
//          6. income chargeable under head "salaries" (3-5)
             incomeChargeable = balance - deducAggregate;
            lable = new Label(9, rowTax+17, String.valueOf(incomeChargeable), rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
            for(int i=rowTax+15;i<=rowTax+19;i++)
            {
                lable = new Label(6, i, "", rNoBoldRightCellFormat);
                sheet.addCell(lable);
            }
            
//          7. Any other income reported by the employee, domain class for this not made as instructed by vivek prabhu
            
//          ADD (a) income from house property
             incomeHouseProperty = 0;
            lable = new Label(6, rowTax+20, String.valueOf(incomeHouseProperty), rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
            lable = new Label(6, rowTax+21, "", rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
//          LESS (a) Home - loan interest
             homeLoanInterest = 0;
            lable = new Label(6, rowTax+22, String.valueOf(homeLoanInterest), rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
//          Total other income (7(a) - 7(b))
             totOtherIncome = incomeHouseProperty - homeLoanInterest;
            lable = new Label(9, rowTax+23, String.valueOf(totOtherIncome), rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
//          8. Gross total income (6+7)
             grossTotalIncome = totOtherIncome + incomeChargeable;
            lable = new Label(9, rowTax+24, String.valueOf(grossTotalIncome), rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
            for(int i=rowTax+23;i<=rowTax+24;i++)
            {
                lable = new Label(6, i, "", rNoBoldRightCellFormat);
                sheet.addCell(lable);
            }
            
            lable = new Label(6, rowTax+25, "Gross Amount", rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
            lable = new Label(6, rowTax+26, "", rNoBoldRightCellFormat);
            sheet.addCell(lable);

            sheet.mergeCells(0, rowTax+25, 5, rowTax+25);
            lable = new Label(0, rowTax+25, "9. Deduction under Chapter VI-A", lNoBoldLeftCellFormat);
            sheet.addCell(lable);

            sheet.mergeCells(0, rowTax+26, 5, rowTax+26);
            lable = new Label(0, rowTax+26, "            (a) Section 80C", lNoBoldLeftCellFormat);
            sheet.addCell(lable);
            

            
//          9(c) section 80CCD
            def sec80CCD = Nps80c.findByEmployeeAndYear(emp,Integer.valueOf(yearQueried))
             section80CCD = sec80CCD.tamount;
            lable = new Label(6, rowTax+31, String.valueOf(section80CCD), rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
//          qualifying
            lable = new Label(7, rowTax+29, String.valueOf(total9a), rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
            lable = new Label(7, rowTax+30, String.valueOf(section80CCC), rNoBoldRightCellFormat);
            sheet.addCell(lable);
                        
            lable = new Label(7, rowTax+31, String.valueOf(section80CCD), rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
            

            sheet.mergeCells(0, rowTax+30, 5, rowTax+30);
            lable = new Label(0, rowTax+30, "            (b) Section 80CCC", lNoBoldLeftCellFormat);
            sheet.addCell(lable);

            sheet.mergeCells(0, rowTax+31, 5, rowTax+31);
            lable = new Label(0, rowTax+31, "            (c) Section 80CCD", lNoBoldLeftCellFormat);
            sheet.addCell(lable);

///////////////////////////////////////////////////////////////////////////////
    

            int rowSec80c = rowTax+27;
            def sec80c = Investment80c.findByEmployeeAndYear(emp,Integer.valueOf(yearQueried))
            int noOfSec80cRows
            int rowEmpPF = rowSec80c+noOfSec80cRows;
            def sec80cAmount

            total9a = 0

            if(sec80c != null)
            {
                noOfSec80cRows = sec80c.size()
                for(int i= 0;i<noOfSec80cRows;i++)
                {
                    sheet.mergeCells(0, rowSec80c+i, 5, rowSec80c+i);
                    lable = new Label(0, rowSec80c+i, "                      "+sec80c.treb, lNoBoldLeftCellFormat);
                    sheet.addCell(lable);

                    sec80cAmount = sec80c.tamount
                    total9a += sec80cAmount  
                    lable = new Label(6, rowSec80c+i, String.valueOf(sec80cAmount), rNoBoldRightCellFormat);
                    sheet.addCell(lable);
                }
                rowEmpPF += i
            }

            sheet.mergeCells(0, rowEmpPF, 5, rowEmpPF);
            lable = new Label(0, rowEmpPF, "                   Employee PF Contribution", lNoBoldLeftCellFormat);
            sheet.addCell(lable);
            employeePF = multiFactor*salary.employeePF;
            lable = new Label(6, rowEmpPF, String.valueOf(employeePF), rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(0, rowEmpPF+1, 5, rowEmpPF+1);
            lable = new Label(0, rowEmpPF+1, "                   Total", lNoBoldLeftCellFormat);
            sheet.addCell(lable);
//          total9a = publicProvidentFund + employeePF;
            
//          gross amount
            lable = new Label(6, rowEmpPF+1, String.valueOf(total9a), rNoBoldRightCellFormat);
            sheet.addCell(lable);

//          qualifying
            lable = new Label(7, rowEmpPF+1, String.valueOf(total9a), rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
//          deductible
            lable = new Label(8, rowEmpPF+1, String.valueOf(total9a), rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(0, rowEmpPF+2, 5, rowEmpPF+2);
            lable = new Label(0, rowEmpPF+2, "            (b) Section 80CCC", lNoBoldLeftCellFormat);
            sheet.addCell(lable);
                        
             def sec80ccc = Penfund.findByEmployeeAndYear(emp,Integer.valueOf(yearQueried))
            int rowSec80ccc = rowEmpPF+3;
            int noOfSec80cccRows = 0;
            int rowSec80cccTotal = rowSec80ccc + noOfSec80cccRows;
            double total9b=0;
            def sec80cccAmount

            if(sec80ccc != null)
            {
                noOfSec80cccRows = sec80ccc.size()
                for(int i= 0;i<noOfSec80cccRows;i++)
                {
                    sheet.mergeCells(0, rowSec80ccc+i, 5, rowSec80ccc+i);
                    lable = new Label(0, rowSec80ccc+i, "                      "+sec80ccc.treb, lNoBoldLeftCellFormat);
                    sheet.addCell(lable);

                    sec80cccAmount = sec80ccc.tamount
                    total9b += sec80cccAmount  
                    lable = new Label(6, rowSec80ccc+i, String.valueOf(sec80cccAmount), rNoBoldRightCellFormat);
                    sheet.addCell(lable);
                }
            }

            
            sheet.mergeCells(0, rowSec80cccTotal, 5, rowSec80cccTotal);
            lable = new Label(0, rowSec80cccTotal, "                        Total", lNoBoldLeftCellFormat);
            sheet.addCell(lable);
            
//          gross amount
            lable = new Label(6, rowSec80cccTotal, String.valueOf(total9b), rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
//          qualifying
            lable = new Label(7, rowSec80cccTotal, String.valueOf(total9b), rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
//          deductible
            lable = new Label(8, rowSec80cccTotal, String.valueOf(total9b), rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
            
            
            
            sheet.mergeCells(0, rowSec80cccTotal+1, 5, rowSec80cccTotal+1);
            lable = new Label(0, rowSec80cccTotal+1, "            (c) Section 80CCD", lNoBoldLeftCellFormat);
            sheet.addCell(lable);

            int rowSec80ccd = rowSec80cccTotal+2;
            int noOfSec80ccdRows = 0;
            int rowSec80ccdTotal = rowSec80ccd + noOfSec80ccdRows;
            double total9c = 0;
            def sec80ccd= Nps80c.findByEmployeeAndYear(emp,Integer.valueOf(yearQueried))
            def sec80ccdAmount

            if(sec80ccd != null)
            {
                noOfSec80ccdRows = sec80ccd.size()
                for(int i= 0;i<noOfSec80ccdRows;i++)
                {
                    sheet.mergeCells(0, rowSec80ccd+i, 5, rowSec80ccd+i);
                    lable = new Label(0, rowSec80ccd+i, "                      "+sec80ccd.treb, lNoBoldLeftCellFormat);
                    sheet.addCell(lable);

                    sec80ccdAmount = sec80ccd.tamount
                    total9c += sec80ccdAmount  
                    lable = new Label(6, rowSec80ccd+i, String.valueOf(sec80ccdAmount), rNoBoldRightCellFormat);
                    sheet.addCell(lable);
                }
            }

            sheet.mergeCells(0, rowSec80ccdTotal, 5, rowSec80ccdTotal);
            lable = new Label(0, rowSec80ccdTotal, "                        Total", lNoBoldLeftCellFormat);
            sheet.addCell(lable);
            
//          gross amount
            lable = new Label(6, rowSec80ccdTotal, String.valueOf(total9c), rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
//          qualifying
            lable = new Label(7, rowSec80ccdTotal, String.valueOf(total9c), rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
//          deductible
            lable = new Label(8, rowSec80ccdTotal, String.valueOf(total9c), rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
            for(int column = 6;column<=9;column++)
            {
                lable = new Label(column, rowEmpPF+2, "", rNoBoldRightCellFormat);
                sheet.addCell(lable);
            }
            for(int column = 6;column<=9;column++)
            {
                lable = new Label(column, rowSec80cccTotal+1, "", rNoBoldRightCellFormat);
                sheet.addCell(lable);
            }
            lable = new Label(9, rowSec80cccTotal, "", rNoBoldRightCellFormat);
            sheet.addCell(lable);
            lable = new Label(9, rowSec80ccdTotal, "", rNoBoldRightCellFormat);
            sheet.addCell(lable);
            

















//////////////////////////////////////////////////////////////////////////////////

            sheet.mergeCells(0, rowSec80ccdTotal+1, 5, rowSec80ccdTotal+1);
            lable = new Label(0, rowSec80ccdTotal+1, "      (Note: 1. Aggregate amount deductible under Section 80C shall not exceed Rs. 1,50,000", lNoBoldLeftCellFormat);
            sheet.addCell(lable);
           
            sheet.mergeCells(0, rowSec80ccdTotal+2, 5, rowSec80ccdTotal+2);
            lable = new Label(0, rowSec80ccdTotal+2, "                2. Aggregate amount deductible under the three sections, i.e., 80C, 80CCC and ", lNoBoldLeftCellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(0, rowSec80ccdTotal+3, 5, rowSec80ccdTotal+3);
            lable = new Label(0, rowSec80ccdTotal+3, "                   80CCD shall not exceed Rs. 1,50,000)", lNoBoldLeftCellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(0, rowSec80ccdTotal+4, 5, rowSec80ccdTotal+4);
            lable = new Label(0, rowSec80ccdTotal+4, "10. Aggregate of Deductible amount under Chapter VI-A", lNoBoldLeftCellFormat);
            sheet.addCell(lable);

            sheet.mergeCells(0, rowSec80ccdTotal+5, 5, rowSec80ccdTotal+5);
            lable = new Label(0, rowSec80ccdTotal+5, "11. Total Income (8-10) (Round off to Rs. 10)", lNoBoldLeftCellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(0, rowSec80ccdTotal+6, 5, rowSec80ccdTotal+6);
            lable = new Label(0, rowSec80ccdTotal+6, "12. Tax on Total Income", lNoBoldLeftCellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(0, rowSec80ccdTotal+11, 5, rowSec80ccdTotal+11);
            lable = new Label(0, rowSec80ccdTotal+11, "      (d) Total", lNoBoldLeftCellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(0, rowSec80ccdTotal+12, 5, rowSec80ccdTotal+12);
            lable = new Label(0, rowSec80ccdTotal+12, "13. Education Cess @ 3%(on tax computed at S.No.12)", lNoBoldLeftCellFormat);
            sheet.addCell(lable);

            sheet.mergeCells(0, rowSec80ccdTotal+13, 5, rowSec80ccdTotal+13);
            lable = new Label(0, rowSec80ccdTotal+13, "14. Tax Payable (12+13)", lNoBoldLeftCellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(0, rowSec80ccdTotal+14, 5, rowSec80ccdTotal+14);
            lable = new Label(0, rowSec80ccdTotal+14, "15. Less: Relief under Section 89 (attach details)", lNoBoldLeftCellFormat);
            sheet.addCell(lable);

            sheet.mergeCells(0, rowSec80ccdTotal+15, 5, rowSec80ccdTotal+15);
            lable = new Label(0, rowSec80ccdTotal+15, "16. Tax payable (14-15)", lNoBoldLeftCellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(0, rowSec80ccdTotal+16, 5, rowSec80ccdTotal+16);
            lable = new Label(0, rowSec80ccdTotal+16, "17. Less", lNoBoldLeftCellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(0, rowSec80ccdTotal+17, 5, rowSec80ccdTotal+17);
            lable = new Label(0, rowSec80ccdTotal+17, "      (a) Tax Deducted at source u/s 192(1)", lNoBoldLeftCellFormat);
            sheet.addCell(lable);
                        
            sheet.mergeCells(0, rowSec80ccdTotal+18, 5, rowSec80ccdTotal+18);
            lable = new Label(0, rowSec80ccdTotal+18, "      (b) Tax Paid by Employer on behalf of Employee u/s 192(1A) on perquisites u/s 17(2)", lNoBoldLeftCellFormat);
            sheet.addCell(lable);
            
            cellFormat = new WritableCellFormat(newCellFormat); 
            cellFormat.setAlignment(Alignment.LEFT);
            cellFormat.setBorder(Border.LEFT, BorderLineStyle.THICK);
            cellFormat.setBorder(Border.BOTTOM, BorderLineStyle.THICK);
            
            sheet.mergeCells(0, rowSec80ccdTotal+19, 5, rowSec80ccdTotal+19);
            lable = new Label(0, rowSec80ccdTotal+19, "18. Tax Payable/Refundable (16-17)", cellFormat);
            sheet.addCell(lable);
            
//          2(a) house rent allowance
            lable = new Label(7, rowTax+6, String.valueOf(exemption), rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
//          2(b) transport allowance, decided by the government according to our salary
//          assuming that manager will enter it
            
             taxSheet = TaxSheet.findByEmployeeAndYear(emp,Integer.valueOf(yearQueried))

             transportAllowance = multiFactor*taxSheet.transportAllowance             //1600;
            lable = new Label(7, rowTax+7, String.valueOf(transportAllowance), rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
             totAllowanceExemp = transportAllowance + exemption;
            lable = new Label(8, rowTax+8, String.valueOf(totAllowanceExemp), rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
//          3. Balance (1-2)
             balance =  totGrossSalary - totAllowanceExemp;
            lable = new Label(8, rowTax+9, String.valueOf(balance), rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
//          4(a) entertainment allowance
             entertainmentAllowance=multiFactor*0;
            lable = new Label(7, rowTax+11, String.valueOf(entertainmentAllowance), rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
//          4.Deductions , (b) Tax on Employment YTD i.e. profession Tax for months already worked
             taxOnEmployment = workingMonths*taxSheet.professionTax               //200;
            lable = new Label(6, rowTax+13, String.valueOf(taxOnEmployment), rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
//          4(b) projection
             taxOnEmploymentProj = 0;
            lable = new Label(6, rowTax+14, String.valueOf(taxOnEmploymentProj), rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
//          4(b)(iii) Total
             totTaxOnEmployment = taxOnEmployment + taxOnEmploymentProj;
            lable = new Label(7, rowTax+15, String.valueOf(totTaxOnEmployment), rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
//          5. aggregate of 4(a) and 4(b)
             deducAggregate = totTaxOnEmployment + entertainmentAllowance;
            lable = new Label(8, rowTax+16, String.valueOf(deducAggregate), rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
//          6. income chargeable under head "salaries" (3-5)
             incomeChargeable = balance - deducAggregate;
            lable = new Label(9, rowTax+17, String.valueOf(incomeChargeable), rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
            for(int i=rowTax+15;i<=rowTax+19;i++)
            {
                lable = new Label(6, i, "", rNoBoldRightCellFormat);
                sheet.addCell(lable);
            }
            
//          7. Any other income reported by the employee, domain class for this not made as instructed by vivek prabhu
            
//          ADD (a) income from house property
             incomeHouseProperty = 0;
            lable = new Label(6, rowTax+20, String.valueOf(incomeHouseProperty), rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
            lable = new Label(6, rowTax+21, "", rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
//          LESS (a) Home - loan interest
             homeLoanInterest = 0;
            lable = new Label(6, rowTax+22, String.valueOf(homeLoanInterest), rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
//          Total other income (7(a) - 7(b))
             totOtherIncome = incomeHouseProperty - homeLoanInterest;
            lable = new Label(9, rowTax+23, String.valueOf(totOtherIncome), rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
//          8. Gross total income (6+7)
             grossTotalIncome = totOtherIncome + incomeChargeable;
            lable = new Label(9, rowTax+24, String.valueOf(grossTotalIncome), rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
            for(int i=rowTax+23;i<=rowTax+24;i++)
            {
                lable = new Label(6, i, "", rNoBoldRightCellFormat);
                sheet.addCell(lable);
            }
            
            lable = new Label(6, rowTax+25, "Gross Amount", rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
            lable = new Label(6, rowTax+26, "", rNoBoldRightCellFormat);
            sheet.addCell(lable);
            System.out.println(rowSec80ccdTotal);
            System.out.println(rowTax+29);
//          
            aggregateDedAmount = total9a + total9b + total9c;
            if(aggregateDedAmount > 150000) aggregateDedAmount = 150000;
            lable = new Label(9, rowSec80ccdTotal+4, String.valueOf(aggregateDedAmount), rNoBoldRightCellFormat);
            sheet.addCell(lable);
            

//          differnt ranges of income tax slabs and corresponding percentage
            lRange1=taxSheet.lRange1;   uRange1=taxSheet.uRange1;
            lRange2=taxSheet.lRange2;   uRange2=taxSheet.uRange2;
            lRange3=taxSheet.lRange3;   uRange3=taxSheet.uRange3;
            lRange4=taxSheet.lRange4;

            percent1=taxSheet.percent1;     percent2=taxSheet.percent2; 
            percent3=taxSheet.percent3;     percent4=taxSheet.percent4;         
            
//          11. Total Income (8-10) (Round off to Rs. 10)
             totalIncome = (double)((int)(grossTotalIncome - aggregateDedAmount));
             round = totalIncome%10;
            if(round>=5)
            {
                totalIncome += (10-round);
            }
            lable = new Label(9, rowSec80ccdTotal+5, String.valueOf(totalIncome), rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
//          tax calculation on total income 
             range1=0;range2=0;range3=0;range4=0;remain=0;
            if(totalIncome > uRange1)
            {
                range1 = uRange1;
                remain = totalIncome - uRange1;
                if(remain > uRange2-lRange2+1)
                {
                    range2 = uRange2-lRange2+1;
                    remain -= uRange2-lRange2+1;
                    if(remain > uRange3-lRange3+1)
                    {
                        range3 = uRange3-lRange3+1;
                        remain -= uRange3-lRange3+1;
                        range4 = remain;
                    }
                    else
                    {
                        range3 = remain;
                    }
                }
                else
                {
                    range2 = remain;
                }
            }
            else
            {
                range1 = totalIncome;
            }
             taxRange1=0;
            taxRange2 = range2*percent2/100;
            taxRange3 = range3*percent3/100;
            taxRange4 = (percent4*range4)/100;
                        
            sheet.mergeCells(0, rowSec80ccdTotal+7, 5, rowSec80ccdTotal+7);
            lable = new Label(0, rowSec80ccdTotal+7, "      (a) Rs. "+String.valueOf(lRange1)+" - Rs. "+String.valueOf(uRange1)+" (Rs. "+String.valueOf(range1)+" at "+String.valueOf(percent1)+"%)", lNoBoldLeftCellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(0, rowSec80ccdTotal+8, 5, rowSec80ccdTotal+8);
            lable = new Label(0, rowSec80ccdTotal+8, "      (b) Rs. "+String.valueOf(lRange2)+" - Rs. "+String.valueOf(uRange2)+" (Rs. "+String.valueOf(range2)+" at "+String.valueOf(percent2)+"%)", lNoBoldLeftCellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(0, rowSec80ccdTotal+9, 5, rowSec80ccdTotal+9);
            lable = new Label(0, rowSec80ccdTotal+9, "      (c) Rs. "+String.valueOf(lRange3)+" - Rs. "+String.valueOf(uRange3)+" (Rs. "+String.valueOf(range3)+" at "+String.valueOf(percent3)+"%)", lNoBoldLeftCellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(0, rowSec80ccdTotal+10, 5, rowSec80ccdTotal+10);
            lable = new Label(0, rowSec80ccdTotal+10, "      (d) Rs. "+String.valueOf(lRange4)+" and above (Rs. "+String.valueOf(range4)+" at "+String.valueOf(percent4)+"%)", lNoBoldLeftCellFormat);
            sheet.addCell(lable);
            

            lable = new Label(8, rowSec80ccdTotal+7, String.valueOf(taxRange1), rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
            lable = new Label(8, rowSec80ccdTotal+8, String.valueOf(taxRange2), rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
            lable = new Label(8, rowSec80ccdTotal+9, String.valueOf(taxRange3), rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
            lable = new Label(8, rowSec80ccdTotal+10, String.valueOf(taxRange4), rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
//          total tax on income
             totalTax = taxRange1+taxRange2+taxRange3+taxRange4;
            lable = new Label(9, rowSec80ccdTotal+11, String.valueOf(totalTax), rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
//          13. education cess 
             eduCess = (double)((int)(3*totalTax)/100);         
            lable = new Label(9, rowSec80ccdTotal+12, String.valueOf(eduCess), rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
//          14. Tax Payable (12+13)
             taxPayable = eduCess + totalTax;
            lable = new Label(9, rowSec80ccdTotal+13, String.valueOf(taxPayable), rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
//          15. Less: Relief under Section 89 (attach details)
             reliefSec89 = 0;
            
            lable = new Label(9, rowSec80ccdTotal+14, String.valueOf(reliefSec89), rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
//          16. Tax payable (14-15)
            taxPayable -= reliefSec89;
            lable = new Label(9, rowSec80ccdTotal+15, String.valueOf(taxPayable), rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
             taxSheet = TaxSheet.findByEmployeeAndYear(emp,Integer.valueOf(yearQueried))
            taxSheet.taxPayable = taxPayable
            taxSheet.save()

//          17. (a) Tax Deducted at source u/s 192(1)
            lable = new Label(9, rowSec80ccdTotal+17, String.valueOf(taxPayable), rNoBoldRightCellFormat);
            sheet.addCell(lable);        
            
            for(int i=rowSec80ccdTotal+1;i<=rowSec80ccdTotal+18;i++)
            {
                lable = new Label(6, i, "", rNoBoldRightCellFormat);
                sheet.addCell(lable);
            }
            
            cellFormat = new WritableCellFormat(newCellFormat); 
            cellFormat.setAlignment(Alignment.RIGHT);
            cellFormat.setBorder(Border.RIGHT, BorderLineStyle.THICK);
            cellFormat.setBorder(Border.BOTTOM, BorderLineStyle.THICK);
            
            lable = new Label(6, rowSec80ccdTotal+19, "", cellFormat);
            sheet.addCell(lable);           
            
//          next column         
            for(int i=rowTax+8;i<=rowTax+10;i++)
            {
                lable = new Label(7, i, "", rNoBoldRightCellFormat);
                sheet.addCell(lable);
            }        
            
            for(int i=rowTax+12;i<=rowTax+14;i++)
            {
                lable = new Label(7, i, "", rNoBoldRightCellFormat);
                sheet.addCell(lable);
            }
            
            for(int i=rowTax+16;i<=rowTax+24;i++)
            {
                lable = new Label(7, i, "", rNoBoldRightCellFormat);
                sheet.addCell(lable);
            }
            
            lable = new Label(7, rowTax+25, "Qualifying", rNoBoldRightCellFormat);
            sheet.addCell(lable);
            lable = new Label(7, rowTax+26, "", rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
            for(int i=rowSec80c;i<=rowEmpPF;i++)
            {
                lable = new Label(7, i, "", rNoBoldRightCellFormat);
                sheet.addCell(lable);
            }        
            
            for(int i=rowSec80ccdTotal+1;i<=rowSec80ccdTotal+18;i++)
            {
                lable = new Label(7, i, "", rNoBoldRightCellFormat);
                sheet.addCell(lable);
            }
            
            cellFormat = new WritableCellFormat(newCellFormat); 
            cellFormat.setAlignment(Alignment.RIGHT);
            cellFormat.setBorder(Border.RIGHT, BorderLineStyle.THICK);
            cellFormat.setBorder(Border.BOTTOM, BorderLineStyle.THICK);
            
            lable = new Label(7, rowSec80ccdTotal+19, "", cellFormat);
            sheet.addCell(lable);
            
//          3rd Rs. column
            
//          lable = new Label(8, rowTax+8, "0", rNoBoldRightCellFormat);
//          sheet.addCell(lable);        
            
            for(int i=rowTax+10;i<=rowTax+15;i++)
            {
                lable = new Label(8, i, "", rNoBoldRightCellFormat);
                sheet.addCell(lable);
            }
            
            for(int i=rowTax+17;i<=rowTax+24;i++)
            {
                lable = new Label(8, i, "", rNoBoldRightCellFormat);
                sheet.addCell(lable);
            }
            
            lable = new Label(8, rowTax+25, "Deductible", rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
            lable = new Label(8, rowTax+26, "", rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
            
            for(int i=rowSec80c;i<=rowEmpPF;i++)
            {
                lable = new Label(8, i, "", rNoBoldRightCellFormat);
                sheet.addCell(lable);
            }
            
            
            for(int i=rowSec80ccdTotal+1;i<=rowSec80ccdTotal+6;i++)
            {
                lable = new Label(8, i, "", rNoBoldRightCellFormat);
                sheet.addCell(lable);
            }
                        
            for(int i=rowSec80ccdTotal+11;i<=rowSec80ccdTotal+18;i++)
            {
                lable = new Label(8, i, "", rNoBoldRightCellFormat);
                sheet.addCell(lable);
            }
            
            cellFormat = new WritableCellFormat(newCellFormat); 
            cellFormat.setAlignment(Alignment.RIGHT);
            cellFormat.setBorder(Border.RIGHT, BorderLineStyle.THICK);
            cellFormat.setBorder(Border.BOTTOM, BorderLineStyle.THICK);
            
            lable = new Label(8, rowSec80ccdTotal+19, "", cellFormat);
            sheet.addCell(lable);
            
//          4th Rs. column
            for(int i=rowTax+18;i<=rowTax+22;i++)
            {
                lable = new Label(9, i, "", rNoBoldRightCellFormat);
                sheet.addCell(lable);
            }
                                    
            for(int i=rowTax+25;i<=rowTax+26;i++)
            {
                lable = new Label(9, i, "", rNoBoldRightCellFormat);
                sheet.addCell(lable);
            }
            for(int i=rowSec80c;i<=rowEmpPF;i++)
            {
                lable = new Label(9, i, "", rNoBoldRightCellFormat);
                sheet.addCell(lable);
            }
            for(int i=rowTax+25;i<=rowSec80ccdTotal+3;i++)
            {
                lable = new Label(9, i, "", rNoBoldRightCellFormat);
                sheet.addCell(lable);
            }
            
            for(int i=rowSec80ccdTotal+6;i<=rowSec80ccdTotal+10;i++)
            {
                lable = new Label(9, i, "", rNoBoldRightCellFormat);
                sheet.addCell(lable);
            }
           
            lable = new Label(9, rowSec80ccdTotal+6, "", rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
            lable = new Label(9, rowSec80ccdTotal+18, "", rNoBoldRightCellFormat);
            sheet.addCell(lable);
                        
            lable = new Label(9, rowSec80ccdTotal+16, "", rNoBoldRightCellFormat);
            sheet.addCell(lable);
            
            cellFormat = new WritableCellFormat(newCellFormat); 
            cellFormat.setAlignment(Alignment.RIGHT);
            cellFormat.setBorder(Border.RIGHT, BorderLineStyle.THICK);
            cellFormat.setBorder(Border.BOTTOM, BorderLineStyle.THICK);
            
            lable = new Label(9, rowSec80ccdTotal+19, "0", cellFormat);
            sheet.addCell(lable);

            workbook.write();
            workbook.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        } catch (WriteException e) {
            e.printStackTrace();
        }
    }

*/


    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond TaxSheet.list(params), model:[taxSheetInstanceCount: TaxSheet.count()]
    }

    def show(Taxsheet taxSheetInstance) {
        respond taxSheetInstance
    }

    def create() {
        respond new Taxsheet(params)
    }

    @Transactional
    def save(Taxsheet taxSheetInstance) {
        if (taxSheetInstance == null) {
            notFound()
            return
        }

        if (taxSheetInstance.hasErrors()) {
            respond taxSheetInstance.errors, view:'create'
            return
        }

        taxSheetInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'taxSheet.label', default: 'TaxSheet'), taxSheetInstance.id])
                redirect taxSheetInstance
            }
            '*' { respond taxSheetInstance, [status: CREATED] }
        }
    }

    def edit(Taxsheet taxSheetInstance) {
        respond taxSheetInstance
    }

    @Transactional
    def update(Taxsheet taxSheetInstance) {
        if (taxSheetInstance == null) {
            notFound()
            return
        }

        if (taxSheetInstance.hasErrors()) {
            respond taxSheetInstance.errors, view:'edit'
            return
        }

        taxSheetInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'TaxSheet.label', default: 'TaxSheet'), taxSheetInstance.id])
                redirect taxSheetInstance
            }
            '*'{ respond taxSheetInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Taxsheet taxSheetInstance) {

        if (taxSheetInstance == null) {
            notFound()
            return
        }

        taxSheetInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'TaxSheet.label', default: 'TaxSheet'), taxSheetInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'taxSheet.label', default: 'TaxSheet'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
