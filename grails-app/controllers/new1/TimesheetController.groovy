package new1



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import java.text.DateFormat;
    import java.text.SimpleDateFormat;
    import java.util.Date;

@Transactional(readOnly = true)
class TimesheetController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Timesheet.list(params), model:[timesheetInstanceCount: Timesheet.count()]
    }

    def show(Timesheet timesheetInstance) {
        respond timesheetInstance
    }

    def create() {
        respond new Timesheet(params)
    }

    @Transactional
    def save() {
        System.out.println(params.fromdate+params.todate+params.task+params.description+params.workhour);
       if(params.fromdate.isEmpty() || params.todate.isEmpty() || params.task.isEmpty() || params.description.isEmpty() || params.workhour.isEmpty()){
            if(params.fromdate.isEmpty()){
                System.out.println("ent1");    
                flash.message="from"
            }
            else{
                if(params.todate.isEmpty())
                    flash.message="to"
                else if(params.task.isEmpty())
                    flash.message="task"
                else if(params.description.isEmpty())
                    flash.message="des"
                else
                    flash.message="work"
            }
            System.out.println("ent");
            redirect(controller:"Employee" ,action:"time")
            flash.message=''
            return
        }
        else{
            System.out.println("hari___BOL")
            def newTs = new Timesheet();
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            Date startDate, endDate;
            startDate = sdf.parse(params.fromdate);
            endDate = sdf.parse(params.todate);
            newTs.fromdate =  startDate 
            newTs.todate = endDate;
            newTs.project= params.project;
            newTs.task = params.task
            newTs.description = params.description;
            int xyz=Integer.parseInt(params.workhour)
            newTs.work_hour = xyz;
            newTs.employee = Employee.findByUsername(params.username);
            if(newTs.save(flush:true)){
                flash.message="submitted"
                redirect(controller:"Employee", action:"time");
            }
            else{
                render('not saved')
                  
            }

        }
    }

    def edit(Timesheet timesheetInstance) {
        respond timesheetInstance
    }

    @Transactional
    def update(Timesheet timesheetInstance) {
        if (timesheetInstance == null) {
            notFound()
            return
        }

        if (timesheetInstance.hasErrors()) {
            respond timesheetInstance.errors, view:'edit'
            return
        }

        timesheetInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Timesheet.label', default: 'Timesheet'), timesheetInstance.id])
                redirect timesheetInstance
            }
            '*'{ respond timesheetInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Timesheet timesheetInstance) {

        if (timesheetInstance == null) {
            notFound()
            return
        }

        timesheetInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Timesheet.label', default: 'Timesheet'), timesheetInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'timesheet.label', default: 'Timesheet'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
