package new1



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class PenfundController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Penfund.list(params), model:[penfundInstanceCount: Penfund.count()]
    }

    def show(Penfund penfundInstance) {
        respond penfundInstance
    }

    def create() {
        respond new Penfund(params)
    }

    @Transactional
     def save() {
        System.out.println("")
       if(params == null){
            redirect(controller:"Employee", action:"dec");
            flash.message=''
            return
        }
        def mall = Penfund.findByTreb(params.trebip)
        if(mall){
            flash.message="alredy given";
            redirect(controller:"Employee", action:"dec");
        }
        else{
            def newM = new Penfund();
            
            
            int from=params.tamountip.toInteger()
            
            String to=params.trebip.toString()
            newM.treb= to
            newM.tamount=from
            System.out.println( )

            
            
            newM.employee = Employee.findByUsername(params.username)
            System.out.println(params.username)
           if(newM.save(flush:true)){
                flash.message =='SUCCESS! Penfund submitted '
                println 'harihari'
               redirect(controller:"Employee", action:"dec");
            }
            else{
                 flash.message =='FAILED! Penfund not submitted'
                 redirect(controller:"Employee", action:"dec");
            }
        }
    }


    def edit(Penfund penfundInstance) {
        respond penfundInstance
    }

    @Transactional
    def update(Penfund penfundInstance) {
        if (penfundInstance == null) {
            notFound()
            return
        }

        if (penfundInstance.hasErrors()) {
            respond penfundInstance.errors, view:'edit'
            return
        }

        penfundInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Penfund.label', default: 'Penfund'), penfundInstance.id])
                redirect penfundInstance
            }
            '*'{ respond penfundInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Penfund penfundInstance) {

        if (penfundInstance == null) {
            notFound()
            return
        }

        penfundInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Penfund.label', default: 'Penfund'), penfundInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'penfund.label', default: 'Penfund'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
