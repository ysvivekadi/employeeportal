package new1



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class MtreatmentController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Mtreatment.list(params), model:[mtreatmentInstanceCount: Mtreatment.count()]
    }

    def show(Mtreatment mtreatmentInstance) {
        respond mtreatmentInstance
    }

    def create() {
        respond new Mtreatment(params)
    }

    @Transactional
    def save(){
        if(params == null){
            redirect(controller:"Employee", action:"dec");
            flash.message=''
            return
        }
        def mall = Mtreatment.findByTexpend(params.texpend)
        if(mall){
            flash.message="alredy given";
            redirect(controller:"Employee", action:"dec");
        }
        else{
            def newM = new Mtreatment();
            System.out.println();
            int b = params.texpend.toInteger();
            newM.texpend= b;
           println params.res1 as boolean
             newM.res1=params.res1 as boolean
               println params.res2 as boolean
             newM.res2=params.res2 as boolean
               println params.rse3 as boolean
             newM.res3=params.res3 as boolean
            System.out.println("b="+b);
            newM.employee = Employee.findByUsername(params.username)
            
            if(newM.save(flush:true)){
                flash.message =='SUCCESS! Mtreatment submitted '
                println 'harihari'
               redirect(controller:"Employee", action:"dec");
            }
            else{
                 flash.message =='FAILED! Mtreatment not submitted'
                 redirect(controller:"Employee", action:"dec");
            }
        }
    }


    def edit(Mtreatment mtreatmentInstance) {
        respond mtreatmentInstance
    }

    @Transactional
    def update(Mtreatment mtreatmentInstance) {
        if (mtreatmentInstance == null) {
            notFound()
            return
        }

        if (mtreatmentInstance.hasErrors()) {
            respond mtreatmentInstance.errors, view:'edit'
            return
        }

        mtreatmentInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Mtreatment.label', default: 'Mtreatment'), mtreatmentInstance.id])
                redirect mtreatmentInstance
            }
            '*'{ respond mtreatmentInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Mtreatment mtreatmentInstance) {

        if (mtreatmentInstance == null) {
            notFound()
            return
        }

        mtreatmentInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Mtreatment.label', default: 'Mtreatment'), mtreatmentInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'mtreatment.label', default: 'Mtreatment'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
