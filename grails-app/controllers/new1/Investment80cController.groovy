package new1



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class Investment80cController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Investment80c.list(params), model:[investment80cInstanceCount: Investment80c.count()]
    }

    def show(Investment80c investment80cInstance) {
        respond investment80cInstance
    }

    def create() {
        respond new Investment80c(params)
    }

    @Transactional
    def save() {
        System.out.println("")
       if(params == null){
            redirect(controller:"Employee", action:"dec");
            flash.message=''
            return
        }
        def mall = Investment80c.findByTreb(params.trebii)
        if(mall){
            flash.message="alredy given";
            redirect(controller:"Employee", action:"dec");
        }
        else{
            def newM = new Investment80c();
            
            
            int from=params.tamountii.toInteger()
            
            String to=params.trebii.toString()
            newM.treb= to
            newM.tamount=from
            System.out.println( )

            
            
            newM.employee = Employee.findByUsername(params.username)
            System.out.println(params.username)
            if(newM.save(flush:true)){
                flash.message =='SUCCESS! Investment80c submitted '
                println 'harihari'
               redirect(controller:"Employee", action:"dec");
            }
            else{
                 flash.message =='FAILED! Investment80c not submitted'
                 redirect(controller:"Employee", action:"dec");
            }
        }
    }


    def edit(Investment80c investment80cInstance) {
        respond investment80cInstance
    }

    @Transactional
    def update(Investment80c investment80cInstance) {
        if (investment80cInstance == null) {
            notFound()
            return
        }

        if (investment80cInstance.hasErrors()) {
            respond investment80cInstance.errors, view:'edit'
            return
        }

        investment80cInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Investment80c.label', default: 'Investment80c'), investment80cInstance.id])
                redirect investment80cInstance
            }
            '*'{ respond investment80cInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Investment80c investment80cInstance) {

        if (investment80cInstance == null) {
            notFound()
            return
        }

        investment80cInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Investment80c.label', default: 'Investment80c'), investment80cInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'investment80c.label', default: 'Investment80c'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
