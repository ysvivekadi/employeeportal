package new1



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class Nps80cController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Nps80c.list(params), model:[nps80cInstanceCount: Nps80c.count()]
    }

    def show(Nps80c nps80cInstance) {
        respond nps80cInstance
    }

    def create() {
        respond new Nps80c(params)
    }

    @Transactional
     def save() {
        System.out.println("")
       if(params == null){
            redirect(controller:"Employee", action:"dec");
            flash.message=''
            return
        }
        def mall = Nps80c.findByTreb(params.trebin)
        if(mall){
            flash.message="alredy given";
            redirect(controller:"Employee", action:"dec");
        }
        else{
            def newM = new Nps80c();
            
            
            int from=params.tamountin.toInteger()
            
            String to=params.trebin.toString()
            newM.treb= to
            newM.tamount=from
            System.out.println( )

            
            
            newM.employee = Employee.findByUsername(params.username)
            System.out.println(params.username)
            if(newM.save(flush:true)){
                flash.message =='SUCCESS! Nps80c submitted '
                println 'harihari'
               redirect(controller:"Employee", action:"dec");
            }
            else{
                 flash.message =='FAILED! Nps80c not submitted'
                 redirect(controller:"Employee", action:"dec");
            }
        }
    }

    def edit(Nps80c nps80cInstance) {
        respond nps80cInstance
    }

    @Transactional
    def update(Nps80c nps80cInstance) {
        if (nps80cInstance == null) {
            notFound()
            return
        }

        if (nps80cInstance.hasErrors()) {
            respond nps80cInstance.errors, view:'edit'
            return
        }

        nps80cInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Nps80c.label', default: 'Nps80c'), nps80cInstance.id])
                redirect nps80cInstance
            }
            '*'{ respond nps80cInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Nps80c nps80cInstance) {

        if (nps80cInstance == null) {
            notFound()
            return
        }

        nps80cInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Nps80c.label', default: 'Nps80c'), nps80cInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'nps80c.label', default: 'Nps80c'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
