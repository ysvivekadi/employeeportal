package new1



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

//@Transactional(readOnly = true)
class MessageController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index() {
       
    }

    def join(String nickname) {
        if ( nickname.trim() == '' ) {
            redirect(action:'index')
        } else {
            session.nickname = nickname
            render (view: 'chat')
        }
    }

    def retrieveLatestMessages() {
        def messages = Message.listOrderByDate(order: 'desc', max:10)
        [messages:messages.reverse()]
    }
    def submitMessage() {
            Date xyz=new Date();
        new Message(date:xyz , nickname: session.emp.username , message:params.comments).save()
        
    }

   
}
