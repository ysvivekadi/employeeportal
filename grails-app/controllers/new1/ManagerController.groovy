 package new1

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import org.json.JSONObject;
import org.json.JSONArray;
import java.text.SimpleDateFormat;
import java.text.DateFormat;



@Transactional(readOnly = true)
class ManagerController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE" , timesheet:"GET"]

    def index(Integer max) {
        session.manage = null;
        params.max = Math.min(max ?: 10, 100)
        respond Manager.list(params), model: [managerInstanceCount: Manager.count()]
    }

    def sal()
    {
        def emp = Employee.findAllByManager(session.manage)
        render(view:"sal",model:[emp:emp])

    }

    def show_sal()
    { def s= Salary.findById(params.s);
       render(view:"show_sal", model:[s:s]);
    }

    def show(Manager managerInstance) {
        respond managerInstance
    }
    def create() {
        respond new Manager(params)
    }
    def emp() {
        redirect(controller: "Employee", action: "index")
    }
    def login() {
        
        def manager = Manager.findByUsernameAndPassword(params.username, params.password)
                      
        if (manager) {
            session.manage = manager
            redirect(action: "abc", params: [username:manager.username])
        } else {
            flash.message = "Error"
                            redirect(action: "index")
        }
    }
     def salary(){
        def emp = Employee.findAllByManager(session.manage)
        render(view:"salary",model:[emp:emp])
    }
    def empcreation() {
        def emplist = Employee.findAllByManager(session.manage)
        render(view: "empcreation", model: [emplist:emplist]);
    }
    def abc() {
        def a = Manager.findByUsername(session.manage.username);
        System.out.println("id=" + a.id);
        def b = Project.getAll();
        def c = Project.createCriteria().list {
            manager {
                eq('id' , a.id)
            }
        }
        System.out.println(c + "\n");
        System.out.println("name: " + c.project_name + " manager_id=" + c.manager.id);
        if (c.size() == 0) {
            flash.message = "no"
                            render(view: "abc", model: [a:a]);
        } else {
            flash.message = "yes"
                            render(view: "abc", model: [a:a , b:b])
        }
    }
    def gantt() {

    }
    @Transactional
    def save() {

    }
    def edit(Manager managerInstance) {
        respond managerInstance
    }
    def check(){
        def man = Manager.findByUsername(session.manage.username);
        def emp = Employee.findAllByManager(man);
        JSONArray nitai = new JSONArray();
        for(b in emp){
        def persons = Checkin_checkout.findAllByEmployee(b);
            for(a in persons){

                JSONArray gaur = new JSONArray();
                gaur.put(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(a.checkin_date));
                gaur.put(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(a.checkout_date));
                gaur.put(a.time_duration);
                def pro = Profile.findByEmployee(a.employee)
                if(pro.first)
                    gaur.put(pro.first);
                else
                    gaur.put(a.employee);
                nitai.put(gaur); 
            }
        }
        if(flash.message!=1)
            flash.message = '0'    
        println flash.message
        render(view: "check", model: [manager:man, emp: emp, data:nitai])
    }
    def timesheet() {
        System.out.println("nggzxvbnxcvbn")
        def man = Manager.findByUsername(session.manage.username);
        def emp = Employee.findAllByManager(man);
        JSONArray nitai = new JSONArray();
        for(b in emp){
        def tab = Timesheet.findAllByEmployee(b);
            for(a in tab){
                JSONArray gaur = new JSONArray();
                gaur.put(new SimpleDateFormat("dd/MM/yyyy").format(a.fromdate));
                gaur.put(new SimpleDateFormat("dd/MM/yyyy").format(a.todate));
                gaur.put(a.project);
                gaur.put(a.task);
                gaur.put(a.description);
                gaur.put(a.work_hour);
                def pro = Profile.findByEmployee(a.employee)
                if(pro.first)
                    gaur.put(pro.first);
                else
                    gaur.put(a.employee);
                nitai.put(gaur);
            }
        }
        if(flash.message!=1)
            flash.message = '0'    
        println flash.message
        render(view: "timesheet", model: [manager:man, emp: emp, data:nitai])
    }

    def selectemp() {
        def man = Manager.findByUsername(session.manage.username);
        def emp = Employee.findAllByManager(man)
        def employee = Employee.findByUsername(params.emplo);
        System.out.println (params.emplo+"-------------")
        def tab = Timesheet.findAllByEmployee(employee);
        System.out.println(emp+" "+employee);
        JSONArray nitai = new JSONArray();
        for (a in tab) {
            JSONArray gaur = new JSONArray();
            gaur.put(a.fromdate);
            gaur.put(a.todate);
            gaur.put(a.project);
            gaur.put(a.task);
            gaur.put(a.description);
            gaur.put(a.work_hour);
            gaur.put(a.employee);
            nitai.put(gaur);
        }
        flash.message = '1'
        render(view: "timesheet", model: [manager:man, emp: emp, employee:employee , data:nitai])
    }
    @Transactional

    def update(Manager managerInstance) {
        if (managerInstance == null) {
            notFound()
            return
        }

        if (managerInstance.hasErrors()) {
            respond managerInstance.errors, view: 'edit'
            return
        }

        managerInstance.save flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Manager.label', default: 'Manager'), managerInstance.id])
                redirect managerInstance
            }
            '*'{ respond managerInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Manager managerInstance) {

        if (managerInstance == null) {
            notFound()
            return
        }

        managerInstance.delete flush: true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Manager.label', default: 'Manager'), managerInstance.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'manager.label', default: 'Manager'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
