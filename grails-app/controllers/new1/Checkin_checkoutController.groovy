package new1



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import org.json.JSONObject;
import org.json.JSONArray;
import java.text.SimpleDateFormat;
//@Transactional(readOnly = true)
class Checkin_checkoutController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Checkin_checkout.list(params), model:[checkin_checkoutInstanceCount: Checkin_checkout.count()]
    }

    def show(Checkin_checkout checkin_checkoutInstance) {
        respond checkin_checkoutInstance
    }

    def create() {
        respond new Checkin_checkout(params)
    }

    def testing(){
        def persons = Checkin_checkout.findAllById(params.id1);
        JSONArray nitai = new JSONArray();
        for(person in persons) {
            JSONArray gaur = new JSONArray();
            gaur.put(new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(person.checkin_date));
            gaur.put(new SimpleDateFormat("MM/dd/yyyy HH:mm:ss").format(person.checkout_date));
            gaur.put(person.time_duration);
            gaur.put(person.employee_id+""); 
            nitai.put(gaur); 
        }
        [data:nitai]
    }

    def entry(){
        System.out.println("1there");
        session.checkout = 1;
        def empl = Employee.findByUsername(params.emp)
        Date date = new Date()
        def intime =  new Checkin_checkout()
        intime.checkin_date = date
        intime.checkout_date = date
        intime.time_duration = ''
        intime.employee = empl
        if (intime.save(flush:true)) {
               render(view:"index")
           }   
        else
            render('not saved ent')
        session.xyz = intime
        render "done";
    }
    def exit(){
        System.out.println("2there");
        session.checkout = null;
        Date date = new Date()
        session.xyz.checkout_date = date
        Date d2 = session.xyz.checkout_date
        Date d1 = session.xyz.checkin_date
        long diff1 = d2.getTime() - d1.getTime()
        long diff2 = diff1/1000
        long diffSeconds = diff2 % 60
        long diffMinutes = diff2/ 60
        long diffHours = diff2 / (60 * 60 )
        session.xyz.time_duration =  String.valueOf(diffHours)+":"+String.valueOf(diffMinutes)+":"+String.valueOf(diffSeconds)       //'diffhours : diffMinutes : diffSeconds'
        if (session.xyz.save(flush:true)) {
               render('saved exit')
           }   
        else
            render('not saved exitt')
        render "done";
        //System.out.println(session.xyz.checkout_date)
    }
 
    @Transactional
    def save(Checkin_checkout checkin_checkoutInstance) {
        if (checkin_checkoutInstance == null) {
            notFound()
            return
        }

        if (checkin_checkoutInstance.hasErrors()) {
            respond checkin_checkoutInstance.errors, view:'create'
            return
        }

        checkin_checkoutInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'checkin_checkout.label', default: 'Checkin_checkout'), checkin_checkoutInstance.id])
                redirect checkin_checkoutInstance
            }
            '*' { respond checkin_checkoutInstance, [status: CREATED] }
        }
    }

    def edit(Checkin_checkout checkin_checkoutInstance) {
        respond checkin_checkoutInstance
    }

    @Transactional
    def update(Checkin_checkout checkin_checkoutInstance) {
        if (checkin_checkoutInstance == null) {
            notFound()
            return
        }

        if (checkin_checkoutInstance.hasErrors()) {
            respond checkin_checkoutInstance.errors, view:'edit'
            return
        }

        checkin_checkoutInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Checkin_checkout.label', default: 'Checkin_checkout'), checkin_checkoutInstance.id])
                redirect checkin_checkoutInstance
            }
            '*'{ respond checkin_checkoutInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Checkin_checkout checkin_checkoutInstance) {

        if (checkin_checkoutInstance == null) {
            notFound()
            return
        }

        checkin_checkoutInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Checkin_checkout.label', default: 'Checkin_checkout'), checkin_checkoutInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'checkin_checkout.label', default: 'Checkin_checkout'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
