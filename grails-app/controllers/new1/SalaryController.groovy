package new1



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class SalaryController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Salary.list(params), model:[salaryInstanceCount: Salary.count()]
    }

    def show(Salary salaryInstance) {
        respond salaryInstance
    }


    def c_show()
    {
        def e= Employee.findByUsername(params.employee)
        def s= Salary.findByYearAndEmployee(params.year, e);
        if(!(s)) 
        {
            flash.message="salary structure for the choosen YEAR and EMPLOYEE doesn't exist!"
            redirect(controller: "manager", action: "sal");
            return;
        }
        else redirect(controller: "manager", action: "show_sal",  params:[s:s.id]);
    }

    def create() {
        respond new Salary(params)
    }

    @Transactional
    def save(){
        def e= Employee.findByUsername(params.employee)
        println e
        def s= Salary.findByYearAndEmployee(params.year, e);
        if(s)
        {
            flash.message="salary structure for this YEAR already exists for this employee!"
            redirect(controller: "manager", action: "salary")
            return;
        }
        def newS = new Salary();

        newS.basic=Double.parseDouble(params.basic)
        newS.conveyance=Double.parseDouble(params.conveyance)
        newS.special=Double.parseDouble(params.special)
        newS.medical=Double.parseDouble(params.medical)
        newS.employeePF=Double.parseDouble(params.employeePF)
        newS.employerPF=Double.parseDouble(params.employerPF)
        newS.year = Integer.parseInt(params.year)
        newS.employee = Employee.findByUsername(params.employee)
        //println params
        print newS.employee

       // print "<<<<<<<"+newS.basic +">>>>>>>>>>"
        if(newS.save(flush:true)){
            flash.message="new salary structure CREATED!"
        }
        else{
            flash.message="not saved"
        }

        redirect(controller:"manager" , action:"salary")
    }

    def edit(Salary salaryInstance) {
        respond salaryInstance
    }

    @Transactional
    def update(Salary salaryInstance) {
        if (salaryInstance == null) {
            notFound()
            return
        }

        if (salaryInstance.hasErrors()) {
            respond salaryInstance.errors, view:'edit'
            return
        }

        salaryInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Salary.label', default: 'Salary'), salaryInstance.id])
                redirect salaryInstance
            }
            '*'{ respond salaryInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Salary salaryInstance) {

        if (salaryInstance == null) {
            notFound()
            return
        }

        salaryInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Salary.label', default: 'Salary'), salaryInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'salary.label', default: 'Salary'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
