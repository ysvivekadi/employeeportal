package new1
import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

//@Transactional(readOnly = true)
class ProfileController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Profile.list(params), model:[profileInstanceCount: Profile.count()]
    }

    def show(Profile profileInstance) {
        respond profileInstance
    }

    def create() {
        respond new Profile(params)
    }
    def save1(){
        def emp= Employee.findByUsername(params.emp)
        def newp = new Profile();
        newp.employee = emp;
        newp.save(flush:true);
    }
    @Transactional
    def save() {
       def emp = Employee.findByUsername(params.username)
       def pro = Profile.findByEmployee(emp)
       if(!pro){
            println 'not'
            def newp = new Profile();
            newp.first = params.first
            newp.last = params.last
            newp.designation = params.designation
            newp.gender = params.gender
            
            newp.date = params.date
            newp.reportingto = params.reportingto
            newp.department = params.department
            newp.location = params.location
            newp.organization = params.organization
            newp.panno = params.panno
            newp.email = params.email
            newp.nationality = params.nationality
            newp.mobile = params.mobile
            newp.employee = emp;
            if(newp.save(flush:true)){
                flash.message= 'saved'
                redirect(controller:"Employee", action:"profile1")
            }
            else{
                flash.message= 'saved'
                render ('not saved')   
            }
        }
        else{
            println 'yes'
           pro.first= params.first
           pro.last= params.last
           pro.designation= params.designation
           pro.gender= params.gender
           pro.date= params.date
           pro.reportingto= params.reportingto
           pro.department= params.department
           pro.location= params.location
           pro.organization= params.organization
           pro.panno= params.panno
           pro.email= params.email
           pro.nationality= params.nationality
           pro.mobile= params.mobile
           if(pro.save(flush:true)){
                flash.message= 'saved'
                redirect(controller:"Employee", action:"profile1")
           }
           else{
            flash.message= 'not saved'
            render ('not saved')
           }
        }
    }

    def edit(Profile profileInstance) {
        respond profileInstance
    }

    @Transactional
    def update(Profile profileInstance) {
        if (profileInstance == null) {
            notFound()
            return
        }

        if (profileInstance.hasErrors()) {
            respond profileInstance.errors, view:'edit'
            return
        }

        profileInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Profile.label', default: 'Profile'), profileInstance.id])
                redirect profileInstance
            }
            '*'{ respond profileInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Profile profileInstance) {

        if (profileInstance == null) {
            notFound()
            return
        }

        profileInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Profile.label', default: 'Profile'), profileInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'profile.label', default: 'Profile'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
