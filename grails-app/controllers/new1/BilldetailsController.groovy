package new1

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class BilldetailsController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Billdetails.list(params), model:[billdetailsInstanceCount: Billdetails.count()]
    }

    def show(Billdetails billdetailsInstance) {
        respond billdetailsInstance
    }

    def create() {
        respond new Billdetails(params)
    }

    @Transactional
    def save(){
        if(params.mallow.equals("")){
            redirect(controller:"Employee", action:"dec");
            flash.message=''
            return
        }
        def mall = Billdetails.findByMallowance(params.mallow)
        if(mall){
            flash.message="alredy given";
            redirect(controller:"Employee", action:"dec");
        }
        else{
            def newM = new Billdetails();
            System.out.println("kuch"+params.mallow);
            int b = params.mallow.toInteger();
            newM.mallowance= b;
            System.out.println("b="+b);
            newM.employee = Employee.findByUsername(params.username)
            System.out.println("1\t"+params.username)
            if(newM.save(flush:true)){
                flash.message=='SUCCESS! Billdetails submitted '
                println 'harihari'
               redirect(controller:"Employee", action:"dec");
            }
            else{
                 flash.message =='FAILED! Billdetails not submitted'
                 redirect(controller:"Employee", action:"dec");
            }
        }
    }
   

    def edit(Billdetails billdetailsInstance) {
        respond billdetailsInstance
    }

    @Transactional
    def update(Billdetails billdetailsInstance) {
        if (billdetailsInstance == null) {
            notFound()
            return
        }

        if (billdetailsInstance.hasErrors()) {
            respond billdetailsInstance.errors, view:'edit'
            return
        }

        billdetailsInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Billdetails.label', default: 'Billdetails'), billdetailsInstance.id])
                redirect billdetailsInstance
            }
            '*'{ respond billdetailsInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Billdetails billdetailsInstance) {

        if (billdetailsInstance == null) {
            notFound()
            return
        }

        billdetailsInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Billdetails.label', default: 'Billdetails'), billdetailsInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'billdetails.label', default: 'Billdetails'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
