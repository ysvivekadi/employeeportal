package new1



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class MedinsController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Medins.list(params), model:[medinsInstanceCount: Medins.count()]
    }

    def show(Medins medinsInstance) {
        respond medinsInstance
    }

    def create() {
        respond new Medins(params)
    }

    @Transactional
    def save() {
        System.out.println("")
       if(params == null){
            redirect(controller:"Employee", action:"dec");
            flash.message=''
            return
        }
        def mall = Medins.findByAnnualpremium1(params.apremium)
        if(mall){
            flash.message="alredy given";
            redirect(controller:"Employee", action:"dec");
        }
        else{
            def newM = new Medins();



    
           int c = params.policyno.toInteger();
           newM.policynumber1=c
            

           int d = params.apremium.toInteger();
           newM.annualpremium1=d
           
            System.out.println(b)
    
           int f = params.policyno2.toInteger();
           newM.policynumber2=f
            

           int g = params.apremium2.toInteger();
           newM.annualpremium2=g
            
            newM.employee = Employee.findByUsername(params.username)
            System.out.println(params.username)
            if(newM.save(flush:true)){
                flash.message =='SUCCESS! Medins submitted '
                println 'harihari'
               redirect(controller:"Employee", action:"dec");
            }
            else{
                 flash.message =='FAILED! Medins not submitted'
                 redirect(controller:"Employee", action:"dec");
            }
        }
    }
    def edit(Medins medinsInstance) {
        respond medinsInstance
    }

    @Transactional
    def update(Medins medinsInstance) {
        if (medinsInstance == null) {
            notFound()
            return
        }

        if (medinsInstance.hasErrors()) {
            respond medinsInstance.errors, view:'edit'
            return
        }

        medinsInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Medins.label', default: 'Medins'), medinsInstance.id])
                redirect medinsInstance
            }
            '*'{ respond medinsInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Medins medinsInstance) {

        if (medinsInstance == null) {
            notFound()
            return
        }

        medinsInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Medins.label', default: 'Medins'), medinsInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'medins.label', default: 'Medins'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
