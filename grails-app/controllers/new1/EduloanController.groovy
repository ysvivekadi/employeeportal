package new1



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class EduloanController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Eduloan.list(params), model:[eduloanInstanceCount: Eduloan.count()]
    }

    def show(Eduloan eduloanInstance) {
        respond eduloanInstance
    }

    def create() {
        respond new Eduloan(params)
    }

    @Transactional
     def save() {
        System.out.println("")
       if(params == null){
            redirect(controller:"Employee", action:"dec");
            flash.message=''
            return
        }
        def mall = Eduloan.findByTreb(params.trebe)
        if(mall){
            flash.message="alredy given";
            redirect(controller:"Employee", action:"dec");
        }
        else{
            def newM = new Eduloan();
            
            int from=params.tamounte.toInteger()
            String to=params.trebe.toString()
            newM.treb= to
            newM.tamount=from
            System.out.println( )

            
            
            newM.employee = Employee.findByUsername(params.username)
            System.out.println(params.username)
            if(newM.save(flush:true)){
                flash.message =='SUCCESS! Eduloan submitted '
                println 'harihari'
               redirect(controller:"Employee", action:"dec");
            }
            else{
                 flash.message =='FAILED! Eduloan not submitted'
                 redirect(controller:"Employee", action:"dec");
            }
        }
    }

    

    def edit(Eduloan eduloanInstance) {
        respond eduloanInstance
    }

    @Transactional
    def update(Eduloan eduloanInstance) {
        if (eduloanInstance == null) {
            notFound()
            return
        }

        if (eduloanInstance.hasErrors()) {
            respond eduloanInstance.errors, view:'edit'
            return
        }

        eduloanInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Eduloan.label', default: 'Eduloan'), eduloanInstance.id])
                redirect eduloanInstance
            }
            '*'{ respond eduloanInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Eduloan eduloanInstance) {

        if (eduloanInstance == null) {
            notFound()
            return
        }

        eduloanInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Eduloan.label', default: 'Eduloan'), eduloanInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'eduloan.label', default: 'Eduloan'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
