package new1
import java.net.InetAddress;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.commons.net.ntp.NTPUDPClient; 
import org.apache.commons.net.ntp.TimeInfo;

import java.io.File;
import java.io.FileInputStream;
import java.util.Date;

import jxl.Workbook;
import jxl.format.VerticalAlignment;
import jxl.write.*;


import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional


import java.net.InetAddress;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.commons.net.ntp.NTPUDPClient; 
import org.apache.commons.net.ntp.TimeInfo;

import java.io.File;
import java.io.FileInputStream;
import java.util.Date;

import jxl.Workbook;
import jxl.format.VerticalAlignment;
import jxl.write.*;

@Transactional(readOnly = true)
class SalarySheetController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]


    WritableWorkbook workbook;
    WritableSheet sheet;
    WritableFont cellFont;
    WritableCellFormat newCellFormat;
    WritableCellFormat cellFormat;
    String yearDuration;
    String yearQueried;
    SimpleDateFormat sdfDate;
    Date currDate;
    String strDate;
    Date dateOfJoin;
    int startMonth;
    int multiFactor;
    String str_Date;
    SimpleDateFormat formatter;
    Date startDate;
    Date endDate;
    long actualDiff;
    long diff;
    int noOfMonths;
    SimpleDateFormat monthFormat;
    SimpleDateFormat yearFormat;
    int currYear;
    int yearOfJoin;
    ArrayList<String> months;
    ArrayList<String> years;
    int lastColumn;
    int noOfSalComp;
    ArrayList<String> salComp;
    int salaryStartRow;
    int workingMonths;
    double[] salStruct;
    double totalSalPerMonth;
    int row;
    double grossTHComp;
    float employeePF;
    int rowGrossDed;
    float totalGrossDed;
    double netPayBeforeTaxes;
    int rowTax;
    WritableCellFormat boldBlueCellFormat;
    WritableCellFormat noBoldBlackRightCellFormat;
    WritableCellFormat noBoldBlackLeftCellFormat;
    double professionTax;
    double[] inTaxPerMonth;
    double totInTax;
    double totInTaxYTD;
    double[] netPayPerMonth;
    double totNetPayAfterTaxes;
    double totNetPayAfterTaxesYTD;
    int month;
    Label lable;



//  this function is just for purpose of generating random date of joining till the time the date is not taken from database
     Date randDateOfJoin() throws Exception{
            GregorianCalendar gc = new GregorianCalendar();
            int year = randBetween(1980, 2015);
            gc.set(gc.YEAR, year);
            int dayOfYear = randBetween(1, gc.getActualMaximum(gc.DAY_OF_YEAR));
            gc.set(gc.DAY_OF_YEAR, dayOfYear);
            String dateOfJoin_Str = (gc.get(gc.YEAR) + "-" + (gc.get(gc.MONTH) + 1) + "-" + gc.get(gc.DAY_OF_MONTH));
            System.out.println(dateOfJoin_Str);
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-M-d");
            Date dateOfJoin = dateFormat.parse(dateOfJoin_Str);
            return dateOfJoin;
        }

        static int randBetween(int start, int end) {
            return start + (int)Math.round(Math.random() * (end - start));
        }
    
    Date currentDate() throws Exception
    {       
        String TIME_SERVER = "time-a.nist.gov";   
        NTPUDPClient timeClient = new NTPUDPClient();
        InetAddress inetAddress = InetAddress.getByName(TIME_SERVER);
        TimeInfo timeInfo = timeClient.getTime(inetAddress);
        long returnTime = timeInfo.getReturnTime();
        Date time = new Date(returnTime);
        return time;
    }  

    def generateSalarySheet()
    {
        println 'hari'
        println 'harihari'
        response.setContentType('application/vnd.ms-excel')
        response.setHeader('Content-Disposition', 'Attachment;Filename="salary_sheet.xls"')
        try
        {
             workbook = Workbook.createWorkbook(response.outputStream);
             sheet = workbook.createSheet("SalarySheet", 0);
             sheet.getSettings().setProtected(true);
             
             sheet.setColumnView(0, 33);
             for(int i=1;i<=15;i++)
            {
                sheet.setColumnView(i, 23);
            }
                
                
             cellFont = new WritableFont(WritableFont.TIMES, 16);
            cellFont.setBoldStyle(WritableFont.BOLD);
            cellFont.setColour(Colour.BLACK);

             newCellFormat = new WritableCellFormat(cellFont);
            newCellFormat.setBackground(Colour.WHITE);
            newCellFormat.setAlignment(Alignment.CENTRE);
            newCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
            

            cellFormat = new WritableCellFormat(newCellFormat);
            cellFormat.setBorder(Border.NONE, BorderLineStyle.THICK);
            
//          get year duration
            yearDuration="Apr 15 - Mar 16"; 
            
            yearQueried = yearDuration.substring(4, 6);   // 15 in this case
                    
            sheet.mergeCells(0, 0,7,0);
             lable = new Label(0, 0, "Salary sheet for the year "+yearDuration, cellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(8, 0,13,0);
            lable = new Label(8, 0, "", cellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(0,1, 13,1);
            lable = new Label(0, 1, "", cellFormat);
            sheet.addCell(lable);
            
            cellFont = new WritableFont(WritableFont.TIMES, 11);
            cellFont.setBoldStyle(WritableFont.BOLD);
            cellFont.setColour(Colour.BLACK);

            newCellFormat = new WritableCellFormat(cellFont);
            newCellFormat.setBackground(Colour.WHITE);
            newCellFormat.setAlignment(Alignment.CENTRE);
            newCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
            

            cellFormat = new WritableCellFormat(newCellFormat);
            cellFormat.setBorder(Border.ALL, BorderLineStyle.THICK);
            cellFormat.setAlignment(Alignment.LEFT);
            
            lable = new Label(0, 2, "Employee ID ", cellFormat);
            sheet.addCell(lable);

            def emp = session.emp
            def profile = Profile.findByEmployeeAndYear(emp)

            String id=String.valueOf(emp.id)
            sheet.mergeCells(1, 2, 2, 2);
            lable = new Label(1, 2, id, cellFormat);
            sheet.addCell(lable);
                        
            lable = new Label(0, 3, "Name ", cellFormat);
            sheet.addCell(lable);
            String name=profile.first+" "+profile.last;
            sheet.mergeCells(1, 3, 2, 3);
            lable = new Label(1, 3, name, cellFormat);
            sheet.addCell(lable);
                       
            lable = new Label(0, 4, "Organization ", cellFormat);
            sheet.addCell(lable);
            String org=profile.organization;
            sheet.mergeCells(1, 4, 2, 4);
            lable = new Label(1, 4, org, cellFormat);
            sheet.addCell(lable);

            lable = new Label(0, 5, "Date of Report Generation ", cellFormat);
            sheet.addCell(lable);
            
//          getting current date and time
                
            sdfDate = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");      //dd/MM/yyyy
            currDate = currentDate();   
            strDate = sdfDate.format(currDate);
            sheet.mergeCells(1, 5, 2, 5);
            lable = new Label(1, 5, strDate, cellFormat);
            sheet.addCell(lable);

            lable = new Label(0, 6, "PAN", cellFormat);
            sheet.addCell(lable);
            String pan=profile.panno;
            sheet.mergeCells(1, 6, 2, 6);
            lable = new Label(1, 6, pan, cellFormat);
            sheet.addCell(lable);
            
//          merging right empty side
            
            cellFormat = new WritableCellFormat(newCellFormat);
            cellFormat.setBorder(Border.NONE, BorderLineStyle.THICK);
            
            sheet.mergeCells(3,2,13,6);
            lable = new Label(3, 2, "", cellFormat);
            sheet.addCell(lable);
            
//          merging rows 7,8,9
            
            sheet.mergeCells(0,7,13,9);
            lable = new Label(0, 7, "", cellFormat);
            sheet.addCell(lable);
            
            cellFont = new WritableFont(WritableFont.TIMES, 12);
            cellFont.setBoldStyle(WritableFont.BOLD);
            cellFont.setColour(Colour.BLACK);

            newCellFormat = new WritableCellFormat(cellFont);
            newCellFormat.setBackground(Colour.LIGHT_BLUE);
            newCellFormat.setAlignment(Alignment.CENTRE);
            newCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
            
            cellFormat = new WritableCellFormat(newCellFormat);
            cellFormat.setBorder(Border.ALL, BorderLineStyle.THICK);
            
            sheet.mergeCells(0,10,0,11);
            lable = new Label(0, 10, "Compensation/ Deduction", cellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(1,10,1,11);
            lable = new Label(1, 10, "Annual", cellFormat);
            sheet.addCell(lable);
            
//          getting the Date of join from Profile class
            dateOfJoin = profile.date
            
//          startMonth of the current year
            startMonth = 0;  // 0 means april
            
            
//          if difference between current date and 1 april is less than diff b/w 31 mar and 1 apr, 
//              if current year is year of joining also, then noOfMonths = currMonth-monthOfJoin+1 & startMonth=monthOfJoin
//              else  noOfMonths=currMonth-april+1 & startMonth = april
//          else noOfMonths=12       & startMonth = april
            
//          this variable multiFactor stores whether in Annual column, i have to multiply by 12 if current is not year of join,
//          else, multiplying by multiFactor, where multiFactor = 12 - (monthOfJoin - 4) i.e. 16 - monthOfJoin
            multiFactor = 12;
            
            str_Date = "01/04/"+yearQueried;            
             formatter = new SimpleDateFormat("dd/MM/yy");
             startDate = formatter.parse(str_Date); 
            str_Date = "31/03/"+String.valueOf(Integer.valueOf(yearQueried)+1); 
             endDate = (Date)formatter.parse(str_Date); 
             actualDiff = currDate.getTime() - startDate.getTime();
             diff = endDate.getTime() - startDate.getTime();
            if(actualDiff < diff)
            {
                 monthFormat = new SimpleDateFormat("M");
                currDate = currentDate();   
                 yearFormat = new SimpleDateFormat("YYYY");
                 currYear = Integer.valueOf(yearFormat.format(currDate));
                 yearOfJoin = Integer.valueOf(yearFormat.format(dateOfJoin));               
                if(currYear == yearOfJoin)
                { 
                    multiFactor = 16 - Integer.valueOf(monthFormat.format(dateOfJoin));
                    noOfMonths = Integer.valueOf(monthFormat.format(currDate))-Integer.valueOf(monthFormat.format(dateOfJoin))+1;
                    startMonth = Integer.valueOf(monthFormat.format(dateOfJoin)) - 4;
                }
                else
                {
                    noOfMonths = Integer.valueOf(monthFormat.format(currDate))-3;
                }
                
            }
            else noOfMonths = 12;
            
            sheet.mergeCells(2,10,1+noOfMonths,10);
            lable = new Label(2, 10, "Past Salary Details", cellFormat);
            sheet.addCell(lable);
            
//          array list to store months
            
             months = new ArrayList<String>();
             years = new ArrayList<String>();
            months.add("Apr");years.add(String.valueOf(yearQueried));
            months.add("May");years.add(String.valueOf(yearQueried));
            months.add("Jun");years.add(String.valueOf(yearQueried));
            months.add("Jul");years.add(String.valueOf(yearQueried));
            months.add("Aug");years.add(String.valueOf(yearQueried));
            months.add("Sep");years.add(String.valueOf(yearQueried));
            months.add("Oct");years.add(String.valueOf(yearQueried));
            months.add("Nov");years.add(String.valueOf(yearQueried));
            months.add("Dec");years.add(String.valueOf(yearQueried));
            months.add("Jan");years.add(String.valueOf(Integer.valueOf(yearQueried)+1));
            months.add("Feb");years.add(String.valueOf(Integer.valueOf(yearQueried)+1));
            months.add("Mar");years.add(String.valueOf(Integer.valueOf(yearQueried)+1));

            
            month = startMonth;
            for(int i=2; i<= 1+noOfMonths; i++)
            {
                lable = new Label(i, 11, months.get(month)+"'"+years.get(month), cellFormat);
                sheet.addCell(lable);
                month++;
            }
            
//          LAST COLUMN OF THE SHEET
             lastColumn = 2+noOfMonths;
            
            sheet.mergeCells(lastColumn, 10, lastColumn, 11);
            lable = new Label(lastColumn, 10, "YTD", cellFormat);
            sheet.addCell(lable);
            
            newCellFormat = new WritableCellFormat(cellFont);
            newCellFormat.setBackground(Colour.WHITE);
            newCellFormat.setAlignment(Alignment.CENTRE);
            newCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
            
            cellFormat = new WritableCellFormat(newCellFormat);
            cellFormat.setBorder(Border.ALL, BorderLineStyle.THICK);
            cellFormat.setAlignment(Alignment.LEFT);
            
            sheet.mergeCells(0,12,lastColumn,12);
            lable = new Label(0, 12, "Compensation Details", cellFormat);
            sheet.addCell(lable);
            
//          REMOVING BOLD STYLE
            
            cellFont = new WritableFont(WritableFont.TIMES, 12);
            cellFont.setBoldStyle(WritableFont.NO_BOLD);
            cellFont.setColour(Colour.BLACK);

            newCellFormat = new WritableCellFormat(cellFont);
            newCellFormat.setBackground(Colour.WHITE);
            newCellFormat.setAlignment(Alignment.CENTRE);
            newCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
            
            cellFormat = new WritableCellFormat(newCellFormat);
            cellFormat.setBorder(Border.ALL, BorderLineStyle.THICK);
            cellFormat.setAlignment(Alignment.LEFT);
            
//          storing number of different components of salary structure in noOfSalComp
             noOfSalComp = 6;  //basic, hra, conveyance allowance, medical allowance, special allowance, employer pf
            
//          ArrayList to store all salary components, get them from salary_structure class
            salComp = new ArrayList<String>();
            salComp.add("Basic");salComp.add("HRA");salComp.add("Conveyance Allowance");salComp.add("Medical Allowance");
            salComp.add("Special Allowance");salComp.add("Employer PF");
            
            
//          starting row number for salary structure
             salaryStartRow = 13;

            for(int i=salaryStartRow; i <= salaryStartRow+noOfSalComp-1; i++)
            {
                lable = new Label(0, i, salComp.get(i-salaryStartRow), cellFormat);
                sheet.addCell(lable);
            }
            
//          writing annual pay under different components in column 1 (starting index 0)            
             workingMonths = 12 - startMonth;
            
//          get the salary structure of the employee and particular year: sal.findByEmployeeAndYear(... , ...)
//          then store values of all attributes in arraylist to reduce redundancy
//          make sure the values are entered in salStruct array corresponding to the strings in salComp arraylist
            
             salStruct = new double[noOfSalComp];
             def salary = Salary.findByEmployeeAndYear(emp) 
//          make a for loop to enter the data instead of entering the way done below
            salStruct[0] = salary.basic;
            salStruct[1] = salary.hra;
            salStruct[2] = salary.conveyance;
            salStruct[3] = salary.medical;
            salStruct[4] = salary.special;
            salStruct[5] = salary.employerPF;
            
            cellFont = new WritableFont(WritableFont.TIMES, 12);
            cellFont.setBoldStyle(WritableFont.BOLD);
            cellFont.setColour(Colour.BLUE);

            newCellFormat = new WritableCellFormat(cellFont);
            newCellFormat.setBackground(Colour.WHITE);
            newCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
            
            cellFormat = new WritableCellFormat(newCellFormat);
            cellFormat.setBorder(Border.ALL, BorderLineStyle.THICK);
            cellFormat.setAlignment(Alignment.RIGHT);
            
            for(int i=salaryStartRow; i <= salaryStartRow+noOfSalComp-1; i++)
            {
                lable = new Label(1, i, String.valueOf(multiFactor*salStruct[i-salaryStartRow]), cellFormat);
                sheet.addCell(lable);
            }
            
            cellFont = new WritableFont(WritableFont.TIMES, 12);
            cellFont.setBoldStyle(WritableFont.NO_BOLD);
            cellFont.setColour(Colour.BLACK);

            newCellFormat = new WritableCellFormat(cellFont);
            newCellFormat.setBackground(Colour.WHITE);
            newCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
            
            cellFormat = new WritableCellFormat(newCellFormat);
            cellFormat.setBorder(Border.ALL, BorderLineStyle.THICK);
            cellFormat.setAlignment(Alignment.RIGHT);
            
//          fill the same details in total workingMonths columns
            
            for(int column = 2; column <= workingMonths+1; column++)
            {
                for(int i=salaryStartRow; i <= salaryStartRow+noOfSalComp-1; i++)
                {
                    lable = new Label(column, i, String.valueOf(salStruct[i-salaryStartRow]), cellFormat);
                    sheet.addCell(lable);
                }
            }
            
            totalSalPerMonth = 0;
            for(int i=0; i<noOfSalComp; i++)
            {
                totalSalPerMonth += salStruct[i];
            }
            
            cellFont = new WritableFont(WritableFont.TIMES, 12);
            cellFont.setBoldStyle(WritableFont.BOLD);
            cellFont.setColour(Colour.BLACK);

            newCellFormat = new WritableCellFormat(cellFont);
            newCellFormat.setBackground(Colour.WHITE);
            newCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
            
            cellFormat = new WritableCellFormat(newCellFormat);
            cellFormat.setBorder(Border.ALL, BorderLineStyle.THICK);
            cellFormat.setAlignment(Alignment.LEFT);
            
//          row after the salary structure
             row = salaryStartRow+noOfSalComp;
            
            lable = new Label(0, row, "Gross Compensation", cellFormat);
            sheet.addCell(lable);
            lable = new Label(0, row+1, "Gross Take-home Compensation", cellFormat);
            sheet.addCell(lable);

             generateSalarySheet2();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    def generateSalarySheet2()
    {
        try {
            
            cellFont = new WritableFont(WritableFont.TIMES, 12);
            cellFont.setBoldStyle(WritableFont.BOLD);
            cellFont.setColour(Colour.BLUE);

            newCellFormat = new WritableCellFormat(cellFont);
            newCellFormat.setBackground(Colour.WHITE);
            newCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
            
            cellFormat = new WritableCellFormat(newCellFormat);
            cellFormat.setBorder(Border.ALL, BorderLineStyle.THICK);
            cellFormat.setAlignment(Alignment.RIGHT);
            
            lable = new Label(1, row, String.valueOf(totalSalPerMonth*multiFactor), cellFormat);
            sheet.addCell(lable);
            
//          Subtracting employer PF from total salary per month to obtain gross take-home compensation
             grossTHComp = totalSalPerMonth - salStruct[5];
            
            lable = new Label(1, row+1, String.valueOf(grossTHComp*multiFactor), cellFormat);
            sheet.addCell(lable);
             
            cellFont = new WritableFont(WritableFont.TIMES, 12);
            cellFont.setBoldStyle(WritableFont.BOLD);
            cellFont.setColour(Colour.BLACK);

            newCellFormat = new WritableCellFormat(cellFont);
            newCellFormat.setBackground(Colour.WHITE);
            newCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
            
            cellFormat = new WritableCellFormat(newCellFormat);
            cellFormat.setBorder(Border.ALL, BorderLineStyle.THICK);
            cellFormat.setAlignment(Alignment.RIGHT);
            
            for(int column = 2; column <= workingMonths+1; column++)
            {           
                lable = new Label(column, row, String.valueOf(totalSalPerMonth), cellFormat);
                sheet.addCell(lable);
                lable = new Label(column, row+1, String.valueOf(grossTHComp), cellFormat);
                sheet.addCell(lable);               
            }
            
            cellFont = new WritableFont(WritableFont.TIMES, 12);
            cellFont.setBoldStyle(WritableFont.BOLD);
            cellFont.setColour(Colour.BLUE);

            newCellFormat = new WritableCellFormat(cellFont);
            newCellFormat.setBackground(Colour.WHITE);
            newCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
            
            cellFormat = new WritableCellFormat(newCellFormat);
            cellFormat.setBorder(Border.ALL, BorderLineStyle.THICK);
            cellFormat.setAlignment(Alignment.RIGHT);
            
            int j=0;
            for(int i = salaryStartRow; i<= row-1; i++)
            {
                lable = new Label(lastColumn, i, String.valueOf(workingMonths*salStruct[j]), cellFormat);
                sheet.addCell(lable);
                j++;
            }
            lable = new Label(lastColumn, row, String.valueOf(workingMonths*totalSalPerMonth), cellFormat);
            sheet.addCell(lable);
            lable = new Label(lastColumn, row+1, String.valueOf(workingMonths*grossTHComp), cellFormat);
            sheet.addCell(lable);
            
            cellFont = new WritableFont(WritableFont.TIMES, 12);
            cellFont.setBoldStyle(WritableFont.BOLD);
            cellFont.setColour(Colour.BLACK);

            newCellFormat = new WritableCellFormat(cellFont);
            newCellFormat.setBackground(Colour.WHITE);
            newCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
            
            cellFormat = new WritableCellFormat(newCellFormat);
            cellFormat.setBorder(Border.NONE, BorderLineStyle.THICK);
            cellFormat.setAlignment(Alignment.LEFT);
            
            sheet.mergeCells(0, row+2, lastColumn, row+2);
            lable = new Label(0, row+2, "", cellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(0, row+3, lastColumn, row+3);
            lable = new Label(0, row+3, "Deduction Details", cellFormat);
            sheet.addCell(lable);
            
            cellFont = new WritableFont(WritableFont.TIMES, 12);
            cellFont.setBoldStyle(WritableFont.NO_BOLD);
            cellFont.setColour(Colour.BLACK);

            newCellFormat = new WritableCellFormat(cellFont);
            newCellFormat.setBackground(Colour.WHITE);
            newCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
            
            cellFormat = new WritableCellFormat(newCellFormat);
            cellFormat.setBorder(Border.ALL, BorderLineStyle.THICK);
            cellFormat.setAlignment(Alignment.LEFT);
            
            lable = new Label(0, row+4, "Employee PF", cellFormat);
            sheet.addCell(lable);
            
//          storing Employee PF in variable employeePF from database
             employeePF = salary.employeePF;
            
            cellFont = new WritableFont(WritableFont.TIMES, 12);
            cellFont.setBoldStyle(WritableFont.BOLD);
            cellFont.setColour(Colour.BLUE);

            newCellFormat = new WritableCellFormat(cellFont);
            newCellFormat.setBackground(Colour.WHITE);
            newCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
            
            cellFormat = new WritableCellFormat(newCellFormat);
            cellFormat.setBorder(Border.ALL, BorderLineStyle.THICK);
            cellFormat.setAlignment(Alignment.RIGHT);
            
            lable = new Label(1, row+4, String.valueOf(employeePF*multiFactor), cellFormat);
            sheet.addCell(lable);
            
            cellFont = new WritableFont(WritableFont.TIMES, 12);
            cellFont.setBoldStyle(WritableFont.NO_BOLD);
            cellFont.setColour(Colour.BLACK);

            newCellFormat = new WritableCellFormat(cellFont);
            newCellFormat.setBackground(Colour.WHITE);
            newCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
            
            cellFormat = new WritableCellFormat(newCellFormat);
            cellFormat.setBorder(Border.ALL, BorderLineStyle.THICK);
            cellFormat.setAlignment(Alignment.RIGHT);
            
            for(int column = 2; column <= workingMonths+1; column++)
            {           
                lable = new Label(column, row+4, String.valueOf(employeePF), cellFormat);
                sheet.addCell(lable);               
            }
            
            cellFont = new WritableFont(WritableFont.TIMES, 12);
            cellFont.setBoldStyle(WritableFont.BOLD);
            cellFont.setColour(Colour.BLUE);

            newCellFormat = new WritableCellFormat(cellFont);
            newCellFormat.setBackground(Colour.WHITE);
            newCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
            
            cellFormat = new WritableCellFormat(newCellFormat);
            cellFormat.setBorder(Border.ALL, BorderLineStyle.THICK);
            cellFormat.setAlignment(Alignment.RIGHT);
            
            lable = new Label(lastColumn, row+4, String.valueOf(employeePF*workingMonths), cellFormat);
            sheet.addCell(lable);
            
//          row for gross deduction
             rowGrossDed = row+5;
            
            cellFont = new WritableFont(WritableFont.TIMES, 12);
            cellFont.setBoldStyle(WritableFont.BOLD);
            cellFont.setColour(Colour.BLACK);

            newCellFormat = new WritableCellFormat(cellFont);
            newCellFormat.setBackground(Colour.WHITE);
            newCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
            
            cellFormat = new WritableCellFormat(newCellFormat);
            cellFormat.setBorder(Border.ALL, BorderLineStyle.THICK);
            cellFormat.setAlignment(Alignment.LEFT);
            
            lable = new Label(0, rowGrossDed, "Gross Deduction", cellFormat);
            sheet.addCell(lable);
            
//          calculating total gross deduction
             totalGrossDed = employeePF;
            
            cellFont = new WritableFont(WritableFont.TIMES, 12);
            cellFont.setBoldStyle(WritableFont.BOLD);
            cellFont.setColour(Colour.BLUE);

            newCellFormat = new WritableCellFormat(cellFont);
            newCellFormat.setBackground(Colour.WHITE);
            newCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
            
            cellFormat = new WritableCellFormat(newCellFormat);
            cellFormat.setBorder(Border.ALL, BorderLineStyle.THICK);
            cellFormat.setAlignment(Alignment.RIGHT);
            
            lable = new Label(1, rowGrossDed, String.valueOf(multiFactor*totalGrossDed), cellFormat);
            sheet.addCell(lable);
            
            cellFont = new WritableFont(WritableFont.TIMES, 12);
            cellFont.setBoldStyle(WritableFont.BOLD);
            cellFont.setColour(Colour.BLACK);

            newCellFormat = new WritableCellFormat(cellFont);
            newCellFormat.setBackground(Colour.WHITE);
            newCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
            
            cellFormat = new WritableCellFormat(newCellFormat);
            cellFormat.setBorder(Border.ALL, BorderLineStyle.THICK);
            cellFormat.setAlignment(Alignment.RIGHT);
            
            for(int column = 2; column <= workingMonths+1; column++)
            {           
                lable = new Label(column, rowGrossDed, String.valueOf(totalGrossDed), cellFormat);
                sheet.addCell(lable);               
            }
            
            cellFont = new WritableFont(WritableFont.TIMES, 12);
            cellFont.setBoldStyle(WritableFont.BOLD);
            cellFont.setColour(Colour.BLUE);

            newCellFormat = new WritableCellFormat(cellFont);
            newCellFormat.setBackground(Colour.WHITE);
            newCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
            
            cellFormat = new WritableCellFormat(newCellFormat);
            cellFormat.setBorder(Border.ALL, BorderLineStyle.THICK);
            cellFormat.setAlignment(Alignment.RIGHT);
            
            lable = new Label(lastColumn, rowGrossDed, String.valueOf(workingMonths*totalGrossDed), cellFormat);
            sheet.addCell(lable);
            
            cellFont = new WritableFont(WritableFont.TIMES, 12);
            cellFont.setBoldStyle(WritableFont.NO_BOLD);
            cellFont.setColour(Colour.BLACK);
            newCellFormat = new WritableCellFormat(cellFont);
            newCellFormat.setBackground(Colour.WHITE);
            newCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
            cellFormat = new WritableCellFormat(newCellFormat);
            cellFormat.setBorder(Border.NONE, BorderLineStyle.THICK);
            cellFormat.setAlignment(Alignment.LEFT);
            
            sheet.mergeCells(0, rowGrossDed+1, lastColumn, rowGrossDed+1);
            lable = new Label(0, 1+rowGrossDed, "", cellFormat);
            sheet.addCell(lable);
            
            cellFormat = new WritableCellFormat(newCellFormat);
            cellFormat.setBorder(Border.ALL, BorderLineStyle.THICK);
            cellFormat.setAlignment(Alignment.LEFT);
            
            lable = new Label(0, 2+rowGrossDed, "Net Pay (before taxes)", cellFormat);
            sheet.addCell(lable);
            
            cellFont = new WritableFont(WritableFont.TIMES, 12);
            cellFont.setBoldStyle(WritableFont.BOLD);
            cellFont.setColour(Colour.BLUE);
            newCellFormat = new WritableCellFormat(cellFont);
            newCellFormat.setBackground(Colour.WHITE);
            newCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
            cellFormat = new WritableCellFormat(newCellFormat);
            cellFormat.setBorder(Border.ALL, BorderLineStyle.THICK);
            cellFormat.setAlignment(Alignment.RIGHT);
            
//          storing net pay (before taxes) in netPayBeforeTaxes
             netPayBeforeTaxes = grossTHComp - totalGrossDed;
            
            lable = new Label(1, 2+rowGrossDed, String.valueOf(multiFactor*netPayBeforeTaxes), cellFormat);
            sheet.addCell(lable);
            
            cellFont = new WritableFont(WritableFont.TIMES, 12);
            cellFont.setBoldStyle(WritableFont.NO_BOLD);
            cellFont.setColour(Colour.BLACK);
            newCellFormat = new WritableCellFormat(cellFont);
            newCellFormat.setBackground(Colour.WHITE);
            newCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
            cellFormat = new WritableCellFormat(newCellFormat);
            cellFormat.setBorder(Border.ALL, BorderLineStyle.THICK);
            cellFormat.setAlignment(Alignment.RIGHT);
            
            for(int column = 2; column <= workingMonths+1; column++)
            {           
                lable = new Label(column, 2+rowGrossDed, String.valueOf(netPayBeforeTaxes), cellFormat);
                sheet.addCell(lable);               
            }
            
            
            cellFont = new WritableFont(WritableFont.TIMES, 12);
            cellFont.setBoldStyle(WritableFont.BOLD);
            cellFont.setColour(Colour.BLUE);
            newCellFormat = new WritableCellFormat(cellFont);
            newCellFormat.setBackground(Colour.WHITE);
            newCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
            cellFormat = new WritableCellFormat(newCellFormat);
            cellFormat.setBorder(Border.ALL, BorderLineStyle.THICK);
            cellFormat.setAlignment(Alignment.RIGHT);
            
            lable = new Label(lastColumn, 2+rowGrossDed, String.valueOf(workingMonths*netPayBeforeTaxes), cellFormat);
            sheet.addCell(lable);
            
            cellFont = new WritableFont(WritableFont.TIMES, 12);
            cellFont.setBoldStyle(WritableFont.BOLD);
            cellFont.setColour(Colour.BLACK);
            newCellFormat = new WritableCellFormat(cellFont);
            newCellFormat.setBackground(Colour.WHITE);
            newCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
            cellFormat = new WritableCellFormat(newCellFormat);
            cellFormat.setBorder(Border.NONE, BorderLineStyle.THICK);
            cellFormat.setAlignment(Alignment.LEFT);
            
            sheet.mergeCells(0, 3+rowGrossDed, lastColumn, 3+rowGrossDed);
            lable = new Label(0, 3+rowGrossDed, "", cellFormat);
            sheet.addCell(lable);
            
            sheet.mergeCells(0, 4+rowGrossDed, lastColumn, 4+rowGrossDed);
            lable = new Label(0, 4+rowGrossDed, "Tax Details", cellFormat);
            sheet.addCell(lable);
            
//          tax row starts from here
             rowTax = rowGrossDed+5;
            
            cellFont = new WritableFont(WritableFont.TIMES, 12);
            cellFont.setBoldStyle(WritableFont.NO_BOLD);
            cellFont.setColour(Colour.BLACK);
            newCellFormat = new WritableCellFormat(cellFont);
            newCellFormat.setBackground(Colour.WHITE);
            newCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
            cellFormat = new WritableCellFormat(newCellFormat);
            cellFormat.setBorder(Border.ALL, BorderLineStyle.THICK);
            cellFormat.setAlignment(Alignment.LEFT);
            
            lable = new Label(0, rowTax, "Taxable Gross", cellFormat);
            sheet.addCell(lable);
            
//          bold blue cell format
            
            cellFont = new WritableFont(WritableFont.TIMES, 12);
            cellFont.setBoldStyle(WritableFont.BOLD);
            cellFont.setColour(Colour.BLUE);
            newCellFormat = new WritableCellFormat(cellFont);
            newCellFormat.setBackground(Colour.WHITE);
            newCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
             boldBlueCellFormat = new WritableCellFormat(newCellFormat);
            boldBlueCellFormat.setBorder(Border.ALL, BorderLineStyle.THICK);
            boldBlueCellFormat.setAlignment(Alignment.RIGHT);
            
            lable = new Label(1, rowTax, String.valueOf(multiFactor*grossTHComp), boldBlueCellFormat);
            sheet.addCell(lable);
            
//          no Bold Black right Cell Format

            cellFont = new WritableFont(WritableFont.TIMES, 12);
            cellFont.setBoldStyle(WritableFont.NO_BOLD);
            cellFont.setColour(Colour.BLACK);
            newCellFormat = new WritableCellFormat(cellFont);
            newCellFormat.setBackground(Colour.WHITE);
            newCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
             noBoldBlackRightCellFormat = new WritableCellFormat(newCellFormat);
            noBoldBlackRightCellFormat.setBorder(Border.ALL, BorderLineStyle.THICK);
            noBoldBlackRightCellFormat.setAlignment(Alignment.RIGHT);
            
            for(int column = 2; column <= workingMonths+1; column++)
            {           
                lable = new Label(column, rowTax, String.valueOf(grossTHComp), noBoldBlackRightCellFormat);
                sheet.addCell(lable);               
            }
            
            lable = new Label(lastColumn, rowTax, String.valueOf(workingMonths*grossTHComp), boldBlueCellFormat);
            sheet.addCell(lable);
            
//          no bold black left cell format
             noBoldBlackLeftCellFormat = new WritableCellFormat(newCellFormat);
            noBoldBlackLeftCellFormat.setBorder(Border.ALL, BorderLineStyle.THICK);
            noBoldBlackLeftCellFormat.setAlignment(Alignment.LEFT);
            
            lable = new Label(0, 1+rowTax, "Profession Tax", noBoldBlackLeftCellFormat);
            sheet.addCell(lable);
            
//          getting profession tax from Tax database
            def taxSheet = taxSheet.findByEmployeeAndYear(emp)
             professionTax = 200.0; 
            
            lable = new Label(1, 1+rowTax, String.valueOf(multiFactor*professionTax), boldBlueCellFormat);
            sheet.addCell(lable);
            
            for(int column = 2; column <= workingMonths+1; column++)
            {           
                lable = new Label(column, 1+rowTax, String.valueOf(professionTax), noBoldBlackRightCellFormat);
                sheet.addCell(lable);               
            }
            
            lable = new Label(lastColumn, 1+rowTax, String.valueOf(workingMonths*professionTax), boldBlueCellFormat);
            sheet.addCell(lable);   
            
            cellFont = new WritableFont(WritableFont.TIMES, 12);
            cellFont.setBoldStyle(WritableFont.BOLD);
            cellFont.setColour(Colour.BLACK);
            newCellFormat = new WritableCellFormat(cellFont);
            newCellFormat.setBackground(Colour.WHITE);
            newCellFormat.setVerticalAlignment(VerticalAlignment.CENTRE);
            cellFormat = new WritableCellFormat(newCellFormat);
            cellFormat.setBorder(Border.ALL, BorderLineStyle.THICK);
            cellFormat.setAlignment(Alignment.LEFT);
            
            lable = new Label(0, 2+rowTax, "Income Tax", cellFormat);
            sheet.addCell(lable);   
            
//          taking income tax value for every month in an Array
             inTaxPerMonth = new double[12];    // index 0 means the month of April
            
            for(int i=startMonth;i<12;i++) inTaxPerMonth[i] = taxSheet.taxPayable/noOfMonths;     //5753.0;
            
//          calculating total income tax
             totInTax=0;
            
            for(int i=startMonth;i<12;i++) 
            {
                totInTax += inTaxPerMonth[i];
            }
            
            lable = new Label(1, 2+rowTax, String.valueOf(totInTax) , boldBlueCellFormat);
            sheet.addCell(lable);   
            
//          total income tax paid till now
             totInTaxYTD=0;
            
//          net pay per month
             netPayPerMonth = new double[12];
            
//          total net pay after taxes
             totNetPayAfterTaxes=0;
//          total net pay after taxes got till now
             totNetPayAfterTaxesYTD=0;
            for(int i=startMonth;i<12;i++)
            {
                netPayPerMonth[i] = grossTHComp - inTaxPerMonth[i];
                totNetPayAfterTaxes += netPayPerMonth[i];
            }
            
            month = startMonth;
            for(int column = 2; column <= workingMonths+1; column++)
            {           
                totInTaxYTD += inTaxPerMonth[month];
                totNetPayAfterTaxesYTD += netPayPerMonth[month];
                lable = new Label(column, 2+rowTax, String.valueOf(inTaxPerMonth[month]), noBoldBlackRightCellFormat);
                sheet.addCell(lable);               
                month++;
            }
            
            lable = new Label(lastColumn, 2+rowTax, String.valueOf(totInTaxYTD), boldBlueCellFormat);
            sheet.addCell(lable);
            
            cellFormat = new WritableCellFormat(newCellFormat);
            cellFormat.setBorder(Border.NONE, BorderLineStyle.THICK);
            cellFormat.setAlignment(Alignment.LEFT);
            
            sheet.mergeCells(0, 3+row, lastColumn, 3+row);
            lable = new Label(0, 3+rowTax, "", cellFormat);
            sheet.addCell(lable);
            
            lable = new Label(0, 4+rowTax, "Net Pay (after Taxes)", noBoldBlackLeftCellFormat);
            sheet.addCell(lable);
            
            lable = new Label(1, 4+rowTax, String.valueOf(totNetPayAfterTaxes), boldBlueCellFormat);
            sheet.addCell(lable);

            month = startMonth;
            for(int column = 2; column <= workingMonths+1; column++)
            {           
                lable = new Label(column, 4+rowTax, String.valueOf(netPayPerMonth[month]), noBoldBlackRightCellFormat);
                sheet.addCell(lable);               
                month++;
            }
            
            lable = new Label(lastColumn, 4+rowTax, String.valueOf(totNetPayAfterTaxesYTD), boldBlueCellFormat);
            sheet.addCell(lable);
            
            
            
            
            
            
            workbook.write();
            workbook.close();
             
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
       

    }





    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond SalarySheet.list(params), model:[salarySheetInstanceCount: SalarySheet.count()]
    }

    def show(SalarySheet salarySheetInstance) {
        respond salarySheetInstance
    }

    def create() {
        respond new SalarySheet(params)
    }

    @Transactional
    def save(SalarySheet salarySheetInstance) {
        if (salarySheetInstance == null) {
            notFound()
            return
        }

        if (salarySheetInstance.hasErrors()) {
            respond salarySheetInstance.errors, view:'create'
            return
        }

        salarySheetInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'salarySheet.label', default: 'SalarySheet'), salarySheetInstance.id])
                redirect salarySheetInstance
            }
            '*' { respond salarySheetInstance, [status: CREATED] }
        }
    }

    def edit(SalarySheet salarySheetInstance) {
        respond salarySheetInstance
    }

    @Transactional
    def update(SalarySheet salarySheetInstance) {
        if (salarySheetInstance == null) {
            notFound()
            return
        }

        if (salarySheetInstance.hasErrors()) {
            respond salarySheetInstance.errors, view:'edit'
            return
        }

        salarySheetInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'SalarySheet.label', default: 'SalarySheet'), salarySheetInstance.id])
                redirect salarySheetInstance
            }
            '*'{ respond salarySheetInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(SalarySheet salarySheetInstance) {

        if (salarySheetInstance == null) {
            notFound()
            return
        }

        salarySheetInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'SalarySheet.label', default: 'SalarySheet'), salarySheetInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'salarySheet.label', default: 'SalarySheet'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}

