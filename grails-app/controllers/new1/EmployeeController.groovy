package new1

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import org.json.JSONObject;
import org.json.JSONArray;
import java.text.SimpleDateFormat;
import java.text.DateFormat;


//@Transactional(readOnly = true)
class EmployeeController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE" , create: "POST" , getnotification: "GET" , editing:"GET"]
    
    def datetime(){

    }
    def testing(){

    }
    def index(Integer max) {
        
        params.max = Math.min(max ?: 10, 100)
        respond Employee.list(params), model:[employeeInstanceCount: Employee.count()]
    }
    def blank(){
        
    }

    def tes(){
        def employee = Employee.findByUsername(session.emp.username)
        render (view:"tes", model:[emp:employee])
    }

    def show(Employee employeeInstance) {
        respond employeeInstance
    }
    def man(){
        redirect(controller:"Manager", action:"index")
    }
    def create1() {
        if(!(params.username)||!(params.password))
        {
            flash.message="error"
            redirect(controller:"Manager" , action:"empcreation")
            return
        }
        
        def employee = Employee.findByUsername(params.username)
        if(employee)
        {
            print("asjvjvdhavh ")
            flash.message="y"
            redirect(controller:"Manager" , action:"empcreation")
            return
        }
        def sample=new Employee()
        //def empstatus=new Employeestatus()
        sample.username=params.username
        sample.password=params.password
        def man = Manager.findByUsernameAndPassword(session.manage.username, session.manage.password)
        System.out.println(man)
        sample.manager = man
        if(sample.save(flush:true))
        render('save')
        else
        render('n save')
        empstatus.username=params.username
        empstatus.status=false
        empstatus.save(flush:true)
        def emplist=Employee.findAllByManager(session.manage)
        redirect(controller:"Manager", action:"empcreation")
   

    }

    def ganttnew()
    {
        
    }

    def editing()
    {
        if(!(params.progress))
        {
            flash.message="error"
            redirect(action:"gantt")
            return
        }
        System.out.println("inside")
       // def subtask=Project_subtask.findAllByEmployee(session.emp)

       
        System.out.println("inside the editing portion")
        def employeename=Employee.findByUsernameAndPassword(session.emp.username,session.emp.password)

        def pro=Project.findByProject_nameAndManager(params.pro_name , employeename.manager)
        def task=Project_task.findByNameAndProject(params.task_name , pro)

       
        def subtask=Project_subtask.findAllByEmployeeAndSubtask_nameAndProject_task(session.emp , params.subtask_name , task)
       System.out.println(employeename)
       System.out.println(pro)
       System.out.println(task)
       System.out.println(subtask)

        double value=Double.parseDouble(params.pr)
           value=value*100 
        int progress=Math.round(value)
        System.out.println(progress)        
        subtask[0].progress=progress
        subtask[0].save(flush:true)
        System.out.println("done it")
       
    }
    @Transactional
    def login(){
        //def a = Manager.findByUsername(params.username)
        //System.out.println(a.id+"\n"+a.username);
        def employee = Employee.findAllByUsernameAndPassword(params.username, params.password)
        def profile = Profile.findByEmployee(employee)
        if(!profile){
            flash.message = 'not'
            println 'there'
        }
        def project = Project.getAll()
        if(employee){
          session.emp = employee
          def messages=Message.findAllByReceiver(session.emp.username)
          
          def empstatus=Employeestatus.findAllByUsername(session.emp.username)
          empstatus[0].status=true;
          empstatus[0].save(flush:true)
          println session.emp.username
           int totalmessages=messages.size()
      

          render(view:"home", model:[employee:employee ,project:project , messages:messages.reverse() , totalmessages:totalmessages ,currentuser:session.emp.username])      
        }else{
          flash.message = "Error"
          redirect(controller:"Manager" , action:"index")
        }
    }
    def tax(){
        def pro = Profile.findByEmployee(session.emp)
        println pro
        if(pro == null)
        {
            render "Please first fill the profile of the employee"
        }
        if(pro)
        {
             def d = pro.date
            def d1 = pro.date.substring(2,4)
            println d1
            def date = new Date()
            DateFormat df = new SimpleDateFormat("MM/dd/yyyy"); 
            String d3 = df.format(date)
            def d4 = d3.substring(8,10)
            println d4
            render(view:"tax", model:[start:d1, end:d4])
        }
       
    }
    def taxsheet(){

    }
    def salarys(){
        def pro = Profile.findByEmployee(session.emp)
        def d = pro.date
        def d1 = pro.date.substring(2,4)
        println d1
        Integer b = Integer.valueOf(d1)
        println b
        def date = new Date()
        DateFormat df = new SimpleDateFormat("MM/dd/yyyy"); 
        String d3 = df.format(date)
        def d4 = d3.substring(8,10)
        println d4
        Integer a = Integer.valueOf(d4)
        println a-b 
        int [] c = new int[a-b+4];
        def i
        for(i=0;i<a-b+4;i++){
            c[i]=b+i;
            println i
        }
        def e = c
        println e
        render(view:"salarys", model:[start:c])
    }
    def salarysheet(){

    }
    def logout()
    {
        def empstatus=Employeestatus.findAllByUsername(session.emp.username)
          empstatus[0].status=false
          empstatus[0].save(flush:true)
        session.emp=null
    }

    def check(){
        def employee = Employee.findByUsername(session.emp.username)
        def empid = employee.id;
        def persons = Checkin_checkout.findAllByEmployee(employee);
        JSONArray nitai = new JSONArray();
        for(person in persons) {
            JSONArray gaur = new JSONArray();
            gaur.put(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(person.checkin_date));
            gaur.put(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(person.checkout_date));
            gaur.put(person.time_duration);
            gaur.put(person.employee.id); 
            nitai.put(gaur); 
        }
        //[data:nitai]
        render(view:"check", model:[employee:employee , id:empid, data:nitai])

    }
    def time(){
        def employee = Employee.findByUsername(session.emp.username)
        def proj = Project.findByManager(employee.manager);
        def task = Project_task.findByProject(proj);
        def tab = Timesheet.findAllByEmployee(employee);
        JSONArray nitai = new JSONArray();
        for(a in tab){
            JSONArray gaur = new JSONArray();
            gaur.put(new SimpleDateFormat("dd/MM/yyyy").format(a.fromdate));   
            gaur.put(new SimpleDateFormat("dd/MM/yyyy").format(a.todate));
            gaur.put(a.project);
            gaur.put(a.task);
            gaur.put(a.description);
            gaur.put(a.work_hour);
            gaur.put(a.employee);
            nitai.put(gaur);
        }
        render (view:"time", model:[proj:proj, task:task, employee:employee, data:nitai, emp:session.emp.id])
        
    }
    def newtime(){
        def employee = Employee.findByUsername(session.emp.username)
        def proj = Project.findByManager(employee.manager);
        def task = Project_task.findAllByProject(proj);
        render (view:"newtime", model:[proj:proj, task:task, employee:employee])
    }
    def attendance(){
        def persons = Checkin_checkout.getAll()
        def employee = Employee.findByUsername(params.username)
        System.out.println("id="+employee.id+persons.employee.id);   
    }
    def cin(){
        def employee = Employee.findByUsername(session.emp.username)
        render(view:"home", model:[employee:employee])      
    }
    def home(){ 
        
        def messages=Message.findAllByReceiver(session.emp.username)
        int totalmessages=messages.size()
           [messages:messages.reverse() , currentuser:session.emp.username , totalmessages:totalmessages]
    }
    def dec(){
        def emp = Employee.findByUsername(session.emp.username)
        def eduloan=new1.Eduloan.findAllByEmployee(emp)
        def donations=new1.Donations.findAllByEmployee(emp)
        def rentdetails=new1.Rentdetails.findAllByEmployee(emp)
        def homeloan=new1.Homeloan.findAllByEmployee(emp)
        def penfund=new1.Penfund.findAllByEmployee(emp)
        def investment80c=new1.Investment80c.findAllByEmployee(emp)
        def lifeins=new1.Lifeins.findAllByEmployee(emp)
        def mtreatment=new1.Mtreatment.findAllByEmployee(emp)
        def medins=new1.Medins.findAllByEmployee(emp)
        def billdetails=new1.Billdetails.findAllByEmployee(emp)
        def nps80c=new1.Nps80c.findAllByEmployee(emp)

        System.out.println(mtreatment);
        
         render(view:"dec", model:[emp:emp,donationss:donations,nps80cs:nps80c,eduloans:eduloan,
            rentdetailss:rentdetails,homeloans:homeloan,investment80cs:investment80c,
            lifeinss:lifeins,medinss:medins,mtreatments:mtreatment,billdetailss:billdetails,penfunds:penfund])
    }
    def signup(){
        //System.out.println(params.id+" empl "+params.username+" "+params.password);
        def a = Manager.findByUsername(session.emp.manager.username);
        System.out.println(a);
        render(view: "signup", model:[a:a]);
    }
    def profile(){
        def emp = Employee.findByUsername(session.emp.username)
        def pro = Profile.findByEmployee(emp);
        if(!pro){
            pro=Profile.findOrCreateWhere(employee:emp)
        }
        render (view:"profile", model:[emp:emp, pro:pro])
    }
    def profile1(){
        println 'what the problem'
        def emp = Employee.findByUsername(session.emp.username)
        def pro = Profile.findByEmployee(emp);
        if(pro){
            flash.message= 'having'
            render(view:"profile1")
        }
        else{
            flash.message = 'noth'
            pro = Profile.findOrCreateWhere(employee:emp)
            println pro.employee
            println 'harihari'
            render(view:"profile1", model:[emp:emp , pro:pro])
        }
        render(view:"profile1", model:[emp:emp, pro:pro])
    }
    def gantt()
    {
        def subtask=Project_subtask.findAllByEmployee(session.emp)
        if(subtask)
        {
        String projectname=""
        def employee=Employee.findAllByUsernameAndPassword(session.emp.username , session.emp.password)
        def _projectlist = Project.findAllByManager(employee[0].manager) 
        JSONObject top = new JSONObject();
        JSONArray  data = new JSONArray();
        JSONArray  links = new JSONArray();
        int id = 1, proj_id=0, proj_task_id=0;
        int  link_id=0;
        int task_id =0;
        DateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        
        for(project in _projectlist){
            for(int i=0 ; i<subtask.size(); i++)
            {
                if(project.project_name==subtask[i].project_task.project.project_name)
                projectname=subtask[i].project_task.project.project_name
            }
            if(project.project_name==projectname)
            {
            JSONObject proj = new JSONObject();
            JSONObject pro_task_link = new JSONObject();
            proj.put("id",id);
            proj_id=id++;
            task_id=id;
            proj.put("text",project.project_name);
            proj.put("type","project"); 
            proj.put("start_date",outputFormat.format(project.start_time));
            proj.put("open",true);
            proj.put("parent",0);
            proj.put("owner"," ");
            def tasks =  Project_task.findAllByProject(project);
                int sums=0;
                double avgs;
                int lens=tasks.size();
                if(lens>0)
                {
                    for(int i=0; i <lens ;i++)
                    {    def st =  Project_subtask.findAllByProject_task(tasks[i]);
                        for(int k=0; k<st.size() ;k++)
                        {
                        sums=sums+st[k].progress
                    }
                    }
                    avgs=(sums*1.0)/lens
                    proj.put("progress",avgs/100);
                }
                else
                proj.put("progress",0.0);
            
            pro_task_link.put("id",link_id++);
            pro_task_link.put("source",proj_id);
            pro_task_link.put("target",task_id);
            pro_task_link.put("type","1");
            links.put(pro_task_link);
            data.put(proj);
            
            def proj_tasks =  Project_task.findAllByProject(project);
            for(projec_task in proj_tasks) {
                JSONObject task = new JSONObject();
                JSONObject link = new JSONObject();
                task.put("id",id);
                proj_task_id = id++;
                task.put("owner"," ");
                task.put("text",projec_task.name);
                task.put("type","task"); 
                task.put("start_date",outputFormat.format(projec_task.start_date));
                task.put("open",true);
                task.put("parent",proj_id);
                def subtasks =  Project_subtask.findAllByProject_task(projec_task);
                int sum=0;
                double avg;
                int len=subtasks.size();
                if(len>0)
                {
                    for(int i=0; i < subtasks.size();i++)
                    {
                        sum=sum+subtasks[i].progress
                    }
                    avg=(sum*1.0)/len
                    task.put("progress",avg/100);
                }
                else
                task.put("progress",0.0);
                data.put(task);
                if(task_id != proj_task_id){

                    link.put("id",link_id++);
                    link.put("source",task_id);
                    link.put("target",proj_task_id);
                    link.put("type","0");
                    links.put(link);
                }
                
                def proj_subtasks =  Project_subtask.findAllByProject_task(projec_task);
                for(projec_subtask in proj_subtasks) {
                    JSONObject sub_task = new JSONObject();
                    JSONObject linkin = new JSONObject();
                    sub_task.put("id",id);              
                    sub_task.put("text",projec_subtask.subtask_name);
                    sub_task.put("type","sub_task"); 
                    sub_task.put("start_date",outputFormat.format(projec_subtask.start_time));
                    sub_task.put("end_date",outputFormat.format(projec_subtask.end_time));
                    sub_task.put("open",false);
                    sub_task.put("parent",proj_task_id);
                    
                    
                    sub_task.put("owner", projec_subtask.employee.id);
                    
                    sub_task.put("progress",projec_subtask.progress/100);
                    data.put(sub_task);
                    linkin.put("id",link_id++);
                    linkin.put("source",proj_task_id);
                    linkin.put("target",id++);
                    linkin.put("type","2");
                    links.put(linkin);
                }
            } 
        }
    }
        top.put("data",data);
        top.put("links",links);
        
        def employee_list = Employee.findAllByManager(employee[0].manager);
            //  JSONObject emp_obj = new JSONObject();
            JSONArray  emp_data = new JSONArray();
            int eid =1;
            for(empl in employee_list){
                JSONObject employ = new JSONObject();
                employ.put("key",empl.id);
                employ.put("label",empl.username);
                emp_data.put(employ);
            }
        //emp_obj.put("emp_data",emp_data);
        System.out.println("done it")
        System.out.println(top.toString())
        render(view:'gantt',model:[jsonData1:top.toString(), jsonData2:emp_data.toString()]);
    }
    

    else
    render(view:'ganttnotask')
    }

    def ganttnotask()
    {
        
    }

    

    def employeechat() {
        def messages = Message.listOrderByDate(order: 'desc', max:10)
        def empstatus=Employeestatus.getAll()
        def employee=Employee.getAll()
        [messages:messages , empstatus:empstatus , employee:employee , currentuser:session.emp.username]
    }
    def submitMessage() {
            Date xyz=new Date();
            String receiver
            if(params.emplo)
            receiver=params.emplo
            else
            receiver="all"
        new Message(date:xyz , nickname: session.emp.username , message:params.comments , receiver:receiver).save()
        redirect(action:"employeechat")
    }

    def getnotification()
    {
        def messages=Message.findAllByReceiver(session.emp.username)
        System.out.println("i am in.....")
       [messages:messages.reverse() , currentuser:session.emp.username] 
    }


    @Transactional
    def save(){
        if(!(params.username)||!(params.password))
        {
            flash.message="error"
            redirect(controller:"Manager" , action:"empcreation")
            return
        }
        
        def employee = Employee.findByUsername(params.username)
        if(employee)
        {
            print("asjvjvdhavh ")
            flash.message="y"
            redirect(controller:"Manager" , action:"empcreation")
            return
        }
        def man = Manager.findByUsernameAndPassword(session.manage.username, session.manage.password)
        def sample = new Employee()
        //def empstatus=new Employeestatus()
        sample.username=params.username
        sample.password=params.password
        System.out.println(man)
        sample.manager = man
        if(sample.save(flush:true))
        {redirect(controller:"Manager", action:"empcreation")}
        else
        {   println sample
            System.out.println(sample.manager);//" "+sample.password+" "+sample.username+" "+sample);
            println 'harih'
            render('n save')}
        /*empstatus.username=params.username
        empstatus.status=false
        empstatus.save(flush:true)*/
        
        /*if(params == null){
            redirect(action:"index")
            flash.message=''
            return
        }
        def employee =Employee.findByUsername(params.username);
        if(employee!=null){
            flash.message="username already exist"
            render(view:"signup")
        }
        else{
            def newEmp = new Employee();
            newEmp.username=params.username
            newEmp.password=params.password
            newEmp.manager=Manager.findByUsername(params.manager);
            System.out.println(params.manager_id)
            
            if(newEmp.save(flush:true)){
                flash.message="Employee created"
                render(view:"index")
            }
            else{
                flash.message="Please enter valid data"
                render(view:"signup")
            }
        }*/
    }

    def edit(Employee employeeInstance) {
        respond employeeInstance
    }

    @Transactional
    def update(Employee employeeInstance) {
        if (employeeInstance == null) {
            notFound()
            return
        }

        if (employeeInstance.hasErrors()) {
            respond employeeInstance.errors, view:'edit'
            return
        }

        employeeInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Employee.label', default: 'Employee'), employeeInstance.id])
                redirect employeeInstance
            }
            '*'{ respond employeeInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Employee employeeInstance) {

        if (employeeInstance == null) {
            notFound()
            return
        }

        employeeInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Employee.label', default: 'Employee'), employeeInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'employee.label', default: 'Employee'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
