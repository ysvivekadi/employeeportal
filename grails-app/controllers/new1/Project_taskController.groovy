package new1



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import java.text.DateFormat
import java.text.SimpleDateFormat

//@Transactional(readOnly = true)
class Project_taskController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE" , deleting:"GET" , testing:"GET"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Project_task.list(params), model:[taskInstanceCount: Project_task.count()]
    }

    def show(Project_task taskInstance) {
        respond taskInstance
    }

    def create() {
        respond new Project_task(params)
    }

    def testing()
    {
        System.out.println("inside")
        
        java.util.Date fecha1 = new java.util.Date(params.start_time);
        DateFormat formatter = new SimpleDateFormat("EEE MMM dd HH:mm:ss Z yyyy");
        
        Date date2
        
        date2 = (Date)formatter.parse(fecha1.toString());
        def sample=new Project_task();
        def xyz=Project.findAllByProject_nameAndManager(params.project_name, session.manage)  
        
        sample.start_date=date2
        sample.name=params.task_name
        
        System.out.println (date2)
        
        
        
        sample.save()
        xyz[0].addToTasks(sample)
        sample.save(flush:true)
        System.out.println(xyz[0])
System.out.println("done it")
    }
    def deleting()
    {
        System.out.println("inside")
        def xyz=Project_task.findAllByName(params.task_name)
        System.out.println(xyz[0])
        xyz[0].delete(flush:true)
        System.out.println("done it")
    }

    @Transactional
    def save() {
        if(params == null){
            render(controller:"Employee" , view:"home")
            flash.message=''
            return
        }
        def task =Project_task.findByProject_task(params.task);
        if(task!=null){
            flash.message="task already exist"
            render(controller:"Employee" , view:"home")
        }
        else{
            def newt = new Project_task();
            newt.task=params.task
            newt.project=Project.findByName(params.project);
            def a = Manager.findByUsername(params.manager);
            //def ad = Project_task.findByProject_id(params.project.id)
            def c = Project_task.createCriteria().list{
                project {
                    eq('id' , newt.project.id)
                }
            }
            System.out.println(c);
            //newt.manager_id=params.manager_id;
            //System.out.println(params.manager_id)
            //newt.manager=Manager.getByid(params.manager_id)
            if(newt.save(flush:true)){
               flash.message="task created"
                redirect(controller:"Manager", action:"abc", params:[username:a.username]);
            }
            else{
                flash.message="Please enter valid data"
                render('Not saved')
            }
        }
    }

    def edit(Project_task taskInstance) {
        respond taskInstance
    }

    @Transactional
    def update(Project_task taskInstance) {
        if (taskInstance == null) {
            notFound()
            return
        }

        if (taskInstance.hasErrors()) {
            respond taskInstance.errors, view:'edit'
            return
        }

        taskInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Project_task.label', default: 'Project_task'), taskInstance.id])
                redirect taskInstance
            }
            '*'{ respond taskInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Project_task taskInstance) {

        if (taskInstance == null) {
            notFound()
            return
        }

        taskInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Project_task.label', default: 'Project_task'), taskInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'task.label', default: 'Project_task'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
