package new1



import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class InvestmentController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Investment.list(params), model:[investmentInstanceCount: Investment.count()]
    }

    def show(Investment investmentInstance) {
        respond investmentInstance
    }

    def create() {
        respond new Investment(params)
    }

    @Transactional
    def save(Investment investmentInstance) {
        if (investmentInstance == null) {
            notFound()
            return
        }

        if (investmentInstance.hasErrors()) {
            respond investmentInstance.errors, view:'create'
            return
        }

        investmentInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'investment.label', default: 'Investment'), investmentInstance.id])
                redirect investmentInstance
            }
            '*' { respond investmentInstance, [status: CREATED] }
        }
    }

    def edit(Investment investmentInstance) {
        respond investmentInstance
    }

    @Transactional
    def update(Investment investmentInstance) {
        if (investmentInstance == null) {
            notFound()
            return
        }

        if (investmentInstance.hasErrors()) {
            respond investmentInstance.errors, view:'edit'
            return
        }

        investmentInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Investment.label', default: 'Investment'), investmentInstance.id])
                redirect investmentInstance
            }
            '*'{ respond investmentInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Investment investmentInstance) {

        if (investmentInstance == null) {
            notFound()
            return
        }

        investmentInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Investment.label', default: 'Investment'), investmentInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'investment.label', default: 'Investment'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
